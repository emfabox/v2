#! /usr/bin/perl


use DBI;
use DBD::mysql;

open(MWCONFIG, '/etc/emfa/mwconfig'); 
        my @mw_config = <MWCONFIG>;
        close MWCONFIG;
        s{^\s+|\s+$}{}g foreach @mw_config;

        my($db_name) = $mw_config[0];
        my($db_host) = $mw_config[1];
        my($db_user) = $mw_config[2];
        my($db_pass) = $mw_config[3];
        my($db_name2) = $mw_config[4]; 
        
$qry="SELECT domain FROM `domain` where avscan='y'";
$dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass);
# set to utf8
$dbh->do('SET NAMES utf8');

$sth = $dbh->prepare($qry)or die "Cannot execute SQL query.\n";
$rv = $sth->execute or die "can't execute the query";

# BIND TABLE COLUMNS TO VARIABLES
$sth->bind_columns(undef, \$domain);

# 
$Directory =$ARGV[0];
chop($Directory);
$avira="AVIRA";
$Directory = "$Directory$avira"; 
mkdir $Directory;

# LOOP THROUGH RESULTS
while($sth->fetch()) {
	$dom= $domain;
	open(DATA, "grep -l $dom *.header |") || die "Failed: $!\n";
	while(<DATA>)
	{
	   my $filetocopy="$_";
	   chomp $filetocopy;
	   $filetocopy =~ s/.header//g;	
	   system("cp -r $filetocopy* $Directory");
	}
}
$rv = $sth->finish;

$Directory .= "/.";
open(DATA, "/var/opt/savapi/bin/clientlib_dir_scan_example $Directory |") || die "Failed: $!\n";

my $filename;
my $infected=0;

#Infected:
#        malware_info->name <Eicar-Test-Signature>
#        malware_info->type <virus>

while(<DATA>)
{
   my $line="$_";

   if ($line =~ /$Directory/)
   {
	if (($line!~ /Scanning directory/) &&
	    ($line!~ /Checking directory path/) &&
	    ($line!~ /Creating log file/))
	{
		chomp $line;
        $match="AVIRA/";
		$empty="";
        $line =~ s/$match/$empty/g;	
		$filename = $line;
		$infected=0;
	}
   }
   if ($line =~ /Infected/)
   {
		$filename="INFECTED::".$filename;
   }
   if ($line =~ /malware_info->name/)
   {
		$line =~ s/malware_info->name//;
		$line =~ s/^\s+//;
		chomp $line;
		$filename .="::$line";
   }
   if ($line =~ /malware_info->type/)
   {
		$line =~ s/malware_info->type//;
		$line =~ s/^\s+//;
		chomp $line;
		$filename .="::$line";
		$infected=1;
   }
   if ($infected==1) 
   {

		print "$filename\n";
		$infected=0;
   }
} 

