#!/usr/bin/perl
################################################################################
# By BumbleBeeWare.com 2006
# check-blacklist.cgi
################################################################################
#
# Modified for use with EMFABOX
# This script is an modification of blacklist-sender.cgi for a bit more security
# 
# Copyright (C) 2014  http://www.cycomptec.com 
#
################################################################################ 
#
# MAILWATCH PART
use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use Sys::Hostname::Long;
use DBI;
use MIME::Lite;


##########################
# configuration
$tempdir = "/var/www/temp";

##########################

# get ip address
$client= $ENV{REMOTE_ADDR};

# get hostname long & to build no-reply email address #


$myhosturl = hostname_long;
@alist = split(/\./, $myhosturl) ;
$myhostname= join('.',$alist[1],$alist[2]);

# parse the input data
&form_parse;

# lets block direct access that is not via the form post
if ($ENV{"REQUEST_METHOD"} ne "POST"){&nopost;}

# use this program to remove all old temp files
# this keeps the director clean without setting up a cron job
opendir TMPDIR, "$tempdir"; 
@alltmpfiles = readdir TMPDIR;

foreach $oldtemp (@alltmpfiles) {

	$age = 0;
	$age = (stat("$tempdir/$oldtemp"))[9];
	# if age is more than 300 seconds or 5 minutes	
	if ((time - $age) > 300){unlink "$tempdir/$oldtemp";}
	
	}


# open the temp datafile for current user based on ip
$tempfile = "$tempdir/$ENV{'REMOTE_ADDR'}";
open (TMPFILE, "<$tempfile")|| ($nofile = 1);
(@tmpfile) = <TMPFILE>;
close TMPFILE;

# if no matching ip file check for a cookie match
# this will compensate for AOL proxy servers accessing images
if ($nofile == 1){
	
$cookieip = $ENV{HTTP_COOKIE};
$cookieip =~ /checkme=([^;]*)/;
$cookieip = $1;

if ($cookieip ne ""){
	
	$tempfile = "$tempdir/$cookieip";
	open (TMPFILE, "<$tempdir/$cookieip")|| &nofile;
	(@tmpfile) = <TMPFILE>;
	close TMPFILE;
}

}

$imagetext = $tmpfile[0];
chomp $imagetext;

# set the form input to lower case
$FORM{'verifytext'} = lc($FORM{'verifytext'});

# compare the form input with the file text
if ($FORM{'verifytext'} ne "$imagetext"){&error;}

# get the other vars from submit form ...
$id = $FORM{'id'};
$to_address = $FORM{'to_address'};
$from_address = $FORM{'from_address'};
$to_domain = $FORM{'to_domain'};
$myhostname= $FORM{'myhostname'};
$mypin = $FORM{'pin'};
$blacklist = $FORM{'blacklist'};


# now delete the temp file so it cannot be used again by the same user
unlink "$tempfile";

# if no error continue with the program
print "Content-type: text/html\n\n";


# get db config
open(WTCONFIG, '/etc/emfa/wtconfig');
@wt_config = <WTCONFIG>;
close WTCONFIG;
s{^\s+|\s+$}{}g foreach @wt_config;
  $db_name = $wt_config[0];
  $db_host = $wt_config[1];
  $db_user = $wt_config[2];
  $db_pass = $wt_config[3];
  
# check id --> pin


            
            $sql4 = "SELECT token FROM tokens WHERE id=\"$id\"";
            
            $dbh4 = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass, {PrintError => 0});
           
            if (!$dbh4) {&error;}
            
            $sth4 = $dbh4->prepare($sql4);
            $sth4->execute;
            
            @result4 = $sth4->fetchrow;
            
            if (!$result4[0]) {
            
            &GlobError;
            
            # Close connections  
            #$sth4->finish();
            #$dbh4->disconnect();
            } 
            else 
            {
            if (!$result4[0] == $mypin ) {
            
                  print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
                  print "<html>";
            	    print "<head>";
            	    print "<title>Antispam Engine Error</title>";
            	    print "</head>";
                  print "<body>";   
                  print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
                  print "<center>";
                  print ":( ... Something went wrong with your <font color=\"red\">PIN</font> and we can't process your request right now.";
                  print "<br>";
                  print "Please go back try again.";
                  print "</center>";
                  print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
                  print "</body>";
            	    print "</html>";   
                  exit;	
            
            }

            }









# get mailscanner db config
    open(MWCONFIG, '/etc/emfa/mwconfig');
    my @mw_config = <MWCONFIG>;
    close MWCONFIG;
    s{^\s+|\s+$}{}g foreach @mw_config;
    my($db_name2) = $mw_config[0];
    my($db_host2) = $mw_config[1];
    my($db_user2) = $mw_config[2];
    my($db_pass2) = $mw_config[3];

# check for trusted relay domain
     
       $dbh2 = DBI->connect("DBI:mysql:database=$db_name2;host=$db_host2", $db_user2, $db_pass2, {PrintError => 0});
       if (!$dbh2) {&error;}
  
       $sql1 = "SELECT domain from domain WHERE domain=\"$to_domain\"";
       
       $sth1 = $dbh2->prepare($sql1);
       $sth1->execute;
       @result = $sth1->fetchrow;
      
       
      if (!$result[0]) { 
        $sth1->finish();
         
        # display failure page
       	print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      	print "<html>";
	      print "<head>";
	      print "<title>Antispam Engine</title>";
	      print "</head>";
      	print "<body>";
        print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
	      print "<div align='center'>";
	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
	      print "<THEAD>";
	      print "<TH>Antispam Learning</TH>";
	      print "</THEAD>";
	      print "</TABLE>";
	      print "<br><strong><font color=\"red\" size=\"6\" > NOT AUTHORIZED!</font></strong><br>";
	      print "<br>Your Account must be trusted to teach the Antispam Firewall!</br>";
	      print "</div>";
	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
	      print "</body>";
	      print "</html>";
        
        exit:        
      }


if ($id =~ /^[A-F0-9]{9}\.[A-F0-9]{5}|[A-F0-9]{10}\.[A-F0-9]{5}|[A-F0-9]{11}\.[A-F0-9]{5}$/ && $blacklist==1 ){



 # Verify if id is present in db
   
            $sql1  = 'REPLACE INTO blacklist (to_address, to_domain, from_address) VALUES';
            $sql1 .= '(\''.$to_address;
	          $sql1 .= '\',\''.$to_domain;
	          $sql1 .= '\',\''.$from_address.'\')';      
  
            $dbh2 = DBI->connect("DBI:mysql:database=$db_name2;host=$db_host2", $db_user2, $db_pass2, {PrintError => 0});
            if (!$dbh2) {&error;}
            $sqlQuery  = $dbh2->prepare($sql1)or die "Can't prepare $sql1: $dbh2->errstr\n";
      	    $sth2 = $sqlQuery->execute or die "can't execute the query: $sqlQuery->errstr";
      	    $str = $sqlQuery->finish;
            
            &mail_results;
            
            
              print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
              print "<html>";
      	      print "<head>";
      	      print "<title>Antispam Engine</title>";
      	      print "</head>";
      	      print "<body>";
              print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "<div align='center'>";
      	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
      	      print "<THEAD>";
      	      print "<TH>Antispam Blacklist Entry</TH>";
      	      print "</THEAD>";
      	      print "</TABLE>";
      	      print "<br><strong><font color=\"blue\" size=\"6\" >SENDER ADDED TO BLACKLIST!</font></strong><br>";
      	      print "<br>Sender <b>$from_address</b> has been added to your personal blacklist.</br>";
      	      print "</div>";
      	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "</body>";
	            print "</html>";  
              exit;
}

if ($id =~ /^[A-F0-9]{9}\.[A-F0-9]{5}|[A-F0-9]{10}\.[A-F0-9]{5}|[A-F0-9]{11}\.[A-F0-9]{5}$/ && $blacklist==2 ){

    
        $sql2 = 'DELETE FROM blacklist WHERE ';
        $sql2 .= 'to_address = \''.$to_address;
        $sql2 .= '\'';
        $sql2 .= ' AND ';
        $sql2 .= 'to_domain = \''.$to_domain;
        $sql2 .= '\'';
        $sql2 .= ' AND ';
        $sql2 .= 'from_address = \''.$from_address.'\' ';
    
     $dbh2 = DBI->connect("DBI:mysql:database=$db_name2;host=$db_host2", $db_user2, $db_pass2, {PrintError => 0});
     if (!$dbh2) {&error;}
     $sqlQuery  = $dbh2->prepare($sql2)or die "Can't prepare $sql2: $dbh2->errstr\n";
     $sqlQuery  = $dbh2->prepare($sql1)or die "Can't prepare $sql1: $dbh2->errstr\n";
     $sth2 = $sqlQuery->execute or die "can't execute the query: $sqlQuery->errstr";
     $str = $sqlQuery->finish;
     
     # show result
     
              print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
              print "<html>";
      	      print "<head>";
      	      print "<title>Antispam Engine</title>";
      	      print "</head>";
      	      print "<body>";
              print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "<div align='center'>";
      	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
      	      print "<THEAD>";
      	      print "<TH>Antispam Blacklist Entry</TH>";
      	      print "</THEAD>";
      	      print "</TABLE>";
      	      print "<br><strong><font color=\"red\" size=\"6\" >SENDER REMOVED FROM BLACKLIST!</font></strong><br>";
      	      print "<br>Sender <b>$from_address</b> has been removed from your personal blacklist.</br>";
      	      print "</div>";
      	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "</body>";
	            print "</html>";  
             exit;
}




sub error {
	
print "Content-type: text/html\n\n";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
print "<center>";
print " :( ... ";
#print "Input verification code does not match the text on the image.";
print "Something went wrong and we can't process your request right now.";
print "<br>";
print "Please try again later.";
print "</center>";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
# now delete the temp file so it cannot be used again by the same user
unlink "$tempdir/$ENV{'REMOTE_ADDR'}";
exit;	
}

sub nofile {
	
print "Content-type: text/html\n\n";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
print "<center>";
print " :( ... ";
#print "No file found for verification.";
print "Something went wrong and we can't process your request right now.";
print "<br>";
print "Please try again later.";
print "</center>";

print "</center>";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";	
exit;	
}


sub nopost {
	
print "Content-type: text/html\n\n";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
print "<center>";
print " :( ... ";
#print "Method not allowed, input must be via a form post.";
print "Something went wrong and we can't process your request right now.";
print "<br>";
print "Please try again later.";
print "<center>";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
exit;	
}



sub form_parse  {
	read (STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	@pairs = split(/&/, $buffer);

	foreach $pair (@pairs)
	{
    	($name, $value) = split(/=/, $pair);
    	$value =~ tr/+/ /;
    	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    	$FORM{$name} = $value;
}}

sub mail_results {


      $mail_from = "no-reply\@".$myhostname;
 #        
      $message_subject = "[INFO] Antispam Blacklist Confirmation";
      $message_body ="\nPlease scroll down for the German translation.\n";
      $message_body = $message_body."Die deutsche Uebersetzung finden Sie im Anschluss an den englischen Text.\n\n";
      $message_body = $message_body."---- ENGLISH -----------\n\n";
			$message_body = $message_body."The Antispam service received an blacklist creation from your address for this Sender $from_address.\n\n";
			$message_body = $message_body."If you did this by mistake use the following link to undo this:\n\n"; 
      $message_body = $message_body."http://".$myhosturl."/cgi-bin/blacklist-sender.cgi?blacklist=2&id=".$id;
			$message_body = $message_body."\n\nIf you didn't made this request please forward this message to your IT Support.\n";
      $message_body = $message_body."This is an automatic generated message. Please do not reply.\n\n";
      $message_body = $message_body."Thank you.\n\n";
      $message_body = $message_body."--- DEUTSCH -----------\n\n";
      $message_body = $message_body."Das Antispam Service einen Blacklist Eintrag von Ihrer E-Mail Adresse fuer folgenden Sender $from_address erhalten.\n\n";
      $message_body = $message_body."Sollte es sich hier um ein Missverstaendniss handeln so koennen Sie diesen Eintrag hier wiederrufen:\n\n";
      $message_body = $message_body."http://".$myhosturl."/cgi-bin/blacklist-sender.cgi?blacklist=2&id=".$id;
      $message_body = $message_body."\n\nWurde dieser Request nicht von Ihnen erstellt? So leiten Sie bitte diese Nachricht an Ihren IT Support weiter.\n";
      $message_body = $message_body."Diese Meldung wurde vom System automatisch erstellt. Beantworten Sie diese bitte nicht.\n\n";
      $message_body = $message_body."Besten Dank.\n\n";
# 

		  my $msg3 = MIME::Lite->new(
			From    => $mail_from,
			To      => $to_address,
		  Subject => $message_subject,
			Type    => 'multipart/mixed',
		);

		$msg3->attach(
			Type     => 'TEXT',
			Data     => $message_body,
		);

		$msg3->send;
    
    }

 sub GlobError {
#print "Content-type: text/html\n\n";
      print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      print "<html>";
	    print "<head>";
	    print "<title>Antispam Engine Error</title>";
	    print "</head>";
      print "<body>";   
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "<center>";
      print ":( ... Something went wrong and we can't process your request right now.";
      print "<br>";
      print "Please try again later.";
      print "</center>";
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "</body>";
	    print "</html>";   
exit;	
}
