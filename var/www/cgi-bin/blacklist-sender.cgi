#!/usr/bin/perl
##############################################################################
# CAPTCHA based on BumbleBeeWare.com 
##############################################################################
#
# Modified for use with EMFABOX
# This script is an modification of blacklist-sender.cgi
# 
# Copyright (C) 2014  http://www.cycomptec.com 
#
############################################################################## 


use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use Sys::Hostname::Long;
use Data::Validate::IP qw(is_ipv4 is_ipv6 is_private_ipv4);
use DBI;
use MIME::Lite;


print "Content-type: text/html \n\n";

$query    = new CGI;
$id = param("id");
$blacklist = param("blacklist");


# get ip address
$client= $ENV{REMOTE_ADDR};


# get hostname long & to build no-reply email address #

$myhosturl = hostname_long;
@alist = split(/\./, $myhosturl) ;
$myhostname= join('.',$alist[1],$alist[2]);

#Anti hack check
if ($id =~ /\|/) {
      print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      print "<html>";
	    print "<head>";
	    print "<title>Antispam Engine Error</title>";
	    print "</head>";
      print "<body>";   
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "<center>";
      print ":( ... Something went wrong and we can't process your request right now.";
      print "<br>";
      print "Please try again later.";
      print "</center>";
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "</body>";
	    print "</html>";   
exit;
}

# first check spamlog
# get mailscanner db config
open(MWCONFIG, '/etc/emfa/mwconfig');
my @mw_config = <MWCONFIG>;
close MWCONFIG;
s{^\s+|\s+$}{}g foreach @mw_config;
my($db_name) = $mw_config[0];
my($db_host) = $mw_config[1];
my($db_user) = $mw_config[2];
my($db_pass) = $mw_config[3];


# CHECK for vars
if ($id eq "" ){&error;}
if ($blacklist eq "" ){&error;}

# CHECK for valid id & blacklist == 1

if ($id =~ /^[A-F0-9]{9}\.[A-F0-9]{5}|[A-F0-9]{10}\.[A-F0-9]{5}|[A-F0-9]{11}\.[A-F0-9]{5}$/ && $blacklist==1 ){

 # Verify if id is present in db
   
  $dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass, {PrintError => 0});
  
  if (!$dbh) {&error;}
      
      
      $sql = "SELECT id,from_address,to_address,subject,to_domain,from_domain from maillog WHERE id=\"$id\"";
  
      $sth = $dbh->prepare($sql);
      $sth->execute;
      @results = $sth->fetchrow;
      
      $from_address = $results[1];
      $to_address = $results[2];
      $subject = $results[3];
      $to_domain = $results[4];
      $from_domain = $results[5];
      
       
      
      if (!$results[0]) { 
      # msg id not found in database ...
      
        $sth->finish();
        $dbh->disconnect();
        
        # print ms like error ..  
 
          print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
          print "<html>";
	        print "<head>";
	        print "<title>Antispam Engine Error</title>";
	        print "</head>";
          print "<body>";   
          print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
          print "<center>";
          print ":( ... Something went wrong and we can't process your request right now.";
          print "<br>";
          print "</center>";
          print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
          print "</body>";
	        print "</html>";    
      exit;
      }      
      else  
      {
      
      # check for trusted relay domain
      
       $sql1 = "SELECT domain from domain WHERE domain=\"$to_domain\"";   
       $sth1 = $dbh->prepare($sql1);
       $sth1->execute;
       @result = $sth1->fetchrow;
      
       
      if (!$result[0]) { 
        $sth1->finish();
         
        # display failure page
       	print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      	print "<html>";
	      print "<head>";
	      print "<title>Antispam Engine</title>";
	      print "</head>";
      	print "<body>";
        print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
	      print "<div align='center'>";
	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
	      print "<THEAD>";
	      print "<TH>Antispam Learning</TH>";
	      print "</THEAD>";
	      print "</TABLE>";
	      print "<br><strong><font color=\"red\" size=\"6\" > NOT AUTHORIZED!</font></strong><br>";
	      print "<br>Your Account must be trusted to teach the Antispam Firewall!</br>";
	      print "</div>";
	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
	      print "</body>";
	      print "</html>";
        exit;         
      }  
      else {
      # This subroutine checks whether the address belongs to any of the private IPv4 networks - 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16 - as defined by RFC 5735.

      if (is_private_ipv4($client)) {

      #print "Looks like an IPv4 address as defined by RFC 5735";
      
      
       if ( $to_domain == $from_domain ) {
            print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
            print "<html>";
	          print "<head>";
	          print "<title>Antispam Engine Error</title>";
	          print "</head>";
            print "<body>";   
            print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
            print "<br>";
            print "<center>";
            print ":( ... Something went wrong and we can't process your request right now.";
            print "<br><br>";
            print "<font color=\"red\" size=\"2\" >";
            print "[ERROR] Account trusted!";
            print "</font>";
            print "<br>";
            print "</center>";
            print "<br>";
            print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
            print "</body>";
	          print "</html>";   
            exit;
          }
          else 
          {
            $sql1  = 'REPLACE INTO blacklist (to_address, to_domain, from_address) VALUES';
            $sql1 .= '(\''.$to_address;
	          $sql1 .= '\',\''.$to_domain;
	          $sql1 .= '\',\''.$from_address.'\')';      
  
          #  $dbh2 = DBI->connect("DBI:mysql:database=$db_name2;host=$db_host2", $db_user2, $db_pass2, {PrintError => 0});
           # if (!$dbh2) {&error;}
            $sqlQuery  = $dbh->prepare($sql1)or die "Can't prepare $sql1: $dbh2->errstr\n";
      	    $sth2 = $sqlQuery->execute or die "can't execute the query: $sqlQuery->errstr";
      	    $str = $sqlQuery->finish;
            
            &mail_results;
            
              print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
              print "<html>";
      	      print "<head>";
      	      print "<title>Antispam Engine</title>";
      	      print "</head>";
      	      print "<body>";
              print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "<div align='center'>";
      	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
      	      print "<THEAD>";
      	      print "<TH>Antispam Blacklist Entry</TH>";
      	      print "</THEAD>";
      	      print "</TABLE>";
      	      print "<br><strong><font color=\"red\" size=\"6\" >SENDER ADDED TO BLACKLIST!</font></strong><br>";
      	      print "<br>Sender <b>$from_address</b> has been added to your personal blacklist.</br>";
      	      print "</div>";
      	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "</body>";
	            print "</html>";  
             exit;
          }
    } else 
    {
      print "  
          <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">    
          <html>    
            <head>    
              <title>Antispam Engine
              </title>    
            </head>    
            <body>    
              <body bgcolor=\"#FFFFFF\">    
                <center>    
                  <form method=\"POST\" action=\"check-blacklist.cgi\">      
                    <table border=\"0\" cellpadding=\"2\" cellspacing=\"2\" width=\"60%\" align=\"center\">      
                      <tr>      
                        <td align=\"center\" colspan=\"2\">      
                          <hr size=\"1\" color=\"#ffa042\">
                        </td>      
                      </tr>        
                      <tr>        
                        <td align=\"center\" colspan=\"2\">
                          <font size=\"3\" face=\"arial\">
                          <strong>        
                              <img src=\"captcha.cgi\" border=\"1\">
                          </strong>        
                          </font>
                          </td>      
                      </tr>      
                      <tr>        
                        <td align=\"center\" colspan=\"2\">        
                        <font size=\"3\" face=\"Arial, Helvetica, sans-serif\">To blacklist sender <b>$from_address</b>, please enter your PIN and the text shown on the image as the verification code below.</font>
                        </td>      
                      </tr>           
                      <tr>        
                        <td align=\"right\" width=\"50%\">          
                          <input type=\"hidden\" name=\"id\" value=\"$id\" />          
                          <input type=\"hidden\" name=\"to_address\" value=\"$to_address\" />            
                          <input type=\"hidden\" name=\"to_domain\" value=\"$to_domain\" />
                          <input type=\"hidden\" name=\"from_address\" value=\"$from_address\" />              
                          <input type=\"hidden\" name=\"myhostname\" value=\"$myhostname\" />
                          <input type=\"hidden\" name=\"blacklist\" value=\"$blacklist\" />
                          PIN:&nbsp;<input type=\"password\" size=\"10\" name=\"pin\" autocomplete=\"off\" />                              
                        </td>
                        <td align=\"left\" width=\"50%\">Text:&nbsp;<input type=\"text\" size=\"10\" name=\"verifytext\"><input type=\"submit\" name=\"checktext\" value=\"Verify\"></td>     
                      </tr>      
                      <tr>        
                        <td align=\"center\" colspan=\"2\">        
                          <hr size=\"1\" color=\"#ffa042\">
                        </td>      
                      </tr>    
                  </form>    
                </center>    
              </body>    
          </html>";
        }
    }          
  }
}

# undo blacklist

if ($id =~ /^[A-F0-9]{9}\.[A-F0-9]{5}|[A-F0-9]{10}\.[A-F0-9]{5}|[A-F0-9]{11}\.[A-F0-9]{5}$/ && $blacklist==2 ){

 $dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass, {PrintError => 0});
  
  if (!$dbh) {&error;}
      
      $sql = "SELECT id,from_address,to_address,subject,to_domain,from_domain from maillog WHERE id=\"$id\"";
  
      $sth = $dbh->prepare($sql);
      $sth->execute;
      @results = $sth->fetchrow;
      
      $from_address = $results[1];
      $to_address = $results[2];
      $subject = $results[3];
      $to_domain = $results[4];
      $from_domain = $results[5];
       
      if (!$results[0]) { 
      # msg id not found in database ...
      
        $sth->finish();
        $dbh->disconnect();
        
        # print ms like error ..  
 
          print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
          print "<html>";
	        print "<head>";
	        print "<title>Antispam Engine Error</title>";
	        print "</head>";
          print "<body>";   
          print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
          print "<center>";
          print ":( ... Something went wrong and we can't process your request right now.";
          print "<br>";
          print "</center>";
          print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
          print "</body>";
	        print "</html>";    
      exit;
      } 
      else {
      
       # check for trusted relay domain
     
  
       $sql1 = "SELECT domain from domain WHERE domain=\"$to_domain\"";
       
       $sth1 = $dbh->prepare($sql1);
       $sth1->execute;
       @result = $sth1->fetchrow;
      
       
      if (!$result[0]) { 
        $sth1->finish();
         
        # display failure page
       	print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      	print "<html>";
	      print "<head>";
	      print "<title>Antispam Engine</title>";
	      print "</head>";
      	print "<body>";
        print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
	      print "<div align='center'>";
	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
	      print "<THEAD>";
	      print "<TH>Antispam Learning</TH>";
	      print "</THEAD>";
	      print "</TABLE>";
	      print "<br><strong><font color=\"red\" size=\"6\" > NOT AUTHORIZED!</font></strong><br>";
	      print "<br>Your Account must be trusted to teach the Antispam Firewall!</br>";
	      print "</div>";
	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
	      print "</body>";
	      print "</html>";
        exit;         
      } 
      else 
      {
        if (is_private_ipv4($client)) {
    
        $sql2 = 'DELETE FROM blacklist WHERE ';
        $sql2 .= 'to_address = \''.$to_address;
        $sql2 .= '\'';
        $sql2 .= ' AND ';
        $sql2 .= 'to_domain = \''.$to_domain;
        $sql2 .= '\'';
        $sql2 .= ' AND ';
        $sql2 .= 'from_address = \''.$from_address.'\' ';
    
     #$dbh2 = DBI->connect("DBI:mysql:database=$db_name2;host=$db_host2", $db_user2, $db_pass2, {PrintError => 0});
     #if (!$dbh2) {&error;}
     $sqlQuery  = $dbh->prepare($sql2)or die "Can't prepare $sql2: $dbh2->errstr\n";
     $sqlQuery  = $dbh->prepare($sql1)or die "Can't prepare $sql1: $dbh2->errstr\n";
     $sth2 = $sqlQuery->execute or die "can't execute the query: $sqlQuery->errstr";
     $str = $sqlQuery->finish;
     
      print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
              print "<html>";
      	      print "<head>";
      	      print "<title>Antispam Engine</title>";
      	      print "</head>";
      	      print "<body>";
              print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "<div align='center'>";
      	      print "<TABLE WIDTH=60% align=\"center\" CELLPADDING=1 CELLSPACING=1 >";
      	      print "<THEAD>";
      	      print "<TH>Antispam Blacklist Entry</TH>";
      	      print "</THEAD>";
      	      print "</TABLE>";
      	      print "<br><strong><font color=\"red\" size=\"6\" >SENDER REMOVED FROM BLACKLIST!</font></strong><br>";
      	      print "<br>Sender <b>$from_address</b> has been removed from your personal blacklist.</br>";
      	      print "</div>";
      	      print "<hr size=\"1\" width=\"60%\" color=\"#ffa042\">  ";
      	      print "</body>";
	            print "</html>";  
             exit;
       } else {
       
            print "  
          <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">    
          <html>    
            <head>    
              <title>Antispam Engine</title>    
            </head>    
            <body>    
              <body bgcolor=\"#FFFFFF\">    
                <center>    
                  <form method=\"POST\" action=\"check-blacklist.cgi\">      
                    <table border=\"0\" cellpadding=\"2\" cellspacing=\"2\" width=\"60%\" align=\"center\">      
                      <tr>      
                        <td align=\"center\" colspan=\"2\">      
                          <hr size=\"1\" color=\"#ffa042\">
                        </td>      
                      </tr>        
                      <tr>        
                        <td align=\"center\" colspan=\"2\">
                          <font size=\"3\" face=\"arial\">
                          <strong>        
                              <img src=\"captcha.cgi\" border=\"1\">
                          </strong>        
                          </font>
                          </td>      
                      </tr>      
                      <tr>        
                        <td align=\"center\" colspan=\"2\">        
                        <font size=\"3\" face=\"Arial, Helvetica, sans-serif\">To undo blacklist sender <b>$from_address</b>, please enter your PIN and the text shown on the image as the verification code below.</font>
                        </td>      
                      </tr>           
                      <tr>        
                        <td align=\"right\" width=\"50%\">          
                          <input type=\"hidden\" name=\"id\" value=\"$id\" />          
                          <input type=\"hidden\" name=\"to_address\" value=\"$to_address\" />            
                          <input type=\"hidden\" name=\"to_domain\" value=\"$to_domain\" />
                          <input type=\"hidden\" name=\"from_address\" value=\"$from_address\" />              
                          <input type=\"hidden\" name=\"myhostname\" value=\"$myhostname\" />
                          <input type=\"hidden\" name=\"blacklist\" value=\"$blacklist\" />
                          PIN:&nbsp;<input type=\"password\" size=\"10\" name=\"pin\" autocomplete=\"off\" />                              
                        </td>
                        <td align=\"left\" width=\"50%\">Text:&nbsp;<input type=\"text\" size=\"10\" name=\"verifytext\"><input type=\"submit\" name=\"checktext\" value=\"Verify\"></td>     
                      </tr>      
                      <tr>        
                        <td align=\"center\" colspan=\"2\">        
                          <hr size=\"1\" color=\"#ffa042\">
                        </td>      
                      </tr>    
                  </form>    
                </center>    
              </body>    
          </html>";      
       }             
    }    
  }
}


sub mail_results {


      $mail_from = "no-reply\@".$myhostname;
 #        
      $message_subject = "[INFO] Antispam Blacklist Confirmation";
      $message_body ="\nPlease scroll down for the German translation.\n";
      $message_body = $message_body."Die deutsche Uebersetzung finden Sie im Anschluss an den englischen Text.\n\n";
      $message_body = $message_body."---- ENGLISH -----------\n\n";
			$message_body = $message_body."The Antispam service received a blacklist creation from your address for this Sender $from_address.\n\n";
			$message_body = $message_body."If you did this by mistake use the following link to undo this:\n\n"; 
      $message_body = $message_body."http://".$myhosturl."/cgi-bin/blacklist-sender.cgi?blacklist=2&id=".$id;
			$message_body = $message_body."\n\nIf you didn't made this request please forward this message to your IT Support.\n";
      $message_body = $message_body."This is an automatic generated message. Please do not reply.\n\n";
      $message_body = $message_body."Thank you.\n\n";
      $message_body = $message_body."--- DEUTSCH -----------\n\n";
      $message_body = $message_body."Das Antispam Service einen Blacklist Eintrag von Ihrer E-Mail Adresse fuer folgenden Sender $from_address erhalten.\n\n";
      $message_body = $message_body."Sollte es sich hier um ein Missverstaendniss handeln so koennen Sie diesen Eintrag hier wiederrufen:\n\n";
      $message_body = $message_body."http://".$myhosturl."/cgi-bin/blacklist-sender.cgi?blacklist=2&id=".$id;
      $message_body = $message_body."\n\nWurde dieser Request nicht von Ihnen erstellt? So leiten Sie bitte diese Nachricht an Ihren IT Support weiter.\n";
      $message_body = $message_body."Diese Meldung wurde vom System automatisch erstellt. Beantworten Sie diese bitte nicht.\n\n";
      $message_body = $message_body."Besten Dank.\n\n";
# 

		  my $msg = MIME::Lite->new(
			From    => $mail_from,
			To      => $to_address,
		  Subject => $message_subject,
			Type    => 'multipart/mixed',
		);

		$msg->attach(
			Type     => 'TEXT',
			Data     => $message_body,
		);

		$msg->send;
    
    }




sub error {
#print "Content-type: text/html\n\n";
      print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      print "<html>";
	    print "<head>";
	    print "<title>Antispam Engine Error</title>";
	    print "</head>";
      print "<body>";   
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "<center>";
      print ":( ... Something went wrong and we can't process your request right now.";
      print "<br>";
      print "Please try again later.";
      print "</center>";
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "</body>";
	    print "</html>";   
exit;	
}

