#!/usr/bin/perl
##############################################################################
#
# Modified for use with EMFABOX
# This script is an modification of learn-msg.cgi
# 
# Copyright (C) 2016  http://www.cycomptec.com 
#
############################################################################## 
use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use Sys::Hostname::Long;
use Data::Validate::IP qw(is_ipv4 is_ipv6 is_private_ipv4);
use NetAddr::IP;
use LWP::UserAgent;
use HTTP::Request::Common;
use Crypt::SSLeay;

# MAIN form 
print "Content-type: text/html\n\n";

my $query=new CGI;

# get ip address
$client= $ENV{REMOTE_ADDR};

$myhosturl = hostname_long;

$blacklist=0;
$id = param("id");

#Anti hack check
if ($id =~ /\|/) {&error;}


$blacklist = param("blacklist");
if ($blacklist != "1"){
$blacklist = "2";
}

# post data to listing.php       
my $ua = LWP::UserAgent->new;
my $url = "https://localhost/listing.php";
my $browser = LWP::UserAgent->new;
my $response = $browser->post($url,
  [
    'id'  => $id,
    'blacklist' => $blacklist,
    'ip' => $client,
    'url' => $myhosturl
  ],
);
die "Error: ", $response->status_line
unless $response->is_success;
if($response->content =~ m/BLACKLIST/) {
 print "<meta http-equiv=\"refresh\" content=\"0;URL=/confirm.html\">";     
} elsif($response->content =~ m/LEARNED/) {
 print "<meta http-equiv=\"refresh\" content=\"0;URL=/learned.html\">";
} else {
  {&error;}
}
exit;

sub error {
 print "<meta http-equiv=\"refresh\" content=\"0;URL=/error.html\">";
exit;	
}