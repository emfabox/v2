#!/usr/bin/perl
##############################################################################
#
# Modified for use with EMFABOX
# This script is an modification of preview-msg.cgi
# 
# Copyright (C) 2016  http://www.cycomptec.com 
#
############################################################################## 
use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use Sys::Hostname::Long;
use Data::Validate::IP qw(is_ipv4 is_ipv6 is_private_ipv4);
use NetAddr::IP;
use DBI;
use LWP::UserAgent;
use HTTP::Request::Common;
use Crypt::SSLeay;

# MAIN form 
print "Content-type: text/html\n\n";

$query = new CGI;

# get ip address
$client= $ENV{REMOTE_ADDR};

$myhosturl = hostname_long;

# get id & datenumber
$datenumber =  param("datenumber");
$id =  param("id");
$to =  param("to");
   
#Anti hack check
if ($id =~ /\|/) {
print "<meta http-equiv=\"refresh\" content=\"0;URL=/error.html\">";
exit;
}

# get mailscanner db config
open(MWCONFIG, '/etc/emfa/mwconfig');
my @mw_config = <MWCONFIG>;
close MWCONFIG;
s{^\s+|\s+$}{}g foreach @mw_config;
my($db_name) = $mw_config[0];
my($db_host) = $mw_config[1];
my($db_user) = $mw_config[2];
my($db_pass) = $mw_config[3];

# CHECK for vars

if ($id eq "" ){&error;}
if ($datenumber eq "" ){&error;}

# CHECK for valid id & datenumber 

if ($id =~ /^[A-F0-9]{9}\.[A-F0-9]{5}|[A-F0-9]{10}\.[A-F0-9]{5}|[A-F0-9]{11}\.[A-F0-9]{5}$/){
   if ($datenumber =~ /^([2-9]\d{3}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|(([2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00))0229)$/){
   
   # Verify if id is present in db
   
  $dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass, {PrintError => 0});
  if (!$dbh) {&error;}
  
  $sql = "SELECT id,from_address,to_address,subject from maillog WHERE id=\"$id\"";
  
  $sth = $dbh->prepare($sql);
  $sth->execute;
  @results = $sth->fetchrow;
      
  $from_address = $results[1];
  $to_address = $results[2];
  $subject = $results[3]; 
      
  if ($results[0]) { 
  $sth->finish();
  $dbh->disconnect(); 
  
  # post data to zzz.php       
  my $ua = LWP::UserAgent->new;
  my $url = "https://localhost/zzzz.php";
  my $browser = LWP::UserAgent->new;
  my $response = $browser->post($url,
  [
    'id'  => $id,
    'datenumber' => $datenumber,
    'to' => $to,
    'from' => $from_address,
    'ip' => $client,
    'url' => $myhosturl,
  ],
);
die "Error: ", $response->status_line
 unless $response->is_success;
if($response->content =~ m/OK/) {
     print "<meta http-equiv=\"refresh\" content=\"0;URL=/ok.php\">";    
} else {
     print "<meta http-equiv=\"refresh\" content=\"0;URL=/error.html\">";
}
exit;
      } else {&error;}
}  else {&error;}
}  else {&error;}


sub error {
 print "<meta http-equiv=\"refresh\" content=\"0;URL=/error.html\">";
exit;	
}