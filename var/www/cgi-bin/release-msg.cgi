#!/usr/bin/perl
##############################################################################
# CAPTCHA based on BumbleBeeWare.com 
##############################################################################
#
# Modified for use with EMFABOX
# This script is an modification of release-msg.cgi
# 
# Copyright (C) 2014  http://www.cycomptec.com 
#
############################################################################## 


use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use Sys::Hostname::Long;
use Data::Validate::IP qw(is_ipv4 is_ipv6 is_private_ipv4);
use NetAddr::IP;
use DBI;




# MAIN form 

print "Content-type: text/html\n\n";

$query    = new CGI;

# get a random number for the querry on the captcha.cgi
# a random number will force old browsers to update the image
# here we just used the time function, since it will be unique on every access of the page
$randomnumber = time;

# get ip address
$client= $ENV{REMOTE_ADDR};

# get hostname long & to build no-reply email address #

$myhosturl = hostname_long;
@alist = split(/\./, $myhosturl) ;
$myhostname= join('.',$alist[1],$alist[2]);



# get id & datenumber
  $datenumber = param("datenumber");
  $id = param("id");
  $to = param("to");
  
#Anti hack check
if ($id =~ /\|/) {
      print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      print "<html>";
	    print "<head>";
	    print "<title>Antispam Engine Error</title>";
	    print "</head>";
      print "<body>";   
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "<center>";
      print ":( ... Something went wrong and we can't process your request right now.";
      print "<br>";
      print "Please try again later.";
      print "</center>";
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "</body>";
	    print "</html>";  
exit;
}

# get mailscanner db config
open(MWCONFIG, '/etc/emfa/mwconfig');
my @mw_config = <MWCONFIG>;
close MWCONFIG;
s{^\s+|\s+$}{}g foreach @mw_config;
my($db_name) = $mw_config[0];
my($db_host) = $mw_config[1];
my($db_user) = $mw_config[2];
my($db_pass) = $mw_config[3];

# CHECK for vars

if ($id eq "" ){&error;}
if ($datenumber eq "" ){&error;}

# CHECK for valid id & datenumber 

if ($id =~ /^[A-F0-9]{9}\.[A-F0-9]{5}|[A-F0-9]{10}\.[A-F0-9]{5}|[A-F0-9]{11}\.[A-F0-9]{5}$/){
   if ($datenumber =~ /^([2-9]\d{3}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|(([2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00))0229)$/){
   
   # Verify if id is present in db
   
  $dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass, {PrintError => 0});
  if (!$dbh) {&error;}
  
  $sql = "SELECT id,from_address,to_address,subject from maillog WHERE id=\"$id\"";
  
  $sth = $dbh->prepare($sql);
      $sth->execute;
      @results = $sth->fetchrow;
      
      $from_address = $results[1];
      $to_address = $results[2];
      $subject = $results[3]; 
      
        if (!$results[0]) { 
        $sth->finish();
        $dbh->disconnect();  
 
        # redirect to failure page
        print "<meta http-equiv=\"refresh\" content=\"0;URL=/notreleased.html\">";
      }
      
      else {
      
      # This subroutine checks whether the address belongs to any of the private IPv4 networks - 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16 - as defined by RFC 5735.

#if (is_private_ipv4($client)) {

    #print "Looks like an IPv4 address as defined by RFC 5735";
          
  
    $sendmail = "/usr/sbin/sendmail.postfix";
    $msgtorelease = "/var/spool/MailScanner/quarantine/$datenumber/spam/$id";
    open(MAIL, "|$sendmail -t <$msgtorelease") or die "Cannot open $sendmail: $!";
    
    close(MAIL);
    
    $sth->finish();
$dbh->disconnect();
    
   # &mail_results;
    
    # redirect to success page
    print "<meta http-equiv=\"refresh\" content=\"0;URL=/released.html\">";
    

          

     
} else {
     die "Error in datenumber syntax";  }
  

} else {
  die "Error in id syntax";
 }
}

sub error {
      print "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
      print "<html>";
	    print "<head>";
	    print "<title>Antispam Engine Error</title>";
	    print "</head>";
      print "<body>";   
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "<center>";
      print ":( ... Something went wrong and we can't process your request right now.";
      print "<br>";
      print "Please try again later.";
      print "</center>";
      print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
      print "</body>";
	    print "</html>";   
exit;	
}

# e-mail subs

sub mail_results {
	
      open( MAIL, "|$sendmail -t" )or die "Cannot open $sendmail: $!";
      print MAIL "Subject: [INFO] Message has been released from quarantine right now.\n";
      print MAIL "From: no-reply\@$myhostname\n";
      print MAIL "To: $to_address\n";
      print MAIL "Reply-to: $to_address\n\n";
      print MAIL "\n\n";
      print MAIL "Please scroll down for the German translation.\n";
      print MAIL "Die deutsche Uebersetzung finden Sie im Anschluss an den englischen Text.\n\n";
      print MAIL "---- ENGLISH -----------\n\n";
      print MAIL "This message has been released.\n\n";
      print MAIL "From: $from_address\n";
      print MAIL "Subject: $subject\n";
      print MAIL "Message id: $id\n";
      print MAIL "Date code: $datenumber\n\n";
      print MAIL "Your Client IP-Address: $client\n\n\n";
      print MAIL "This is an automatic generated message. Please do not reply.\n\n";
      print MAIL "--\n";
      print MAIL "Thank you.\n\n";
      print MAIL "--- DEUTSCH -----------\n\n";
      print MAIL "Diese Nachricht wurde nun ausgeliefert.\n\n";
      print MAIL "Absender: $from_address\n";
      print MAIL "Betreff: $subject\n";
      print MAIL "Nachrichten ID: $id\n";
      print MAIL "Datums Code: $datenumber\n\n";
      print MAIL "Client Computer IP-Adresse: $client\n\n\n";
      print MAIL "Diese Meldung wurde vom System automatisch erstellt. Beantworten Sie diese bitte nicht.\n\n";
      print MAIL "--\n";
      print MAIL "Danke.\n";
      close MAIL;
}
