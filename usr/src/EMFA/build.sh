#!/bin/bash
#set -x
########################################################################
# EMFABOX Install                                                      #
# WORKS WITH                                                           #
# CentOS 6.7 NETINSTALL 64 BIT ONLY                                    #
########################################################################
# Copyright (C) 2014, 2015, 2016  http://www.cycomptec.com             #
########################################################################
#
# Version 2016-03-06
#
########################################################################

# +---------------------------------------------------+
# Variables
# +---------------------------------------------------+
MYVERSION="2.0.0.0"


# DIRS
SOURCE_DIR='/usr/src/EMFA'
VAR_DIR='/etc/emfa'
DB_CONF='/etc/DB-Config'
VERSIONS_FILE='/etc/emfa/versions'
VAR_SOURCE='/opt/emfa/install/conf'
VAR_TRACK='/opt/emfa/id'
WWW_DIR='/var/www/html'
INSTALL_LOG_DIR='/opt/emfa/logs'
UPDATE_DIR='/opt/emfa/update'
BACKUP_DIR='/opt/emfa/backup'
MY_LIB_DIR='/usr/local/lib/emfa'
MY_IXED_DIR='/opt/emfa/ixed'
EMFA_LOG_DIR='/var/log/emfa'

# MailScanner
THISCURRPMDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
CURL='/usr/bin/curl'
YUM='/usr/bin/yum'

# DATABASE
DBHOST="127.0.0.1"
MY_DB_HOST="localhost"
MYSQL_PORT="3306"

# MAILSCANNER
MAILSCANNER_DBNAME="mailscanner"
MAILSCANNER_DBUSER="mailwatch"

# SQLGREY
SQLGREY_DBNAME="sqlgrey"
SQLGREY_DNBUSER="sqlgrey"

# FUZZYOCR
#FUZZYOCR_DBUSER="fuzzyocr"
#FUZZYOCR_DBNAME="FuzzyOcr"

# SA_BAYES
SA_BAYES_DBUSER="sa_user"
SA_BAYES_DBNAME="sa_bayes"

#WEBTOKEN
WEB_TOKEN_DBUSER="webtoken"
WEB_TOKEN_DBNAME="webtoken"
#
SA_BAYES_DBUSER="sa_user"
SA_BAYES_DBNAME="sa_bayes"
#
CLUEBRINGER_DBUSER="cluebringer"
CLUEBRINGER_DBNAME="cluebringer"

OPENDMARC_DBNAME="opendmarc"
OPENDMARC_DBUSER="opendmarc"




#########################################################################
# EFA Base (https://efa-project.org)
version="3.0.0.9"
logdir="/var/log/emfa"
gitdlurl="https://raw.githubusercontent.com/E-F-A/v3/$version/build"
password="EMF@B0x2o16"
mirror="http://dl.efa-project.org"
mirrorpath="/build/$version"
yumexclude="kernel* mysql* postfix* mailscanner* clamav* clamd* open-vm-tools*"

IMAGECEBERUSVERSION="1.1"
SPAMASSASSINVERSION="3.4.0a"
PYZORVERSION="0.7.0"
#########################################################################



# +---------------------------------------------------+
# Check target platform and environment.
# +---------------------------------------------------+
# Required by OpenVZ:
# Make sure others can read-write /dev/null and /dev/*random, so that it won't
# interrupt installation.
# +---------------------------------------------------+
chmod go+rx /dev/null /dev/*random &>/dev/null

# Function used to Wait for n seconds
timewait () {
  DELAY=$1
  sleep $DELAY
}



# +---------------------------------------------------+
# Pre-build
# +---------------------------------------------------+
emfa_prebuild () {
    # mounting /tmp without nosuid and noexec while building as it breaks building some components.
    mount -o remount rw /tmp
    
    # override nameservers
    # during build
      echo "nameserver 8.8.8.8" > /etc/resolv.conf
      echo "nameserver 8.8.4.4" >> /etc/resolv.conf
    
    # SQLGREY
    # hosts
      echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4" > /etc/hosts
      echo "::1         localhost6.localdomain6    localhost6">>/etc/hosts
    ## Add Host entry
      echo $eth0ip $hostf $hosts >> /etc/hosts 
      
 
}
# +---------------------------------------------------+ 


# +---------------------------------------------------+
# add EFA Repo
# +---------------------------------------------------+
emfa_efarepo () {
   rpm --import https://dl.efa-project.org/rpm/RPM-GPG-KEY-E.F.A.Project
   cd /etc/yum.repos.d/
   /usr/bin/wget --no-check-certificate $mirror/rpm/EFA.repo
   yum install -y unrar perl-IP-Country perl-Mail-SPF-Query perl-Net-Ident perl-Mail-ClamAV
}
# +---------------------------------------------------+



# +---------------------------------------------------+
# add epel repository
# +---------------------------------------------------+
emfa_epelrepo () {
   rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6
   yum install epel-release -y
   yum install -y tnef perl-BerkeleyDB perl-Convert-TNEF perl-Filesys-Df perl-File-Tail perl-IO-Multiplex perl-Net-Server perl-Net-CIDR perl-File-Tail perl-Net-Netmask perl-NetAddr-IP re2c
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Postfix RPMS
# https://emfabox.s3.amazonaws.com/build/rpm/postfix-perl-scripts-3.0.4-1.el6.x86_64.rpm
# https://emfabox.s3.amazonaws.com/build/rpm/postfix-debuginfo-3.0.4-1.el6.x86_64.rpm
# https://emfabox.s3.amazonaws.com/build/rpm/postfix-3.0.4-1.el6.x86_64.rpm
# +---------------------------------------------------+
emfa_postfix_rpm () {
mkdir -p /usr/src/EMFA/install/postfix
cd /usr/src/EMFA/install/postfix
/usr/bin/wget --no-check-certificate https://emfabox.s3.amazonaws.com/build/rpm/postfix-3.0.4-1.el6.x86_64.rpm
/usr/bin/wget --no-check-certificate https://emfabox.s3.amazonaws.com/build/rpm/postfix-perl-scripts-3.0.4-1.el6.x86_64.rpm
/usr/bin/wget --no-check-certificate https://emfabox.s3.amazonaws.com/build/rpm/postfix-debuginfo-3.0.4-1.el6.x86_64.rpm

# bug after upgrade to postfix 3.0.4 
# warning: /etc/postfix/main.cf, line 683: overriding earlier entry: inet_protocols=all 
# disable inet_protocols = all in main.cf
sed -i 's/^inet_protocols = all/#inet_protocols = all/g' /etc/postfix/main.cf

yum -y install postfix-3.0.4-1 postfix-perl-scripts-3.0.4-1 postfix-debuginfo-3.0.4-1

}

# +---------------------------------------------------+
# Update system before we start
# +---------------------------------------------------+
emfa_upgradeOS () {
    yum -y upgrade
}
# +---------------------------------------------------+



# +---------------------------------------------------+
# emfa rpms
# +---------------------------------------------------+

emfa_rpms () {

# remove firmware
yum remove -y aic94xx-firmware ql2400-firmware libertas-usb8388-firmware zd1211-firmware ql2200-firmware ipw2200-firmware iwl5150-firmware iwl6050-firmware iwl6000g2a-firmware iwl6000-firmware ivtv-firmware xorg-x11-drv-ati-firmware atmel-firmware iwl4965-firmware iwl3945-firmware rt73usb-firmware ql23xx-firmware bfa-firmware iwl1000-firmware iwl5000-firmware iwl100-firmware ql2100-firmware ql2500-firmware rt61pci-firmware ipw2100-firmware alsa-utils alsa-lib

# remove regular rpms
yum remove -y php php-common php-mysql php-devel php-pear php-pdo sendmail
 
yum install -y anacron aspell audit bind bind-chroot bind-utils binutils caching-nameserver crash db4-devel dos2unix gcc glibc-devel httpd iptables libaio links logwatch lynx m2crypto make man-pages man-pages-overrides mdadm microcode_ctl mod_ssl mysql-server ntp openssh-clients OpenIPMI openssl openssh patch perl-DBD-MySQL perl-Date-Calc php-cli php-devel php-mbstring php-pecl-Fileinfo php-mysqli php-pear php-pdo policycoreutils postfix cyrus-sasl-plain rpm rpm-build setarch spamassassin shellinabox stunnel sudo symlinks sysstat tar time unzip unix2dos vim-enhanced wget which yum-security yum-utils zip libtool-ltdl policycoreutils-python php-process php-xml antiword clamav clamav-db clamav-devel clamd clamav-update clamav-server clamav-data-empty GeoIP GeoIP-devel libmcrypt php-mcrypt perl-Filesys-Df perl-DBI pyzor perl-Razor-Agent perl-DBD-SQLite perl-CPAN perl-Time-HiRes perl-URI perl-Digest-SHA1 perl-Digest-HMAC perl-Net-DNS perl-Mail-SPF tnef perl-File-Tail perl-Archive-Zip perl-Compress-Raw-Zlib perl-Compress-Zlib perl-Convert-BinHex perl-Convert-TNEF perl-ExtUtils-MakeMaker perl-File-Temp perl-HTML-Parser perl-HTML-Tagset perl-MailTools perl-Net-CIDR perl-Net-IP perl-OLE-Storage_Lite perl-Pod-Escapes perl-Sys-Hostname-Long perl-Sys-SigAction perl-Test-Harness perl-Test-Pod perl-Test-Simple perl-Inline
    
yum install -y bzip2-devel gcc libtool-ltdl man-pages man-pages-overrides openssl-devel patch time unzip zip tmpwatch ntpdate

yum install -y jwhois nmap telnet yum-plugin-downloadonly mailx tcptraceroute traceroute htop lsof iftop dstat lua lua-devel pkgconfig asciidoc lsyncd rsync

yum install -y libc-client php-gd php-imap php-pear-Auth-SASL php-pear-Mail php-pear-Net-Curl php-pear-Net-DNS2 php-pear-Net-SMTP php-pear-Net-SMTP php-pear-Net-Socket php-ldap openldap

yum install -y pypolicyd-spf python-pyspf openldap-devel p7zip pcre gd re2c

yum install -y perl-BerkeleyDB perl-CGI perl-ExtUtils-CBuilder perl-Geography-Countries perl-IO-Multiplex perl-Module-Build perl-Net-CIDR-Lite perl-Net-Netmask perl-Net-Server perl-Data-Validate-IP perl-Data-Validate-Domain perl-Data-Validate-Email perl-Data-Validate-URI perl-Email-Address perl-Net-Domain-TLD perl-MIME-Types perl-Email-Date-Format perl-MIME-Lite perl-Error perl-Test-Pod perl-URI pyzor tnef  perl-Test-Simple

yum install -y perl-Archive-Zip perl-CPAN perl-Convert-BinHex perl-Convert-TNEF perl-DBD-SQLite perl-DBI perl-Digest-HMAC perl-Digest-SHA1 perl-Filesys-Df perl-HTML-Parser perl-HTML-Tagset perl-IO-stringy perl-Inline perl-MIME-tools perl-Mail-DKIM perl-Mail-IMAPClient perl-Mail-SPF perl-Net-CIDR perl-Net-DNS perl-Net-IP perl-OLE-Storage_Lite perl-Razor-Agent perl-Sys-Hostname-Long perl-Sys-SigAction perl-Crypt-OpenSSL-Bignum perl-Crypt-OpenSSL-RSA perl-Crypt-OpenSSL-Random perl-Digest-SHA perl-NetAddr-IP perl-Parse-RecDescent  
    
yum install -y mailgraph rrdtool-perl

yum install -y python-dns python-pydns python-setuptools python-IPy

# prepare for cluster option
yum install -y lsyncd

# prepare for git updates
yum install -y git perl-Git

# use fail2ban
yum install -y fail2ban

# ipset
yum install -y ipset ipset-devel

# mc
yum install -y mc

yum install -y getmail cyrus-sasl-md5 cyrus-sasl-gssapi
    
yum install -y crypto-utils cyrus-imapd cyrus-imapd-utils
    
yum install -y yum-plugin-remove-with-leaves yum-utils tre tre-devel crm114 rbldnsd
    
# MailScanner --> MailScanner-4.85.2-3 #
yum install -y binutils gcc glibc-devel libaio make man-pages man-pages-overrides patch rpm tar time unzip which zip libtool-ltdl perl curl wget openssl openssl-devel bzip2-devel
    
yum install -y perl-Archive-Tar perl-Archive-Zip perl-Compress-Raw-Zlib perl-Compress-Zlib perl-Convert-BinHex perl-Convert-TNEF perl-CPAN perl-Data-Dump perl-DBD-SQLite perl-DBI perl-Digest-HMAC perl-Digest-SHA1 perl-Env perl-ExtUtils-MakeMaker perl-File-ShareDir-Install perl-File-Temp perl-Filesys-Df perl-Getopt-Long perl-IO-String perl-IO-stringy perl-HTML-Parser perl-HTML-Tagset perl-Inline perl-IO-Zlib perl-Mail-DKIM perl-Mail-IMAPClient perl-Mail-SPF perl-MailTools perl-MIME-tools perl-Net-CIDR perl-Net-DNS perl-Net-DNS-Resolver-Programmable perl-Net-IP perl-OLE-Storage_Lite perl-Pod-Escapes perl-Pod-Simple perl-Scalar-List-Utils perl-Storable perl-Pod-Escapes perl-Pod-Simple perl-Razor-Agent perl-Sys-Hostname-Long perl-Sys-SigAction perl-Test-Manifest perl-Test-Pod perl-Time-HiRes perl-TimeDate perl-URI pyzor unrar tnef unrar
    
yum install -y net-snmp net-snmp-libs monit
    
yum install -y sshpass

# php-soap for ispconfig
yum install -y php-soap

# ldap search for zimbra
yum install -y openldap-clients

#mtpolicy pre install
yum install -y perl-Cache-Memcached perl-String-CRC32 perl-Config-General perl-JSON perl-Moose perl-MooseX-Getopt  perl-MooseX-Role-Parameterized  perl-Tie-IxHash  perl-Time-Piece memcached perl-namespace-autoclean perl-MailTools

# rkhunter php-pecl-mailparse
yum install -y rkhunter php-pecl-mailparse perl-Crypt-SSLeay

# makeself 
yum install -y makeself
  
# set date 
ntpdate -s ntp.ubuntu.com      

}
# +---------------------------------------------------+



# +---------------------------------------------------+
# Building Perl Modules
# +---------------------------------------------------+
emfa_cpan () {
  clear
echo -e "\e[1;42m"
echo -e '\n\n'
cat <<EOF

***********************************************************************
* Building Perl Modules                                               *
*                                                                     *
* This may take a while ... :(                                        *
***********************************************************************
EOF

sleep 5s

echo -e "\e[0m"

clear

YUM='/usr/bin/yum';
CPANOPTION=1

# 2015-04-03
# MAILSCANNER STYLE

CARRAY=();
CARRAY+=('CPANPLUS');
CARRAY+=('YAML');
CARRAY+=('Archive::Tar');
CARRAY+=('Archive::Zip');
CARRAY+=('bignum');
CARRAY+=('Business::ISBN');
CARRAY+=('Business::ISBN::Data');
CARRAY+=('Cache::FastMmap');
CARRAY+=('Carp');
CARRAY+=('CGI::Lite');
CARRAY+=('Compress::Raw::Zlib');
CARRAY+=('Compress::Zlib');
CARRAY+=('Config::IniFiles');
CARRAY+=('Convert::BinHex');
CARRAY+=('Convert::TNEF');
CARRAY+=('Data::Dump');
CARRAY+=('Data::Dumper');
CARRAY+=('Data::Validate::Domain');
CARRAY+=('Data::Validate::Email');
CARRAY+=('Data::Validate::IP');
CARRAY+=('Data::Validate::URI');
CARRAY+=('Date::Parse');
CARRAY+=('DB_File');
CARRAY+=('DBD::SQLite');
CARRAY+=('DBD::mysql');
CARRAY+=('DBI');
CARRAY+=('Digest');
CARRAY+=('Digest::HMAC');
CARRAY+=('Digest::MD5');
CARRAY+=('Digest::SHA1 ');
CARRAY+=('DirHandle');
CARRAY+=('Encode::Detect');
CARRAY+=('Env');
CARRAY+=('Error');
CARRAY+=('Exporter');
CARRAY+=('ExtUtils::CBuilder');
CARRAY+=('ExtUtils::MakeMaker');
CARRAY+=('ExtUtils::ParseXS');
CARRAY+=('Fcntl');
CARRAY+=('File::Basename');
CARRAY+=('File::Copy');
CARRAY+=('File::Path');
CARRAY+=('File::ShareDir::Install');
CARRAY+=('File::Slurp');
CARRAY+=('File::Spec');
CARRAY+=('File::Temp');
CARRAY+=('File::Tail');
CARRAY+=('FileHandle');
CARRAY+=('Filesys::Df');
CARRAY+=('Encoding::FixLatin');
CARRAY+=('Getopt::Long');
CARRAY+=('HTML::Entities');
CARRAY+=('HTML::Parser');
CARRAY+=('HTML::Tagset');
CARRAY+=('HTML::TokeParser');
CARRAY+=('HTTP::Cookies');
CARRAY+=('HTTP::Request');
CARRAY+=('Inline');
CARRAY+=('Inline::C');
CARRAY+=('IO');
CARRAY+=('IO::File');
CARRAY+=('IO::Pipe');
CARRAY+=('IO::String');
CARRAY+=('IO::Stringy');
CARRAY+=('IO::Zlib');
CARRAY+=('IP::Country');
CARRAY+=('IP::Country::Fast');
CARRAY+=('IO::Socket::IP');
CARRAY+=('LWP');
CARRAY+=('Mail::DKIM');
CARRAY+=('Mail::Field');
CARRAY+=('Mail::Header');
CARRAY+=('Mail::IMAPClient');
CARRAY+=('Mail::Internet');
CARRAY+=('Mail::SPF');
CARRAY+=('Mail::SPF::Query');
CARRAY+=('Mail::ClamAV');
CARRAY+=('Math::BigInt');
CARRAY+=('Math::BigRat');
CARRAY+=('MIME::Base64');
CARRAY+=('MIME::Base64::Perl');
CARRAY+=('MIME::Decoder');
CARRAY+=('MIME::Decoder::UU');
CARRAY+=('MIME::Head');
CARRAY+=('MIME::Parser');
CARRAY+=('MIME::QuotedPrint');
CARRAY+=('MIME::Tools');
CARRAY+=('MIME::WordDecoder');
CARRAY+=('Module::Build');
CARRAY+=('Net::CIDR');
CARRAY+=('Net::CIDR::Lite');
CARRAY+=('Net::DNS');
CARRAY+=('Net::DNS::Nameserver');
CARRAY+=('Net::DNS::Resolver::Programmable');
CARRAY+=('Net::Ident');
CARRAY+=('Net::IP');
CARRAY+=('Net::LDAP');
CARRAY+=('Net::Patricia');
CARRAY+=('Net::Server');
CARRAY+=('Net::WhoisNG');
CARRAY+=('Net::Whois::Parser');
CARRAY+=('NetAddr::IP');
CARRAY+=('OLE::Storage_Lite');
CARRAY+=('Pod::Escapes');
CARRAY+=('Parse::RecDescent');
CARRAY+=('PHP::Session');
CARRAY+=('Pod::Simple');
CARRAY+=('POSIX');
CARRAY+=('Scalar::Util');
CARRAY+=('Socket');
CARRAY+=('Storable');
CARRAY+=('String::Util');
CARRAY+=('Sys::Hostname::Long');
CARRAY+=('Sys::SigAction');
CARRAY+=('Sys::Syslog');
CARRAY+=('Test::Harness');
CARRAY+=('Test::Manifest');
CARRAY+=('Test::Pod');
CARRAY+=('Test::Simple');
CARRAY+=('Text::Balanced');
CARRAY+=('Time::HiRes');
CARRAY+=('Time::localtime');
CARRAY+=('URI');
CARRAY+=('version');
CARRAY+=('Geo::IP::PurePerl');
CARRAY+=('Geo::IP');
CARRAY+=('List::Util');
CARRAY+=('Config::IniFiles');
CARRAY+=('Cache::FastMmap');
#CARRAY+=('Mail::SpamAssassin');
#PERL_MM_USE_DEFAULT=1 perl -MCPAN -e "CPAN::Shell->force(qw(install Geo::IP::PurePerl Geo::IP));"


mkdir -p /root/.cpan/CPAN
cd /root/.cpan/CPAN
wget --no-check-certificate -O MyConfig.pm https://bitbucket.org/emfabox/beta/raw/ed6dfed7b49886afa84ac9ec6ca1a6f0770f9457/src/MyConfig.pm
      
#perl -MCPAN -e 'my $c = "CPAN::HandleConfig"; $c->load(doit => 1, autoconfig => 1); $c->edit(prerequisites_policy => "follow"); $c->edit(build_requires_install_policy => "yes"); $c->commit'

export PERL_MM_USE_DEFAULT=1 

for i in "${CARRAY[@]}"
do
        perldoc -l $i >/dev/null 2>&1
        if [ $? != 0 ]; then
                echo "$i is missing. Trying to install via Yum first ..."; echo;
                THING="'perl($i)'";
                $YUM -y install $THING
        fi
done

for i in "${CARRAY[@]}"
do
        perldoc -l $i >/dev/null 2>&1
        if [ $? != 0 ]; then
                        echo "$i is missing. Installing via CPAN ..."; echo;
                        sleep 1
                        PERL_MM_USE_DEFAULT=1 perl -MCPAN -e "CPAN::Shell->force(qw(install $i));"
                echo "$i => OK";
        fi
done



# only form cpan ..

#perl -MCPAN -e "CPAN::Shell->force(qw(install Exporter));"
PERL_MM_USE_DEFAULT=1 perl -MCPAN -e "CPAN::Shell->force(qw(install Mail::SpamAssassin));"

#New plugins
#-----------
#There are three new plugins added with this release:
#  Mail::SpamAssassin::Plugin::TxRep
#  Mail::SpamAssassin::Plugin::PDFInfo
#  Mail::SpamAssassin::Plugin::URILocalBL

#clear

echo "done"      

timewait 3
    
### Mail::SpamAssassin ###
# Mail::DKIM requires the following Perl modules:
#  Crypt::OpenSSL::RSA
#  Digest::SHA
#  Mail::Address
#  MIME::Base64
#  Net::DNS

 
}

# +---------------------------------------------------+


# +---------------------------------------------------+
# Create Password
# +---------------------------------------------------+

emfa_build_config () {

touch /opt/emfa/id/build_config

# check emfa dir
if [ ! -d /etc/emfa ] ; then
  mkdir -p  /etc/emfa
fi

# generate versions file
if [ ! -f $VERSIONS_FILE ] ; then
  touch  $VERSIONS_FILE
fi

SERIAL=`uuidgen -r | tr '[a-z]' '[0-9]'`

echo ${SERIAL}${HWADDR} > /etc/emfa/serial
IMAGEDATE="`date +%Y%m%d-%s`"

# generate key
EMFAKEY=`uuidgen -r | tr '[0-9]' '[0-9]'`
printf "${EMFAKEY}" > /etc/emfa/EMFAKey
chmod 0644 /etc/emfa/EMFAKey

PASS=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)
PASS2=$(tr -cd '[:alnum:]' < /dev/urandom | tr -dc _A-Z-a-z-0-9 | head -c15 ) 
PASS3=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)
PASS4=$(tr -cd '[:alnum:]' < /dev/urandom | tr -dc _A-Z-a-z-0-9 | head -c15 ) 
PASS5=$(tr -cd '[:alnum:]' < /dev/urandom | tr -dc _A-Z-a-z-0-9 | head -c15 )
PASS6=$(tr -cd '[:alnum:]' < /dev/urandom | tr -dc _A-Z-a-z-0-9 | head -c15 )
PASS7=$(tr -cd '[:alnum:]' < /dev/urandom | tr -dc _A-Z-a-z-0-9 | head -c15 )
PASS8=$(tr -cd '[:alnum:]' < /dev/urandom | tr -dc _A-Z-a-z-0-9 | head -c15 )   

# generate unique like Hostname

#hn="cat /dev/urandom | env LC_CTYPE=C tr -cd 'A-Z0-9' | head -c 6"

water_mark=$(cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 15 | head -n 1)

# additional infos
VAR_MEM=$(awk -F":" '$1~/MemTotal/{print $2}' /proc/meminfo )

# TRANSPORT UUID

TRANSPORTKEY=`uuidgen -t | tr '[0-9]' '[0-9]'`

cat << EOF > /etc/emfa/variables.conf
#############################################################
# EMFABox by Cyber Computer Technology Inc.                 #
# www.emfabox.com                                           #
# autogenerated do not edit by hand!!!                      #
#############################################################

EMFABOX_VERSION="${MYVERSION}"
EMFABOX_INSTALLDATE="${InstallDate}"
EMFABOX_MEMORY="${VAR_MEM}"
EMFABOX_AUTOUPDATE="no"

DBHOST="${MY_DB_HOST}"

EMFABOX_TIME_UTC=""
EMFABOX_USE_NTP=""
EMFABOX_NTP_SERVER_LIST=""

EMFABOX_HTTP_PORT="80"

EMFABOX_DEFAULT_0=""
EMFABOX_DEFAULT_1=""
EMFABOX_DEFAULT_2=""
EMFABOX_DEFAULT_3=""
EMFABOX_DEFAULT_4=""
EMFABOX_DEFAULT_5=""
EMFABOX_DEFAULT_6=""
EMFABOX_DEFAULT_7=""
EMFABOX_DEFAULT_8=""
EMFABOX_DEFAULT_9=""



EMFABOX_FQHOSTNAME="${MYHOSTNAME}"
EMFABOX_HOSTNAME="${MY_SUB_DOMAIN}"
EMFABOX_DOMAIN="${MY_DOMAIN}"

EMFABOX_ADMIN_MAIL=""
EMFABOX_ADMIN_USER="admin"
EMFABOX_MAIL_DOMAIN=""

EMFABOX_IANA_CODE=""
EMFABOX_KEYBOARD=""
EMFABOX_TIMEZONE=""
EMFABOX_MAILSERVER=""
EMFABOX_ORGNAME=""
EMFABOX_ORG_LONG_NAME=""
EMFABOX_ORG_WWW=""

EMFABOX_COUNTRY_CODE=""
EMFABOX_STATE_PROVINCE=""
EMFABOX_CITY=""
EMFABOX_SSL_ORG=""
EMFABOX_ORGUNIT=""
EMFABOX_SSL_CN=""
EMFABOX_SSL_MAIL=""

EMFABOX_VMWARE_TOOLS=""

EMFABOX_CONFIGURED=""

EMFABOX_DNSMASQ=""
EMFABOX_UNBOUND=""
EMFABOX_NAMED=""

EMFABOX_SQLGREY="yes"
EMFABOX_SPF="0"
EMFABOX_DKIM="yes"

EMFABOX_MAILWATCH_HOME=""
EMFABOX_MASTER_HOME="/var/www/html/master"

EMFABOX_LSYNC=""
EMFABOX_LSYNC_VARS=""

EMFABOX_DMARC=""
EMFABOX_DMARC_REPORT=""
EMFABOX_DMARC_REPORT_USER=""
EMFABOX_DMARC_REPORT_DOMAIN=""

EMFABOX_TRANSPORT_UUID="${TRANSPORTKEY}"

EMFABOX_STARTUP_SPAM_SCORE="5"
EMFABOX_STARTUP_HIGH_SPAM_SCORE="10"

IPADDRESS=`ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'`
NETMASK=`/sbin/ifconfig eth0 | sed -rn '2s/ .*:(.*)$/\1/p'`
GATEWAY="$(/sbin/ip route | awk '/default/ { print $3 }')"
NAMESERVER_1="127.0.0.1"
NAMESERVER_2="8.8.8.8"
NAMESERVER_3="8.8.4.4"

EMFA_SEARCH_DOMAIN=""

EMFABOX_STUNNEL=""
EMFABOX_STUNNEL_CONF=""
EMFABOX_STUNNEL_PORT=""
EMFABOX_STUNNEL_CERT=""

MAILSCANNER_DBNAME="mailscanner"
MAILSCANNER_DBUSER="mailwatch"
#MAILSCANNER_DBPASS="${PASS}"
MAILSCANNER_DBPASS="M@IlwAtchB1E"

SQLGREY_DBNAME="sqlgrey"
SQLGREY_DNBUER="sqlgrey"
SQLGREY_DBPASS="${PASS2}"

FUZZYOCR_DBUSER="fuzzyocr"
FUZZYOCR_DBNAME="FuzzyOcr"
FUZZYOCR_DBPASS="${PASS4}"

SA_BAYES_DBUSER="sa_user"
SA_BAYES_DBNAME="sa_bayes"
SA_BAYES_DBPASS="${PASS5}"

WEB_TOKEN_DBUSER="webtoken"
WEB_TOKEN_DBNAME="webtoken"
WEB_TOKEN_DBPASS="${PASS6}"

CLUEBRINGER_DBUSER="cluebringer"
CLUEBRINGER_DBNAME="cluebringer"
CLUEBRINGER_DBPASS="${PASS7}"

OPENDMARC_DBUSER="opendmarc"
OPENDMARC_DBNAME="opendmarc"
OPENDMARC_DBPASS="${PASS8}"

MYSQL_ROOTPASS="${PASS3}"

WATERMARK_SECRET="${water_mark}"

EOF
#####

# create DB-Config

cat << EOF > /etc/DB-Config
SAUSERSQLPWD:${PASS5}
MAILWATCHSQLPWD:${PASS}
SQLGREYSQLPWD:${PASS2}
TOKENSQLPWD:${PASS6}
MYSQLROOTPWD:${PASS3}
WEBTOKENDBPWD:${PASS6}
CLUEBRINGERDBPWD:${PASS7}
EOF

### MYSQL
cat << EOF > /etc/emfa/sqlconfig
root
${PASS3}
EOF


### MW

cat << EOF > /etc/emfa/mwconfig
$MAILSCANNER_DBNAME
$MY_DB_HOST
$MAILSCANNER_DBUSER
${PASS}
$WEB_TOKEN_DBNAME
EOF

### SQLGREY
cat << EOF > /etc/emfa/greyconfig
$SQLGREY_DBNAME
$MY_DB_HOST
$SQLGREY_DBUSER
${PASS2}
EOF

### CLUEBRINGER
cat << EOF > /etc/emfa/policydconfig
$CLUEBRINGER_DBNAME
$MY_DB_HOST
$CLUEBRINGER_DBUSER
${PASS7}
EOF

### TOKEN
cat << EOF > /etc/emfa/wtconfig
$WEB_TOKEN_DBNAME
$MY_DB_HOST
$WEB_TOKEN_DBUSER
${PASS6}
EOF
###

### OPENDMARC
cat << EOF > /etc/emfa/dmarcconfig
$OPENDMARC_DBNAME
$MY_DB_HOST
$OPENDMARC_DBUSER
${PASS8}
EOF
###


### SA
cat << EOF > /etc/emfa/saconfig
$SA_BAYES_DBNAME
$MY_DB_HOST
$SA_BAYES_DBUSER
${PASS5}
EOF
###

cd  /etc/emfa/
chmod 0644 /etc/DB-Config
chmod 0644 /etc/emfa/mwconfig
chmod 0644 /etc/emfa/greyconfig
chmod 0644 /etc/emfa/wtconfig
chmod 0644 /etc/emfa/saconfig

chown apache:apache variables.conf
chmod 0644 /etc/emfa/variables.conf


}
# +---------------------------------------------------+

# +---------------------------------------------------+
# MTAGROUP
# +---------------------------------------------------+
emfa_mtagroup () {

# add groups
groupadd mtagroup

#add apache, clamav, and postfix to mtagroup
usermod -a -G mtagroup apache
usermod -a -G mtagroup postfix
usermod -a -G mtagroup clamav
usermod -a -G mtagroup clam

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Get Master Files
# https://bitbucket.org/emfabox/v2/get/master.tar.gz
# +---------------------------------------------------+

emfa_masterfiles () {

mkdir -p /usr/src/EMFA/install/master 

cd /usr/src/EMFA/install/master 
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/master/master.tar.gz https://bitbucket.org/emfabox/v2/get/master.tar.gz

tar -xzvf master.tar.gz
}

# +---------------------------------------------------+

# +---------------------------------------------------+
# MailScanner
# +---------------------------------------------------+
emfa_mailscanner () {

#db passwords
source /etc/emfa/variables.conf

    mkdir -p /usr/src/EMFA/install/mailscanner     
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/mailscanner/mailscanner-4.85.2-3.noarch.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/MailScanner-4.85.2-3/mailscanner-4.85.2-3.noarch.rpm
    
    cd /usr/src/EMFA/install/mailscanner   
    rm -f jb_ms_rpm_public.key
    $CURL -O https://s3.amazonaws.com/mailscanner/gpg/jb_ms_rpm_public.key
    rpm --import jb_ms_rpm_public.key
    rm -f jb_ms_rpm_public.key

    echo "Installing the MailScanner RPM ... ";
    
    rpm -Uhv mailscanner*noarch.rpm

    if [ -f /root/.rpmmacros ] ; then   
    rm -f /root/.rpmmacros  
    fi
  
    chown postfix:postfix /var/spool/MailScanner/quarantine
    mkdir /var/spool/MailScanner/spamassassin
    chown postfix:postfix /var/spool/MailScanner/spamassassin
    
    if [ ! -d /var/spool/mqueue ]; then      
        mkdir /var/spool/mqueue    
    fi
    
    chown postfix:postfix /var/spool/mqueue
    touch /var/lock/subsys/MailScanner.off
    touch /etc/MailScanner/rules/spam.blacklist.rules
    
    #Required SpamAssassin Score = &SQLSpamScores
    #High SpamAssassin Score = &SQLHighSpamScores
    
    #Some Defaults 

    sed -i "/^%org-name% =/ c\%org-name% = EMFABOX" /etc/MailScanner/MailScanner.conf
    sed -i "/^%org-long-name% =/ c\%org-long-name% = EMFABOX-EMAILFIREWALL" /etc/MailScanner/MailScanner.conf
    sed -i "/^%web-site% =/ c\%web-site% = www.emfabox.com" /etc/MailScanner/MailScanner.conf

    # Configure MailScanner
    sed -i '/^Max Children =/ c\Max Children = 2' /etc/MailScanner/MailScanner.conf
    sed -i '/^Run As User =/ c\Run As User = postfix' /etc/MailScanner/MailScanner.conf
    sed -i '/^Run As Group =/ c\Run As Group = mtagroup' /etc/MailScanner/MailScanner.conf
    sed -i '/^Incoming Queue Dir =/ c\Incoming Queue Dir = \/var\/spool\/postfix\/hold' /etc/MailScanner/MailScanner.conf
    sed -i '/^Outgoing Queue Dir =/ c\Outgoing Queue Dir = \/var\/spool\/postfix\/incoming' /etc/MailScanner/MailScanner.conf
    sed -i '/^MTA =/ c\MTA = postfix' /etc/MailScanner/MailScanner.conf
    sed -i '/^Incoming Work Group =/ c\Incoming Work Group = mtagroup' /etc/MailScanner/MailScanner.conf
    sed -i '/^Incoming Work Permissions =/ c\Incoming Work Permissions = 0644' /etc/MailScanner/MailScanner.conf
    sed -i '/^Quarantine User =/ c\Quarantine User = postfix' /etc/MailScanner/MailScanner.conf
    sed -i '/^Quarantine Group =/ c\Quarantine Group = mtagroup' /etc/MailScanner/MailScanner.conf
    sed -i '/^Quarantine Permissions =/ c\Quarantine Permissions = 0660' /etc/MailScanner/MailScanner.conf
    sed -i '/^Deliver Unparsable TNEF =/ c\Deliver Unparsable TNEF = yes' /etc/MailScanner/MailScanner.conf
    sed -i "/^Maximum Archive Depth =/ c\Maximum Archive Depth = 2" /etc/MailScanner/MailScanner.conf
    sed -i '/^Virus Scanners =/ c\Virus Scanners = clamd' /etc/MailScanner/MailScanner.conf
    sed -i '/^Non-Forging Viruses =/ c\Non-Forging Viruses = Joke\/ OF97\/ WM97\/ W97M\/ eicar Zip-Password' /etc/MailScanner/MailScanner.conf
    ## Web Bug Replacement
    sed -i '/^Web Bug Replacement =/ c\Web Bug Replacement = https://s3.amazonaws.com/mailscanner/images/1x1spacer.gif' /etc/MailScanner/MailScanner.conf
    sed -i '/^Quarantine Whole Message =/ c\Quarantine Whole Message = yes' /etc/MailScanner/MailScanner.conf
    sed -i "/^Quarantine Infections =/ c\Quarantine Infections = yes" /etc/MailScanner/MailScanner.conf
    sed -i "/^Quarantine Silent Viruses =/ c\Quarantine Silent Viruses = yes" /etc/MailScanner/MailScanner.conf
    sed -i '/^Keep Spam And MCP Archive Clean =/ c\Keep Spam And MCP Archive Clean = yes' /etc/MailScanner/MailScanner.conf
    # X-%org-name%-EMFABox
    sed -i 's/X-%org-name%-MailScanner/X-%org-name%-EMFABox/g' /etc/MailScanner/MailScanner.conf
    sed -i '/^Remove These Headers =/ c\Remove These Headers = X-Mozilla-Status: X-Mozilla-Status2: Disposition-Notification-To: Return-Receipt-To:' /etc/MailScanner/MailScanner.conf
    sed -i '/^Disarmed Modify Subject =/ c\Disarmed Modify Subject = no' /etc/MailScanner/MailScanner.conf
    sed -i '/^Send Notices =/ c\Send Notices = no' /etc/MailScanner/MailScanner.conf
    sed -i '/^Notice Signature =/ c\Notice Signature = -- \\nEMFABox\\nEMail Security\\nwww.emfabox.com' /etc/MailScanner/MailScanner.conf
    sed -i '/^Notices From =/ c\Notices From = EMFA' /etc/MailScanner/MailScanner.conf
    sed -i '/^Inline HTML Signature =/ c\Inline HTML Signature = %rules-dir%\/sig.html.rules' /etc/MailScanner/MailScanner.conf
    sed -i '/^Inline Text Signature =/ c\Inline Text Signature = %rules-dir%\/sig.text.rules' /etc/MailScanner/MailScanner.conf
    sed -i "/^Dont Sign HTML If Headers Exist =/ c\Dont Sign HTML If Headers Exist = In-Reply-To: References:" /etc/MailScanner/MailScanner.conf
    sed -i '/^Is Definitely Not Spam =/ c\Is Definitely Not Spam = &SQLWhitelist' /etc/MailScanner/MailScanner.conf
    sed -i '/^Is Definitely Spam =/ c\Is Definitely Spam = &SQLBlacklist' /etc/MailScanner/MailScanner.conf
    sed -i '/^Definite Spam Is High Scoring =/ c\Definite Spam Is High Scoring = yes' /etc/MailScanner/MailScanner.conf
    # WATERMARKING 
    sed -i '/^Use Watermarking =/ c\Use Watermarking = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Add Watermark =/ c\Add Watermark = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Check Watermarks With No Sender =/ c\Check Watermarks With No Sender = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Treat Invalid Watermarks With No Sender as Spam =/ c\Treat Invalid Watermarks With No Sender as Spam = 3' /etc/MailScanner/MailScanner.conf
    sed -i '/^Watermark Secret =/ c\Watermark Secret = %org-name%-${WATERMARK_SECRET}' /etc/MailScanner/MailScanner.conf
    sed -i '/^Check Watermarks To Skip Spam Checks =/ c\Check Watermarks To Skip Spam Checks = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Watermark Lifetime =/ c\Watermark Lifetime = 604800' /etc/MailScanner/MailScanner.conf
    sed -i '/^Watermark Header =/ c\Watermark Header = X-%org-name%-EMFABox-Watermark:' /etc/MailScanner/MailScanner.conf
    sed -i '/^Max SpamAssassin Size =/ c\Max SpamAssassin Size = 100k continue 150k' /etc/MailScanner/MailScanner.conf
    sed -i "/^Max Spam Check Size =/ c\Max Spam Check Size = 2048k" /etc/MailScanner/MailScanner.conf
    #Required SpamAssassin Score = &SQLSpamScores
    sed -i '/^Required SpamAssassin Score =/ c\Required SpamAssassin Score = &SQLSpamScores' /etc/MailScanner/MailScanner.conf
    #High SpamAssassin Score = &SQLHighSpamScores
    sed -i "/^High SpamAssassin Score =/ c\High SpamAssassin Score = &SQLHighSpamScores" /etc/MailScanner/MailScanner.conf
    #Spam Checks
    sed -i '/^Spam Checks =/ c\Spam Checks = &SQLNoScan' /etc/MailScanner/MailScanner.conf
    sed -i '/^Spam Actions =/ c\Spam Actions = store notify' /etc/MailScanner/MailScanner.conf
    sed -i '/^High Scoring Spam Actions =/ c\High Scoring Spam Actions = store' /etc/MailScanner/MailScanner.conf
    sed -i '/^Non Spam Actions =/ c\Non Spam Actions = store deliver header "X-Spam-Status:No" ' /etc/MailScanner/MailScanner.conf
    sed -i '/^Log Spam =/ c\Log Spam = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Log Silent Viruses =/ c\Log Silent Viruses = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Log Dangerous HTML Tags =/ c\Log Dangerous HTML Tags = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^SpamAssassin Local State Dir =/ c\SpamAssassin Local State Dir = /var/lib/spamassassin' /etc/MailScanner/MailScanner.conf
    sed -i '/^SpamAssassin User State Dir =/ c\SpamAssassin User State Dir = /var/spool/MailScanner/spamassassin' /etc/MailScanner/MailScanner.conf
    sed -i '/^Detailed Spam Report =/ c\Detailed Spam Report = yes' /etc/MailScanner/MailScanner.conf
    sed -i '/^Include Scores In SpamAssassin Report =/ c\Include Scores In SpamAssassin Report = yes' /etc/MailScanner/MailScanner.conf
    sed -i "/^Log Speed =/ c\Log Speed = yes" /etc/MailScanner/MailScanner.conf
	  sed -i "/^Rebuild Bayes Every =/ c\Rebuild Bayes Every = 86400" /etc/MailScanner/MailScanner.conf
    sed -i '/^Always Looked Up Last =/ c\Always Looked Up Last = &MailWatchLogging' /etc/MailScanner/MailScanner.conf
    sed -i '/^Clamd Socket/ c\Clamd Socket = \/var\/run\/clamav\/clamd.sock' /etc/MailScanner/MailScanner.conf
    sed -i '/^Log SpamAssassin Rule Actions =/ c\Log SpamAssassin Rule Actions = no' /etc/MailScanner/MailScanner.conf
    sed -i "/^Sign Clean Messages =/ c\Sign Clean Messages = yes" /etc/MailScanner/MailScanner.conf
    sed -i "/^Deliver Cleaned Messages =/ c\Deliver Cleaned Messages = No" /etc/MailScanner/MailScanner.conf
    sed -i "/^Maximum Processing Attempts =/ c\Maximum Processing Attempts = 0" /etc/MailScanner/MailScanner.conf
    
#Virus Scanning
    v01="Virus Scanning = yes"
    v02="Virus Scanning = %rules-dir%/virus.scanning.rules"
    sed -i "s/$v01/$v02/g" /etc/MailScanner/MailScanner.conf

#Highlight Phishing Fraud
    v03="Highlight Phishing Fraud = yes"
    v04="Highlight Phishing Fraud = no"
    sed -i "s/$v03/$v04/g" /etc/MailScanner/MailScanner.conf

#SpamScore Number Instead Of Stars = yes
    v05="SpamScore Number Instead Of Stars = no"
    v06="SpamScore Number Instead Of Stars = yes"
    sed -i "s/$v05/$v06/g" /etc/MailScanner/MailScanner.conf

#Always Include SpamAssassin Report = no
    v07="Always Include SpamAssassin Report = no"
    v08="Always Include SpamAssassin Report = yes"
    sed -i "s/$v07/$v08/g" /etc/MailScanner/MailScanner.conf

# HEADERs
#    sed -i '/^Mail Header =/ c\Mail Header = X-%org-name%-EMFABox:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^Spam Header =/ c\Spam Header = X-%org-name%-EMFABox-SpamCheck:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^Spam Score Header =/ c\Spam Score Header = X-%org-name%-EMFABox-SpamScore:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^Information Header =/ c\Information Header = X-%org-name%-EMFABox-Information:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^Add Envelope From Header =/ c\Add Envelope From Header = yes' /etc/MailScanner/MailScanner.conf
#    sed -i '/^Envelope From Header =/ c\Envelope From Header = X-%org-name%-EMFABox-From:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^Envelope To Header =/ c\Envelope To Header = X-%org-name%-EMFABox-To:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^ID Header =/ c\ID Header = X-%org-name%-EMFABox-ID:' /etc/MailScanner/MailScanner.conf
#    sed -i '/^IP Protocol Version Header =/ c\IP Protocol Version Header = X-%org-name%-EMFABox-IP-Protocol:' /etc/MailScanner/MailScanner.conf

# updates

sed -i "/^Place New Headers At Top Of Message =/ c\Place New Headers At Top Of Message = yes" /etc/MailScanner/MailScanner.conf
sed -i "/^Notify Senders =/ c\Notify Senders = no" /etc/MailScanner/MailScanner.conf
sed -i "/^Sign Clean Messages =/ c\Sign Clean Messages = /etc/MailScanner/rules/signing.rules" /etc/MailScanner/MailScanner.conf
#Sign Clean Messages#
echo "FromOrTo: default yes" > /etc/MailScanner/rules/signing.rules

    
 # Startup Header ...   
    sed -i '/^envelope_sender_header / c\envelope_sender_header X-EMFABOX-EMFABox-From' /etc/MailScanner/spam.assassin.prefs.conf
    sed -i "/^DB DSN =/ c\DB DSN = dbi:mysql:dbname=${MAILSCANNER_DBNAME};host=localhost;port=3306" /etc/MailScanner/MailScanner.conf
    sed -i "/^DB Username =/ c\DB Username = ${MAILSCANNER_DBUSER}" /etc/MailScanner/MailScanner.conf
    sed -i "/^DB Password =/ c\DB Password = ${MAILSCANNER_DBPASS}" /etc/MailScanner/MailScanner.conf
    sed -i "/^SQL Serial Number =/ c\SQL Serial Number = SELECT value FROM ms_config WHERE options='confserialnumber'" /etc/MailScanner/MailScanner.conf
    sed -i "/^SQL Quick Peek =/ c\SQL Quick Peek = SELECT value FROM ms_config WHERE external=? AND hostname=?" /etc/MailScanner/MailScanner.conf
    sed -i "/^SQL Config =/ c\SQL Config = SELECT options, value FROM ms_config WHERE hostname=?" /etc/MailScanner/MailScanner.conf
   # sed -i "/^SQL Ruleset =/ c\SQL Ruleset = SELECT num, rule FROM ms_rulesets WHERE name=? ORDER BY num ASC" /etc/MailScanner/MailScanner.conf
   #SELECT @rownum:=@rownum+1 num, rule FROM ms_rulesets, (SELECT @rownum:=0) r WHERE name = ?
    sed -i '/^SQL Ruleset =/ c\SQL Ruleset = SELECT @rownum:=@rownum+1 num, rule FROM ms_rulesets, (SELECT @rownum:=0) r WHERE name=?' /etc/MailScanner/MailScanner.conf
    # SQL Debug --> yes
    sed -i "/^SQL Debug =/ c\SQL Debug = yes" /etc/MailScanner/MailScanner.conf
    
    sed -i "/^include \/etc\/MailScanner\/conf.d\/*/ c\#EMFABox END/*" /etc/MailScanner/MailScanner.conf
    
    # mailscanner 4.85.2-2
    # 
    # fix the clamav wrapper if the user does not exist
    if [ -f '/etc/freshclam.conf' ]; then
    	if id -u clam >/dev/null 2>&1; then
    		#clam is being used instead of clamav
    		OLDCAVUSR='ClamUser="clamav"';
    		NEWCAVUSR='ClamUser="clam"'
    	
    		OLDCAVGRP='ClamGroup="clamav"';
    		NEWCAVGRP='ClamGroup="clam"';
    	
    		perl -pi -e 's/'$OLDCAVUSR'/'$NEWCAVUSR'/;' /usr/share/MailScanner/clamav-wrapper
    		perl -pi -e 's/'$OLDCAVGRP'/'$NEWCAVGRP'/;' /usr/share/MailScanner/clamav-wrapper
    	fi
    fi
    
    # remove freshclam from /etc/cron.daily
    rm -f /etc/cron.daily/freshclam 

    mkdir -p /usr/local/share/GeoIP/
    
    if [ ! -d /var/www/html/master/lib/temp ]; then   
    mkdir -p  /var/www/html/master/lib/temp     
    echo "#EMFAbox" > /var/www/html/master/lib/temp/placeholder.txt 
    /usr/bin/wget --no-check-certificate -O /var/www/html/master/lib/temp/GeoIP.dat.gz  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/geoip/GeoIP.dat.gz
    /usr/bin/wget --no-check-certificate -O /var/www/html/master/lib/temp/GeoIPv6.dat.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/geoip/GeoIPv6.dat.gz 
    cd  /var/www/html/master/lib/temp
    /bin/gunzip GeoIP.dat.gz -q -f   
    /bin/gunzip GeoIPv6.dat.gz -q -f
    fi

    if [ ! -f /var/www/html/master/lib/temp/GeoIP.dat ] ; then   
    cd  /var/www/html/master/lib/temp
    /usr/bin/wget --no-check-certificate -O /var/www/html/master/lib/temp/GeoIP.dat.gz  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/geoip/GeoIP.dat.gz
    /bin/gunzip GeoIP.dat.gz -q -f
    fi
    
    if [ ! -f /var/www/html/master/lib/temp/GeoIPv6.dat ] ; then
    
    cd  /var/www/html/master/lib/temp
    /usr/bin/wget --no-check-certificate -O /var/www/html/master/lib/temp/GeoIPv6.dat.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/geoip/GeoIPv6.dat.gz
    /bin/gunzip GeoIPv6.dat.gz -q -f
    fi
    
    ln -s /var/www/html/master/lib/temp/*.dat /usr/local/share/GeoIP/
      
    touch /etc/MailScanner/rules/sig.html.rules
    touch /etc/MailScanner/rules/sig.text.rules
   
    rm -rf /var/spool/MailScanner/incoming
    mkdir -p /var/spool/MailScanner/incoming
    echo "none /var/spool/MailScanner/incoming tmpfs noatime 0 0">>/etc/fstab
    mount -a
  
    
    # comment 2015-04-17
    # new Fix
    #sed -i '/^#!\/usr\/bin\/perl -I\/usr\/lib\/MailScanner/ c\#!\/usr\/bin\/perl -I\/usr\/lib\/MailScanner\ -U' /usr/sbin/MailScanner
    
    # Replace reports
    
    rm -rf /etc/MailScanner/reports
    
    if [ -d /usr/src/EMFA/install/reports/ ]; then   
    rm -rf  /usr/src/EMFA/install/reports/
    fi
    
    mkdir -p /opt/emfa/MailScanner/  
    cd /opt/emfa/MailScanner/
    
    #https://bitbucket.org/emfabox/beta/src/79277208efaf69e68118592faae564f2877f5704/src/MailScanner/etc/reports.tar.gz?at=master
    wget --no-check-certificate -O /opt/emfa/MailScanner/reports.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/MailScanner/etc/reports.tar.gz
    
    tar -xzvf reports.tar.gz
    
    rm -f /opt/emfa/MailScanner/reports.tar.gz
    
    cd /opt/emfa/MailScanner/reports
    
    rsync -ar *  /etc/MailScanner/reports

    # Force mailscanner init to return a code on all failures 
    sed -i 's/failure/failure \&\& RETVAL=1/g' /etc/init.d/MailScanner

    # Remove duplicates 
    rm -f /etc/cron.daily/clean.quarantine
        
   
   #Fix Filename rules
    sed -i 's/\(^.*Filename contains lots of white space.*$\)/#\1/' /etc/MailScanner/filename.rules.conf
    
   ## 2015-03-25
   
   cd /etc/MailScanner/
   #https://bitbucket.org/emfabox/beta/raw/1970e469a9d1d25fafeada0c99e9f3b04008ef3b/src/MailScanner/etc/filename.rules.conf_template
   wget --no-check-certificate -O /etc/MailScanner/filename.rules.conf_template https://bitbucket.org/emfabox/beta/raw/1970e469a9d1d25fafeada0c99e9f3b04008ef3b/src/MailScanner/etc/filename.rules.conf_template
   
   #https://bitbucket.org/emfabox/beta/raw/1970e469a9d1d25fafeada0c99e9f3b04008ef3b/src/MailScanner/etc/filetype.rules.conf_template
   wget --no-check-certificate -O /etc/MailScanner/filetype.rules.conf_template  https://bitbucket.org/emfabox/beta/raw/1970e469a9d1d25fafeada0c99e9f3b04008ef3b/src/MailScanner/etc/filetype.rules.conf_template

   ### rulesets
   
   #https://bitbucket.org/emfabox/beta/raw/b28ebd8b8d2decf01191e8dfdbdaca2c6014a502/src/MailScanner/etc/filename.rules.allowall.conf
   wget --no-check-certificate -O /etc/MailScanner/filename.rules.allowall.conf https://bitbucket.org/emfabox/beta/raw/b28ebd8b8d2decf01191e8dfdbdaca2c6014a502/src/MailScanner/etc/filename.rules.allowall.conf

   #https://bitbucket.org/emfabox/beta/raw/b28ebd8b8d2decf01191e8dfdbdaca2c6014a502/src/MailScanner/etc/filetype.rules.allowall.conf
  wget --no-check-certificate -O /etc/MailScanner/filetype.rules.allowall.conf https://bitbucket.org/emfabox/beta/raw/b28ebd8b8d2decf01191e8dfdbdaca2c6014a502/src/MailScanner/etc/filetype.rules.allowall.conf
     
   # config script 
   #2015-03-26
   #https://bitbucket.org/emfabox/beta/raw/3efb0ea9b5c563ab25fb91f101a0b820da393c69/src/emfa_perl_scripts/ConfigTemplate.pm
   wget --no-check-certificate -O /usr/local/share/perl5/ConfigTemplate.pm https://bitbucket.org/emfabox/beta/raw/3efb0ea9b5c563ab25fb91f101a0b820da393c69/src/emfa_perl_scripts/ConfigTemplate.pm

   ## build startup
   
#2015-06-23
echo "From:           127.0.0.1       /etc/MailScanner/filename.rules.allowall.conf" > /etc/MailScanner/filename.rules
echo "From:           192.168.130.   /etc/MailScanner/filetype.rules.allowall.conf" >> /etc/MailScanner/filename.rules
echo "FromOrTo:       default /etc/MailScanner/filename.rules.conf" >> /etc/MailScanner/filename.rules

echo "From:           127.0.0.1       /etc/MailScanner/filetype.rules.allowall.conf" > /etc/MailScanner/filetype.rules
echo "From:           192.168.130.   /etc/MailScanner/filetype.rules.allowall.conf" >> /etc/MailScanner/filetype.rules
echo "FromOrTo:       default /etc/MailScanner/filetype.rules.allowall.conf" >> /etc/MailScanner/filetype.rules
#

sed -i 's/use_auto_whitelist 0/#use_auto_whitelist 0/g' /etc/MailScanner/spam.assassin.prefs.conf


}
# +---------------------------------------------------+

# +---------------------------------------------------+
# configure POSTFIX-OUT
# +---------------------------------------------------+
emfa_postfix_out () {

#db passwords
    source /etc/emfa/variables.conf
    
 if [ ! -d /etc/postfix-out ] ; then
    mkdir -p /etc/postfix-out/  
fi
    
    cp -rp /etc/postfix/* /etc/postfix-out
    
    cd  /etc/postfix-out
    #https://bitbucket.org/emfabox/beta/raw/1d7adfb3800495ce257b7cd4fa0fb6f91d742a13/src/postfix-out/main.cf
    /usr/bin/wget --no-check-certificate -O /etc/postfix-out/main.cf https://bitbucket.org/emfabox/beta/raw/1d7adfb3800495ce257b7cd4fa0fb6f91d742a13/src/postfix-out/main.cf
    
    #https://bitbucket.org/emfabox/beta/raw/1d7adfb3800495ce257b7cd4fa0fb6f91d742a13/src/postfix-out/master.cf
    /usr/bin/wget --no-check-certificate -O /etc/postfix-out/master.cf https://bitbucket.org/emfabox/beta/raw/1d7adfb3800495ce257b7cd4fa0fb6f91d742a13/src/postfix-out/master.cf
    
    # https://bitbucket.org/emfabox/beta/raw/b568f5ec4f0ecedccf40ce00064049f7fc00c4fd/src/postfix-out/bounce.cf
    /usr/bin/wget --no-check-certificate -O /etc/postfix-out/bounce.cf https://bitbucket.org/emfabox/beta/raw/b568f5ec4f0ecedccf40ce00064049f7fc00c4fd/src/postfix-out/bounce.cf
    
    mkdir -p /var/spool/postfix-out
    
    # set hostname in main.cf
    
    sed -i "/^myhostname =/ c\myhostname = mail-out.${HOSTNAME}" /etc/postfix-out/main.cf
    
    postfix -c /etc/postfix-out check
    
    if [ ! -d /var/lib/postfix-out ] ; then
    mkdir -p /var/lib/postfix-out
    fi
    
    chown postfix:root  /var/lib/postfix-out
    
    cd  /etc/postfix-out/

    touch  /etc/postfix-out/sender_access
    /usr/sbin/postmap  /etc/postfix-out/sender_access

    #tls_policy
    touch /etc/postfix-out/tls_policy
    
    /usr/sbin/postmap  /etc/postfix-out/tls_policy
    
    #create transport.db generic.db
    /usr/sbin/postmap  /etc/postfix-out/transport 
    /usr/sbin/postmap  /etc/postfix-out/generic
    
    # sasl_passwd relayhost
    touch /etc/postfix-out/sasl_passwd
     
    /usr/sbin/postmap  /etc/postfix-out/sasl_passwd

    # postfix-out init
    #/etc/rc.d/init.d

    /usr/bin/wget --no-check-certificate -O /etc/rc.d/init.d/postfix-out https://bitbucket.org/emfabox/beta/raw/be794664d36536ca4fa53c8e984f87d03a6db0e2/src/init.d/postfix-out
    
    chmod 755 /etc/rc.d/init.d/postfix-out
    
    #postconf -c /etc/postfix-out/ -e "inet_interfaces = all"
    postconf -c /etc/postfix-out -e "inet_interfaces = loopback-only"
    
    # tls config out -->
    postconf -c /etc/postfix-out/ -e "smtpd_use_tls = yes"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_key_file = /etc/postfix-out/ssl/smtpd.pem"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_cert_file = /etc/postfix-out/ssl/smtpd.pem"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_CAfile = /etc/postfix-out/ssl/smtpd.pem"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_loglevel = 1"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_received_header = yes"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_session_cache_timeout = 3600s"
    postconf -c /etc/postfix-out/ -e "tls_random_source = dev:/dev/urandom"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_session_cache_database = btree:/var/lib/postfix-out/smtpd_tls_session_cache"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_security_level = may"  
    postconf -c /etc/postfix-out/ -e "message_size_limit = 50000000"  # message size limit #
   
    # Poodle
    # Disable SSLv2 & SSLv3 
    postconf -c /etc/postfix-out/ -e 'smtpd_tls_mandatory_protocols = !SSLv2,!SSLv3'
    postconf -c /etc/postfix-out/ -e 'smtp_tls_mandatory_protocols = !SSLv2,!SSLv3'
    postconf -c /etc/postfix-out/ -e 'smtpd_tls_protocols = !SSLv2,!SSLv3'
    postconf -c /etc/postfix-out/ -e 'smtp_tls_protocols = !SSLv2,!SSLv3'
       
    # Logjam Vulnerability 
    openssl dhparam -out /etc/postfix-out/ssl/dhparam.pem 2048
    postconf  -c /etc/postfix-out/ -e "smtpd_tls_dh1024_param_file = /etc/postfix/ssl/dhparam.pem"
    #postconf -e "smtpd_tls_ciphers = low"
    
    #https://wiki.zimbra.com/wiki/Postfix_PCI_Compliance_in_ZCS
    postconf  -c /etc/postfix-out/ -e "smtpd_tls_ciphers = high"
    postconf  -c /etc/postfix-out/ -e "smtpd_tls_exclude_ciphers = aNULL,MD5,DES"
     
    # change sylog name
    postconf -c /etc/postfix-out/ -e 'syslog_name = postfix-out'
        
    # add service
    chkconfig --add postfix-out
    chkconfig postfix-out on

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# configure spf
# +---------------------------------------------------+

emfa_spf () {
      mkdir -p /usr/src/EMFA/install/spf/
      
      #https://bitbucket.org/emfabox/beta/src/73bfa2d84a92ea674fa0bfe62f3bb04472c4f698/src/ipaddr-2.1.11.tar.gz?at=master   
      /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/spf/ipaddr-2.1.11.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/ipaddr-2.1.11.tar.gz
      
      #https://bitbucket.org/emfabox/beta/src/98fd0393a33251423a0107c0f2e96d94dd0efe4f/src/pypolicyd-spf-1.3.1.tar.gz?at=master 
      #/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/spf/pypolicyd-spf-1.3.1.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/pypolicyd-spf-1.3.1.tar.gz
      
      #https://bitbucket.org/emfabox/beta/raw/840ef7daca55f229a5bbff51729022d6fa05cb82/src/pypolicyd-spf-1.3.2.tar.gz
      /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/spf/pypolicyd-spf-1.3.2.tar.gz https://bitbucket.org/emfabox/beta/raw/840ef7daca55f229a5bbff51729022d6fa05cb82/src/pypolicyd-spf-1.3.2.tar.gz
      
      # https://bitbucket.org/emfabox/beta/src/6ed4de375ef9297dc5e2246ca7e398e8fbe29088/src/pyspf-2.0.11.tar.gz?at=master
      #/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/spf/pyspf-2.0.11.tar.gz  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/pyspf-2.0.11.tar.gz
     
      #https://bitbucket.org/emfabox/beta/raw/6a01aee8f6c05308d1a7927f22397a1e735adf7d/src/pyspf-2.0.12t.tar.gz
      /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/spf/pyspf-2.0.12t.tar.gz https://bitbucket.org/emfabox/beta/raw/6a01aee8f6c05308d1a7927f22397a1e735adf7d/src/pyspf-2.0.12t.tar.gz
      
      # https://bitbucket.org/emfabox/beta/src/6ed4de375ef9297dc5e2246ca7e398e8fbe29088/src/pydns-2.3.6.tar.gz?at=master
      /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/spf/pydns-2.3.6.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/pydns-2.3.6.tar.gz
      
      cd /usr/src/EMFA/install/spf/
      
      tar -xzvf ipaddr-2.1.11.tar.gz
      
      cd ipaddr-2.1.11
      
      /usr/bin/python setup.py build
	    /usr/bin/python setup.py install
      
      cd /usr/src/EMFA/install/spf/
           
      tar -xzvf pyspf-2.0.12t.tar.gz
      
      cd pyspf-*
            
      /usr/bin/python setup.py build   
      /usr/bin/python setup.py install
      
      cd /usr/src/EMFA/install/spf/
      
      tar -xzvf pypolicyd-spf-1.3.2.tar.gz
      
      cd pypolicyd-spf-1.3.2
      
      /usr/bin/python setup.py build 
      /usr/bin/python setup.py install
      
      tar -xzvf pydns-2.3.6.tar.gz
      
      cd pydns-*
      
      /usr/bin/python setup.py build 
      /usr/bin/python setup.py install

      ###
      echo "#" >> /etc/postfix/master.cf
      echo "# ==================================================================== " >> /etc/postfix/master.cf
      echo "#" >> /etc/postfix/master.cf
      
      echo "### policyd-spf " >> /etc/postfix/master.cf
      echo "policyd-spf  unix  -       n       n       -       -       spawn" >> /etc/postfix/master.cf
      echo "            user=nobody argv=/usr/bin/python /usr/bin/policyd-spf /etc/python-policyd-spf/policyd-spf.conf" >> /etc/postfix/master.cf
      ###

      yes | cp /etc/python-policyd-spf/policyd-spf.conf /etc/python-policyd-spf/policyd-spf.conf.orig
      
      printf "# Configuration written by EMFABox install script\n" > /etc/python-policyd-spf/policyd-spf.conf
      printf "#  For a fully commented sample config file see policyd-spf.conf.commented\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "debugLevel = 5\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "defaultSeedOnly = 0\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "HELO_reject = SPF_Not_Pass\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "Mail_From_reject = Fail\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "PermError_reject = False\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "TempError_Defer = False\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "\n" >> /etc/python-policyd-spf/policyd-spf.conf
      #printf "skip_addresses = 127.0.0.0/8,::ffff:127.0.0.0/104,::1\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "skip_addresses =127.0.0.0/8,::ffff:127.0.0.0//104,::1//128\n" >> /etc/python-policyd-spf/policyd-spf.conf
      printf "\n" >> /etc/python-policyd-spf/policyd-spf.conf
      
      # commented file 
      yes | cp policyd-spf.conf.commented  /etc/python-policyd-spf/

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# configure MYSQL
# +---------------------------------------------------+
emfa_mysql () {
clear
 
    echo "Mysql configuration starts now .."
    
    timewait 2
    
    #db passwords
    source /etc/emfa/variables.conf
    
     # create my.cnf

    mv /etc/my.cnf /etc/my.cnf.orig
    cp /usr/share/mysql/my-medium.cnf /etc/my.cnf
    sed -i 's/#innodb_/innodb_/g' /etc/my.cnf

    /sbin/service mysqld start

    # give mysql 10 s time to create innodb files
    printf "Giving MySQL 10 seconds time to spin up ...\n"
    printf "\n"
    sleep 10s

    QUERY_0="FLUSH PRIVILEGES;"
    /usr/bin/mysql -e "$QUERY_0"
    sleep 2s
       
    MYARRAY=();
    MYARRAY+=('mailscanner');
    MYARRAY+=('sqlgrey');   ## issue with utf8 ##  2015-04-09
    MYARRAY+=('sa_bayes');
    MYARRAY+=('cluster');
    MYARRAY+=('cluebringer');
    MYARRAY+=('opendmarc');
     
    for i in "${MYARRAY[@]}"
do
    timewait 1
    echo "Creating database $i ..."; echo;
    #/usr/bin/mysql -e  "CREATE DATABASE IF NOT EXISTS ${i} CHARACTER SET utf8";  # 2015-04-09 
    /usr/bin/mysql -e  "CREATE DATABASE IF NOT EXISTS ${i};"
    timewait 2
done
    
  
    QUERY_8="DROP DATABASE test;"
    /usr/bin/mysql -e "$QUERY_8"
    timewait 2
    
    QUERY_12="DELETE FROM mysql.user WHERE User='';"
    /usr/bin/mysql -e "$QUERY_12"
    timewait 2
    
    QUERY_13="DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    /usr/bin/mysql -e "$QUERY_13"
    timewait 2   
  
    mkdir -p /usr/src/EMFA/install/sql/


    cd /usr/src/EMFA/install/sql/
    
    # import databases
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/mailscanner_master.sql https://bitbucket.org/emfabox/beta/raw/e14fcf6632828a351a4935cd834404439a68c7ba/src/sql/mailscanner_master.sql
    /usr/bin/mysql --default-character-set=utf8 mailscanner < /usr/src/EMFA/install/sql/mailscanner_master.sql
    
#############################    
#   MailScanner DB END      #
#############################


     #https://bitbucket.org/emfabox/beta/raw/f241094d2d868ae31c8fb760d8e2b67b7ebf960a/src/sql/bayes_mysql.sql
    
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/bayes_mysql.sql https://bitbucket.org/emfabox/beta/raw/f241094d2d868ae31c8fb760d8e2b67b7ebf960a/src/sql/bayes_mysql.sql
    /usr/bin/mysql --default-character-set=utf8 sa_bayes < /usr/src/EMFA/install/sql/bayes_mysql.sql
    

    #https://bitbucket.org/emfabox/beta/raw/f241094d2d868ae31c8fb760d8e2b67b7ebf960a/src/sql/awl_mysql.sql
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/awl_mysql.sql https://bitbucket.org/emfabox/beta/raw/f241094d2d868ae31c8fb760d8e2b67b7ebf960a/src/sql/awl_mysql.sql
    /usr/bin/mysql --default-character-set=utf8 sa_bayes < /usr/src/EMFA/install/sql/awl_mysql.sql
    
    #https://bitbucket.org/emfabox/beta/raw/ff441566816c7540b1ffa6d847ef95907d6aa54c/src/sql/cluster.sql
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/cluster.sql https://bitbucket.org/emfabox/beta/raw/ff441566816c7540b1ffa6d847ef95907d6aa54c/src/sql/cluster.sql
    /usr/bin/mysql --default-character-set=utf8 cluster < /usr/src/EMFA/install/sql/cluster.sql
    
    
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/policyd.mysql.sql https://bitbucket.org/emfabox/beta/raw/714f71521b4207766eb16b34dcecec655365e14c/src/cluebringer/policyd.mysql.sql
    
    #Newest MySQL server doesn't recognize 'TYPE=MyISAM' or 'TYPE=InnoDB' while creating SQL table, so we'd better convert them to 'ENGINE=' before importing it.
    perl -pi -e 's#TYPE=#ENGINE=#g' /usr/src/EMFA/install/sql/policyd.mysql.sql
    #/usr/bin/mysql --default-character-set=utf8 cluebringer < /usr/src/EMFA/install/sql/policyd.mysql.sql
     /usr/bin/mysql  --default-character-set=utf8 cluebringer < /usr/src/EMFA/install/sql/policyd.mysql.sql
    
    #https://bitbucket.org/emfabox/beta/raw/714f71521b4207766eb16b34dcecec655365e14c/src/cluebringer/extra.sql
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/policyd_extra.sql https://bitbucket.org/emfabox/beta/raw/714f71521b4207766eb16b34dcecec655365e14c/src/cluebringer/extra.sql
    #/usr/bin/mysql --default-character-set=utf8 cluebringer < /usr/src/EMFA/install/sql/policyd_extra.sql
    /usr/bin/mysql  --default-character-set=utf8 cluebringer < /usr/src/EMFA/install/sql/policyd_extra.sql
   
    #https://bitbucket.org/emfabox/beta/raw/714f71521b4207766eb16b34dcecec655365e14c/src/cluebringer/column_character_set.mysql
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/column_character_set.mysql https://bitbucket.org/emfabox/beta/raw/714f71521b4207766eb16b34dcecec655365e14c/src/cluebringer/column_character_set.mysql
    #/usr/bin/mysql --default-character-set=utf8 cluebringer < /usr/src/EMFA/install/sql/column_character_set.mysql
    /usr/bin/mysql  --default-character-set=utf8 cluebringer < /usr/src/EMFA/install/sql/column_character_set.mysql
    
    #https://bitbucket.org/emfabox/beta/raw/d926f58e9d32c9a8ba7c9fc9d97c8913480e23cf/src/sql/opendmarc.sql
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/install/sql/opendmarc.sql https://bitbucket.org/emfabox/beta/raw/d926f58e9d32c9a8ba7c9fc9d97c8913480e23cf/src/sql/opendmarc.sql
    /usr/bin/mysql --default-character-set=utf8 opendmarc < /usr/src/EMFA/install/sql/opendmarc.sql   
     
     
    # Create the users
    
    #SQLGREY
    QUERY_10="GRANT ALL ON sqlgrey.* to sqlgrey@localhost IDENTIFIED BY '${SQLGREY_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_10"
    timewait 2
    
    QUERY_11="GRANT ALL ON sqlgrey.* to mailwatch@localhost IDENTIFIED BY '${MAILSCANNER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_11"
    timewait 2
    
    #CLUEBRINGER  CLUEBRINGER_DBPASS="${PASS7}"
    QUERY_12="GRANT SELECT,INSERT,UPDATE,DELETE ON cluebringer.* TO cluebringer@localhost IDENTIFIED BY '${CLUEBRINGER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_12"
    timewait 2
    
    QUERY_13="GRANT ALL ON cluebringer.* to mailwatch@localhost IDENTIFIED BY '${MAILSCANNER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_13"
    timewait 2
    
    #opendmarc
    QUERY_14="GRANT ALL PRIVILEGES ON opendmarc.* TO opendmarc IDENTIFIED BY '${OPENDMARC_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_14"
    timewait 2
    
    QUERY_15="GRANT ALL ON opendmarc.* to mailwatch@localhost IDENTIFIED BY '${MAILSCANNER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_15"
    timewait 2
     
    #MAILSCANNER
    QUERY_20="GRANT ALL ON mailscanner.* TO mailwatch@localhost IDENTIFIED BY '${MAILSCANNER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_20"
    sleep 2s
    QUERY_21="GRANT FILE ON *.* TO mailwatch@localhost IDENTIFIED BY '${MAILSCANNER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_21"
    timewait 2

    #SA_BAYES
    QUERY_35="GRANT SELECT, INSERT, UPDATE, DELETE ON sa_bayes.* TO 'sa_user'@'localhost' IDENTIFIED BY '${SA_BAYES_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_35"
    timewait 2
    
    #SQLGREY
    QUERY_40="GRANT ALL ON sqlgrey.* to sqlgrey@localhost IDENTIFIED BY '${SQLGREY_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_40"
    timewait 2
    
    
    # CLUSTER
    QUERY_62="GRANT ALL ON cluster.* TO mailwatch@localhost IDENTIFIED BY '${MAILSCANNER_DBPASS}';"
    /usr/bin/mysql -e "$QUERY_62"
    timewait 2
    
    #Q0="FLUSH PRIVILEGES;"
    /usr/bin/mysql -e "$QUERY_0"
    timewait 2
    
    # create startup entry for relay host db --> default to no      
    echo "DELETE FROM allowed_hosts;">/usr/src/allowed_hosts.sql
    echo "INSERT INTO allowed_hosts (active,IP,Client,Comments) VALUES ('no', '${MYIP}', '$HOSTNAME', 'emfabox');">>/usr/src/allowed_hosts.sql
    
    mysql -hlocalhost -uroot -Dmailscanner </usr/src/allowed_hosts.sql
    
    #Q0="FLUSH PRIVILEGES;"
    /usr/bin/mysql -e "$QUERY_0"
    timewait 2

    # remove defaults from MySQL.    
    /usr/bin/mysqladmin -u root password "${MYSQL_ROOTPASS}"  
    /usr/bin/mysqladmin -u root -p"${MYSQL_ROOTPASS}" -h localhost.localdomain password "${MYSQL_ROOTPASS}"

}
# +---------------------------------------------------+


# +---------------------------------------------------+
# MYSQL SSL
# http://blog.schaal-24.de/server/mysql-replikation-mit-ssl-absichern/
# +---------------------------------------------------+
emfa_mysql_ssl () {
mkdir -p /etc/mysql/ssl
chown mysql.mysql /etc/mysql/ssl
chmod 750 /etc/mysql/ssl

cd /etc/mysql/ssl

openssl genrsa 2048 > ca-key.pem

openssl req -new -x509 -nodes -days 1825  -subj "/C=US/ST=*/L=*/O=EMFABOX/CN=*" -key ca-key.pem -out ca-cert.pem
openssl req -new -newkey rsa:4096 -days 1825 -nodes -x509 -subj "/C=US/ST=*/L=*/O=EMFABOX/CN=*" -keyout server1-key.pem -out server1-req.pem
openssl rsa -in server1-key.pem -out server1-key.pem
openssl x509 -req -in server1-req.pem -days 1825 -CA ca-cert.pem -CAkey ca-key.pem -set_serial 01 -out server1-cert.pem

openssl req -newkey rsa:2048 -days 1825 -nodes -subj "/C=US/ST=*/L=*/O=EMFABOX/CN=*" -keyout server2-key.pem -out server2-req.pem
openssl rsa -in server2-key.pem -out server2-key.pem

openssl x509 -req -in server2-req.pem -days 1825 -CA ca-cert.pem -CAkey ca-key.pem -set_serial 01 -out server2-cert.pem

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# configure opendkim
# http://www.opendkim.org/
# +---------------------------------------------------+
emfa_opendkim () {
#db passwords
source /etc/emfa/variables.conf

 
 # 2015-04-02
 # prepare db connection
 # upgrade to  opendkim-2.10.1 + database support
 

mkdir -p /usr/src/EMFA/install/opendkim/

cd  /usr/src/EMFA/install/opendkim/

#https://bitbucket.org/emfabox/beta/src/5a37d93421a87a48cf04626d0c1e8a6f09eb6822/src/opendkim/opendbx-1.4.6-68.2.x86_64.rpm?at=master
/usr/bin/wget --no-check-certificate -O opendbx-1.4.6-68.2.x86_64.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/opendkim/opendbx-1.4.6-68.2.x86_64.rpm 
 
#https://bitbucket.org/emfabox/beta/src/5a37d93421a87a48cf04626d0c1e8a6f09eb6822/src/opendkim/opendbx-devel-1.4.6-68.2.x86_64.rpm?at=master
/usr/bin/wget --no-check-certificate -O opendbx-devel-1.4.6-68.2.x86_64.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/opendkim/opendbx-devel-1.4.6-68.2.x86_64.rpm
 
#https://bitbucket.org/emfabox/beta/src/5a37d93421a87a48cf04626d0c1e8a6f09eb6822/src/opendkim/opendbx-mysql-1.4.6-68.2.x86_64.rpm?at=master
/usr/bin/wget --no-check-certificate -O opendbx-mysql-1.4.6-68.2.x86_64.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/opendkim/opendbx-mysql-1.4.6-68.2.x86_64.rpm
 
#https://bitbucket.org/emfabox/beta/src/5a37d93421a87a48cf04626d0c1e8a6f09eb6822/src/opendkim/opendkim-2.10.1.tar.gz?at=master
#/usr/bin/wget --no-check-certificate -O opendkim-2.10.1.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/opendkim/opendkim-2.10.1.tar.gz

#https://bitbucket.org/emfabox/beta/raw/a79870a6e5184091b6cb329be428a96dac7e1c9f/src/opendkim/opendkim-2.10.3.tar.gz
/usr/bin/wget --no-check-certificate -O opendkim-2.10.3.tar.gz	https://bitbucket.org/emfabox/beta/raw/a79870a6e5184091b6cb329be428a96dac7e1c9f/src/opendkim/opendkim-2.10.3.tar.gz
 
#https://bitbucket.org/emfabox/beta/raw/50e0cc26830a7036b70fe580230458830e3ca24a/src/opendkim/opendkim
#/usr/bin/wget --no-check-certificate -O /etc/init.d/opendkim https://bitbucket.org/emfabox/beta/raw/50e0cc26830a7036b70fe580230458830e3ca24a/src/opendkim/opendkim
 
#https://bitbucket.org/emfabox/beta/raw/cf078014ee5d15615675ad4033617c7d31beacc6/src/opendkim/opendkim.conf
/usr/bin/wget --no-check-certificate -O /etc/opendkim.conf https://bitbucket.org/emfabox/beta/raw/cf078014ee5d15615675ad4033617c7d31beacc6/src/opendkim/opendkim.conf
 
 
cd  /usr/src/EMFA/install/opendkim/
 
rpm -Uhv opendbx-1.4.6-68.2.x86_64.rpm
 
rpm -Uhv opendbx-devel-1.4.6-68.2.x86_64.rpm

rpm -Uhv opendbx-mysql-1.4.6-68.2.x86_64.rpm

yum install -y openssl-devel libbsd-devel sendmail-devel
 
tar -xzvf opendkim-2.10.3.tar.gz
 
cd  opendkim-*
./configure --with-odbx --with-sql-backend=mysql --sysconfdir=/etc --prefix=/usr --localstatedir=/var
make
make install

ldconfig

timewait 2

useradd -r -U -s /sbin/nologin opendkim 

yes | cp /usr/src/EMFA/install/opendkim/opendkim-2.10.3/contrib/init/redhat/opendkim /etc/init.d/

chmod 755 /etc/init.d/opendkim

#yes | cp  /usr/share/doc/opendkim/opendkim.conf.sample  /etc/opendkim.conf

cd /etc/init.d

## remove sendmail 
chkconfig sendmail off
service sendmail stop
chkconfig --del sendmail

# change start up after mysql starts #
sed -i '/^# chkconfig: - 41 59/ c\# chkconfig: - 71 59' /etc/init.d/opendkim

chkconfig --add opendkim
chkconfig opendkim on

cd /etc

mkdir -p /etc/opendkim/keys
chown -R opendkim:opendkim /etc/opendkim
chmod -R go-wrx /etc/opendkim/keys

if [ ! -d /var/run/opendkim ]; then
mkdir /var/run/opendkim
fi

chown -R opendkim:opendkim /var/run/opendkim

OPENDKIM_DIR="/etc/opendkim/keys/"
   
cat << EOF > /etc/opendkim/TrustedHosts
# OPENDKIM TRUSTED HOSTS
# To use this file, uncomment the #ExternalIgnoreList and/or the #InternalHosts
# option in /etc/opendkim.conf then restart OpenDKIM. Additional hosts
# may be added on separate lines (IP addresses, hostnames, or CIDR ranges).
# The localhost IP (127.0.0.1) should always be the first entry in this file.
127.0.0.1
localhost
${EMFABOX_DOMAIN}
${EMFABOX_HOSTNAME}
${MYIP}
EOF

timewait 2  

# Configure Opendkim

if [ -f /etc/opendkim.conf.orig ]; then

yes | mv  /etc/opendkim.conf.orig  /etc/opendkim.conf.orig.$(date +"%Y-%m-%d_%H-%M-%S")
    
fi

yes | cp /etc/opendkim.conf /etc/opendkim.conf.$(date +"%Y-%m-%d_%H-%M-%S")

timewait 2


sed -i '/^Mode/ c\Mode sv' /etc/opendkim.conf
sed -i '/^KeyFile/ c\#KeyFile /etc/opendkim/keys/default.private' /etc/opendkim.conf
sed -i '/^#ExternalIgnoreList/ c\ExternalIgnoreList refile:/etc/opendkim/TrustedHosts' /etc/opendkim.conf
sed -i '/^#InternalHosts/ c\InternalHosts refile:/etc/opendkim/TrustedHosts' /etc/opendkim.conf
sed -i '/^#SigningTable/ c\SigningTable dsn:mysql://mailwatch:'${MAILSCANNER_DBPASS}'@localhost/mailscanner/table=dkim?keycol=domain_name?datacol=id' /etc/opendkim.conf
sed -i '/^#KeyTable/ c\KeyTable  dsn:mysql://mailwatch:'${MAILSCANNER_DBPASS}'@localhost/mailscanner/table=dkim?keycol=id?datacol=domain_name,selector,private_key' /etc/opendkim.conf   

timewait 2   
   
# 2015-04-03
# remove sendmail
yum remove -y sendmail  

}
# +---------------------------------------------------+


# +---------------------------------------------------+
# configure opendmarc
# http://blog.schaal-24.de/mail/dmarc-check-unter-debian-wheezy/?lang=en
# +---------------------------------------------------+
emfa_opendmarc () {

#db passwords
source /etc/emfa/variables.conf

yum install -y opendmarc

cd /etc

mv /etc/opendmarc.conf  /etc/opendmarc.conf.install

hostf=$(hostname)

#https://bitbucket.org/emfabox/beta/raw/0d8789a7061b8e5723d4f21b8850aaef44d620c2/src/opendmarc/opendmarc.conf.install.emfa.txt
/usr/bin/wget --no-check-certificate -O /etc/opendmarc.conf https://bitbucket.org/emfabox/beta/raw/0d8789a7061b8e5723d4f21b8850aaef44d620c2/src/opendmarc/opendmarc.conf.install.emfa.txt

sed -i '/^#AuthservID name/ c\AuthservID HOSTNAME'  /etc/opendmarc.conf
sed -i '/^#HistoryFile/ c\HistoryFile /opt/opendmarc.dat'  /etc/opendmarc.conf
sed -i '/^HistoryFile/ c\HistoryFile /opt/opendmarc.dat'  /etc/opendmarc.conf
sed -i '/^#PublicSuffixList path/ c\PublicSuffixList /etc/opendmarc/effective_tld_names.dat'  /etc/opendmarc.conf
sed -i '/^#RecordAllMessages/ c\RecordAllMessages true'  /etc/opendmarc.conf

postconf -e "smtpd_milters = inet:127.0.0.1:8893"
postconf -e "non_smtpd_milters = \$smtpd_milters"
postconf -e "milter_default_action = accept" 

hash -r

service opendmarc start

#postfix reload
chkconfig opendmarc on
/usr/bin/wget --no-check-certificate -q -N -P /etc/opendmarc https://publicsuffix.org/list/effective_tld_names.dat
chown opendmarc:opendmarc /etc/opendmarc/effective_tld_names.dat

printf "SHELL=/bin/bash\n" > /etc/cron.weekly/opendmarc.cron
printf "PATH=/sbin:/bin:/usr/sbin:/usr/bin\n" >> /etc/cron.weekly/opendmarc.cron    
printf "# Runs the Public Suffix List update program\n"  >> /etc/cron.weekly/opendmarc.cron
printf "# This will run weekly\n" >> /etc/cron.weekly/opendmarc.cron
printf "/usr/bin/wget --no-check-certificate -q -N -P /etc/opendmarc https://publicsuffix.org/list/effective_tld_names.dat &> /dev/null\n" >> /etc/cron.weekly/opendmarc.cron
printf "#\n" >> /etc/cron.weekly/opendmarc.cron
clear

chmod 755 /etc/cron.weekly/opendmarc.cron


#https://bitbucket.org/emfabox/beta/raw/159f9f1d6372d54daea16c2c8fd05fa13a5dd4ea/src/scripts/opendmarc-send-reports.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/opendmarc-send-reports.sh https://bitbucket.org/emfabox/beta/raw/159f9f1d6372d54daea16c2c8fd05fa13a5dd4ea/src/scripts/opendmarc-send-reports.sh 

chmod 755 /usr/local/sbin/opendmarc-send-reports.sh

printf "SHELL=/bin/bash\n" > /etc/cron.weekly/opendmarcreport.cron
printf "PATH=/sbin:/bin:/usr/sbin:/usr/bin\n" >> /etc/cron.weekly/opendmarcreport.cron   
printf "# Runs the OpenDMARC report program\n"  >> /etc/cron.weekly/opendmarcreport.cron
printf "# This will run weekly\n" >> /etc/cron.weekly/opendmarcreport.cron
printf "/usr/local/sbin/opendmarc-send-reports.sh &> /dev/null\n" >> /etc/cron.weekly/opendmarc.cron
printf "#\n" >>  /etc/cron.weekly/opendmarc.cron
clear

chmod 755 /etc/cron.weekly/opendmarcreport.cron

#https://bitbucket.org/emfabox/beta/raw/83469c815d5cbeab7e3f9bdf6efc73910ea65c0b/src/opendmarc/opendmarc-import.pl
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/opendmarc-import https://bitbucket.org/emfabox/beta/raw/83469c815d5cbeab7e3f9bdf6efc73910ea65c0b/src/opendmarc/opendmarc-import.pl
chmod 755 /usr/local/sbin/opendmarc-import

#https://bitbucket.org/emfabox/beta/raw/711f45e3d2dc4c568e5a746c36363f4dac3ea411/src/opendmarc/opendmarc-expire.pl
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/opendmarc-expire https://bitbucket.org/emfabox/beta/raw/711f45e3d2dc4c568e5a746c36363f4dac3ea411/src/opendmarc/opendmarc-expire.pl
chmod 755 /usr/local/sbin/opendmarc-expire

#https://bitbucket.org/emfabox/beta/raw/711f45e3d2dc4c568e5a746c36363f4dac3ea411/src/opendmarc/opendmarc-reports.pl
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/opendmarc-reports https://bitbucket.org/emfabox/beta/raw/711f45e3d2dc4c568e5a746c36363f4dac3ea411/src/opendmarc/opendmarc-reports.pl
chmod 755 /usr/local/sbin/opendmarc-reports

service opendmarc stop


#Jun 30 09:07:49 mail01 opendmarc[1996]: C19455FE1A: /opt/opendmarc.dat: fopen(): Permission denied

touch /opt/opendmarc.dat

chown opendmarc:opendmarc  /opt/opendmarc.dat
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# configure SQLgrey
# http://sqlgrey.sourceforge.net/
# +---------------------------------------------------+
emfa_sqlgrey () {
    cd ${SOURCE_DIR}
    useradd sqlgrey -m -d /home/sqlgrey -s /sbin/nologin
    wget $mirror/$mirrorpath/sqlgrey-1.8.0.tar.gz
    tar -xvzf sqlgrey-1.8.0.tar.gz
    cd sqlgrey-1.8.0
    make rh-install

    # pre-create the local files so users won't be confused if the file is not there.
    touch /etc/sqlgrey/clients_ip_whitelist.local
    touch /etc/sqlgrey/clients_fqdn_whitelist.local

    # Make the changes to the config file...
    sed -i '/conf_dir =/ c\conf_dir = /etc/sqlgrey' /etc/sqlgrey/sqlgrey.conf
    sed -i '/user =/ c\user = sqlgrey' /etc/sqlgrey/sqlgrey.conf
    sed -i '/group =/ c\group = sqlgrey' /etc/sqlgrey/sqlgrey.conf
    sed -i '/confdir =/ c\confdir = /etc/sqlgrey' /etc/sqlgrey/sqlgrey.conf
    sed -i '/connect_src_throttle =/ c\connect_src_throttle = 5' /etc/sqlgrey/sqlgrey.conf
    sed -i "/awl_age = 32/d" /etc/sqlgrey/sqlgrey.conf
    sed -i "/group_domain_level = 10/d" /etc/sqlgrey/sqlgrey.conf
    sed -i '/awl_age =/ c\awl_age = 60' /etc/sqlgrey/sqlgrey.conf
    sed -i '/group_domain_level =/ c\group_domain_level = 2' /etc/sqlgrey/sqlgrey.conf
    sed -i '/db_type =/ c\db_type = mysql' /etc/sqlgrey/sqlgrey.conf
    sed -i '/db_name =/ c\db_name = sqlgrey' /etc/sqlgrey/sqlgrey.conf
    sed -i '/db_host =/ c\db_host = localhost' /etc/sqlgrey/sqlgrey.conf
    sed -i '/db_port =/ c\db_port = default' /etc/sqlgrey/sqlgrey.conf
    sed -i '/db_user =/ c\db_user = sqlgrey' /etc/sqlgrey/sqlgrey.conf
    sed -i "/db_pass =/ c\db_pass = $SQLGREY_DBPASS" /etc/sqlgrey/sqlgrey.conf
    sed -i '/db_cleandelay =/ c\db_cleandelay = 1800' /etc/sqlgrey/sqlgrey.conf
    sed -i '/clean_method =/ c\clean_method = sync' /etc/sqlgrey/sqlgrey.conf
    sed -i '/prepend =/ c\prepend = 1' /etc/sqlgrey/sqlgrey.conf
    sed -i "/reject_first_attempt\/reject_early_reconnect/d" /etc/sqlgrey/sqlgrey.conf
    sed -i '/reject_first_attempt =/ c\reject_first_attempt = immed' /etc/sqlgrey/sqlgrey.conf
    sed -i '/reject_early_reconnect =/ c\reject_early_reconnect = immed' /etc/sqlgrey/sqlgrey.conf
    sed -i "/reject_code = dunno/d" /etc/sqlgrey/sqlgrey.conf
    sed -i '/reject_code =/ c\reject_code = 451' /etc/sqlgrey/sqlgrey.conf
    sed -i '/whitelists_host =/ c\whitelists_host = sqlgrey.bouton.name' /etc/sqlgrey/sqlgrey.conf
    sed -i '/optmethod =/ c\optmethod = optout' /etc/sqlgrey/sqlgrey.conf

    # start and stop sqlgrey (first launch will create all database tables)
    # We give it 15 seconds to populate the database and then stop it again.
    service sqlgrey start
    sleep 15
    service sqlgrey stop
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Install cluebringer
# +---------------------------------------------------+
emfa_cluebringer () {
    #db passwords
    source /etc/emfa/variables.conf

    mkdir -p /usr/src/EMFA/install/cluebringer/
    
    groupadd cluebringer
    useradd -m -d /home/cluebringer -s /sbin/nologin -g cluebringer cluebringer

    cd /usr/src/EMFA/install/cluebringer/
    
    #https://bitbucket.org/emfabox/beta/src/4cd9c60a3289fb96b08f0b2e3f3ae9366115c002/src/cluebringer/cluebringer-2.0.14-1.noarch.rpm?at=master
 
    /usr/bin/wget --no-check-certificate -O cluebringer-2.0.14-1.noarch.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/cluebringer/cluebringer-2.0.14-1.noarch.rpm
    
    #https://bitbucket.org/emfabox/beta/src/d7f373efe975cfabcf6215d0cc0138493d763a44/src/cluebringer/perl-Cache-FastMmap-1.34-1.el6.rf.x86_64.rpm?at=master
    /usr/bin/wget --no-check-certificate -O perl-Cache-FastMmap-1.34-1.el6.rf.x86_64.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/cluebringer/perl-Cache-FastMmap-1.34-1.el6.rf.x86_64.rpm
     
     #https://bitbucket.org/emfabox/beta/src/d7f373efe975cfabcf6215d0cc0138493d763a44/src/cluebringer/perl-Config-IniFiles-2.56-1.el6.rf.noarch.rpm?at=master
    /usr/bin/wget --no-check-certificate -O perl-Config-IniFiles-2.56-1.el6.rf.noarch.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/cluebringer/perl-Config-IniFiles-2.56-1.el6.rf.noarch.rpm
    
     # https://bitbucket.org/emfabox/beta/src/9f209495d4e22c3a3118e35ec5930b93e15903b3/src/cluebringer/perl-Mail-SPF-2.007-1.el6.noarch.rpm?at=master
    /usr/bin/wget --no-check-certificate -O perl-Mail-SPF-2.007-1.el6.noarch.rpm  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/cluebringer/perl-Mail-SPF-2.007-1.el6.noarch.rpm
     
    rpm -Uhv perl-Cache-FastMmap-1.34-1.el6.rf.x86_64.rpm
    
    rpm -Uhv perl-Config-IniFiles-2.56-1.el6.rf.noarch.rpm
    
    #rpm -Uhv perl-Mail-SPF-2.007-1.el6.noarch.rpm
    
    rpm -Uhv cluebringer-2.0.14-1.noarch.rpm
    
    cd /etc/policyd/
    
    mkdir -p /opt/emfa/install/backup/policyd/
    
    cp -rp /etc/policyd/*.conf /opt/emfa/install/backup/policyd/
    
    cp -rp  /etc/policyd/cluebringer.conf  /etc/policyd/cluebringer.conf.install
    
    rm -rf /etc/policyd/cluebringer.conf
     
    /usr/bin/wget --no-check-certificate -O /etc/policyd/cluebringer.conf https://bitbucket.org/emfabox/beta/raw/5d5eaffec66e26acecb8b782bbce90ce9da0b23a/src/cluebringer/cluebringer.conf

    sed -i "/^DSN =/ c\DSN = DBI:mysql:database=cluebringer;host=localhost;user=cluebringer;password=${CLUEBRINGER_DBPASS}" /etc/policyd/cluebringer.conf
    sed -i "/^Password =/ c\Password = ${CLUEBRINGER_DBPASS}" /etc/policyd/cluebringer.conf

    # repair  /usr/sbin/cbpadmin
    sed -i "/^use lib/ c\use lib('/usr/local/lib/policyd-2.0','/usr/lib/policyd-2.0','/usr/lib64/policyd-2.0');" /usr/sbin/cbpadmin 
        
    
    #Start Cluebringer service
    /etc/init.d/cbpolicyd restart
    chkconfig --level 345 cbpolicyd on
    
    # emfabox cluebringer addons
    
     cd /usr/src/EMFA/install/cluebringer/
     
     mkdir -p /usr/src/EMFA/install/cluebringer/addons
     
     cd  /usr/src/EMFA/install/cluebringer/addons
     
     #https://bitbucket.org/emfabox/beta/src/e61450171b13240956b50b3319f2b33417435b6a/src/addons/python-IPy-0.75-1.el6.noarch.rpm?at=master
     /usr/bin/wget --no-check-certificate -O python-IPy-0.75-1.el6.noarch.rpm https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/addons/python-IPy-0.75-1.el6.noarch.rpm
     
     rpm -Uhv python-IPy-0.75-1.el6.noarch.rpm
     
     #https://bitbucket.org/emfabox/beta/raw/7cf62a550ae0abb1b6a6a5935ab79b612cb37206/src/addons/rblwatch.py
      /usr/bin/wget --no-check-certificate -O /usr/local/sbin/rblwatch.py https://bitbucket.org/emfabox/beta/raw/7cf62a550ae0abb1b6a6a5935ab79b612cb37206/src/addons/rblwatch.py
      
      chmod 755 /usr/local/sbin/rblwatch.py
      ## setup for small size
      
sed -i '/min_servers=/d' /etc/policyd/cluebringer.conf
sed -i '/min_spare_servers=/d' /etc/policyd/cluebringer.conf
sed -i '/max_spare_servers=/d' /etc/policyd/cluebringer.conf
sed -i '/max_servers=/d' /etc/policyd/cluebringer.conf
sed -i '/max_requests=/d' /etc/policyd/cluebringer.conf

/bin/sed -i '/^# Large mailserver/amax_requests=1000' /etc/policyd/cluebringer.conf
/bin/sed -i '/^# Large mailserver/amax_servers=10' /etc/policyd/cluebringer.conf
/bin/sed -i '/^# Large mailserver/amax_spare_servers=4' /etc/policyd/cluebringer.conf
/bin/sed -i '/^# Large mailserver/amin_spare_servers=2' /etc/policyd/cluebringer.conf
/bin/sed -i '/^# Large mailserver/amin_servers=2' /etc/policyd/cluebringer.conf


mv -f  /etc/httpd/conf.d/cluebringer.conf  /opt/emfa/backup/cluebringer.conf.$(date +"%Y-%m-%d_%H-%M-%S")
  
# reinit List::Util 
 cd /root/.cpan/CPAN
 export PERL_MM_USE_DEFAULT=1
 export PERL_EXTUTILS_AUTOINSTALL="--defaultdeps"
 perl -MCPAN -e "CPAN::Shell->force(qw(install List::Util));"
 
 
 # 
 cd /etc/cron.daily


#cbpolicyd_cleanup.sh
printf '#!/bin/bash\n'>/etc/cron.daily/cbpolicyd_cleanup.sh
printf '#\n'>>/etc/cron.daily/cbpolicyd_cleanup.sh
printf '\n'>>/etc/cron.daily/cbpolicyd_cleanup.sh
printf  '/usr/local/sbin/emfa/cbpadmin --cleanup >/dev/null\n'>>/etc/cron.daily/cbpolicyd_cleanup.sh

chmod 755 /etc/cron.daily/cbpolicyd_cleanup.sh

#cbpadmin
cd /usr/local/sbin/emfa/

#https://bitbucket.org/emfabox/update/raw/870e7796b4d45dd86d103e93f817aa4ec73e880e/2000/usr/local/sbin/emfa/cbpadmin
/usr/bin/wget --no-check-certificate -O cbpadmin https://bitbucket.org/emfabox/update/raw/870e7796b4d45dd86d103e93f817aa4ec73e880e/2000/usr/local/sbin/emfa/cbpadmin
chmod 755 cbpadmin
   
 }
# +---------------------------------------------------+

# +---------------------------------------------------+
# Postfix
# +---------------------------------------------------+
 emfa_postfix () {

    #db passwords
    source /etc/emfa/variables.conf
# backup posfix *.cf files

      mkdir -p /opt/emfa/install/backup/postfix/    
      cp -rp /etc/postfix/*.cf /opt/emfa/install/backup/postfix/
      # cp -f /etc/postfix/*.cf /opt/emfa/install/backup/postfix/
  
      grep -v "^#"  /etc/postfix/main.cf > /etc/postfix/main.new 
  
      mv  /etc/postfix/main.cf /etc/postfix/main.commited
      #rm -rf /etc/postfix/main.cf
  
      sed '/^$/d' /etc/postfix/main.new > /etc/postfix/main.cf
      rm -rf /etc/postfix/main.new
    
          if [ ! -d /etc/postfix/ssl ] ; then
               mkdir /etc/postfix/ssl
          fi
          
     # https://bitbucket.org/emfabox/beta/raw/b568f5ec4f0ecedccf40ce00064049f7fc00c4fd/src/postfix-out/bounce.cf
     /usr/bin/wget --no-check-certificate -O /etc/postfix/bounce.cf https://bitbucket.org/emfabox/beta/raw/b568f5ec4f0ecedccf40ce00064049f7fc00c4fd/src/postfix-out/bounce.cf
          
      #mailscanner config    
      echo /^Received:/ HOLD>>/etc/postfix/header_checks
      echo /^Received:.*/\[127\.0\.0\.1/ PREPEND X-EMFABox-Origin:Yes>>/etc/postfix/header_checks
      
      # Remove the characters before first dot in myhostname is mydomain.
    echo "${HOSTNAME}" | grep '\..*\.' >/dev/null 2>&1
    if [ X"$?" == X"0" ]; then
        mydomain="$(echo "${HOSTNAME}" | awk -F'.' '{print $2 "." $3}')"
        postconf -e mydomain="${mydomain}"
    else
        postconf -e mydomain="${HOSTNAME}"
    fi
    
      postconf -e "inet_protocols = ipv4"
      postconf -e "inet_interfaces = all"
      postconf -e "mynetworks = 127.0.0.0/8"
      postconf -e "mynetworks_style= host"
      postconf -e "header_checks = regexp:/etc/postfix/header_checks"
      postconf -e "myorigin = \$mydomain"
      #postconf -e "mydestination = \$myhostname, localhost.\$mydomain, localhost"
      postconf -e mydestination="\$myhostname, localhost, localhost.localdomain, localhost.\$myhostname"
      postconf -e "relay_domains = hash:/etc/postfix/transport"
      postconf -e "transport_maps = hash:/etc/postfix/transport"
      postconf -e "local_recipient_maps = "
      postconf -e "smtpd_helo_required = yes"
      postconf -e "smtpd_delay_reject = yes"
      postconf -e "disable_vrfy_command = yes"
      postconf -e "virtual_alias_maps = hash:/etc/postfix/virtual"
      postconf -e "alias_maps = hash:/etc/aliases"
      postconf -e "alias_database = hash:/etc/aliases"
      postconf -e "default_destination_recipient_limit = 1"
      # SASL config
      postconf -e "broken_sasl_auth_clients = yes"
      postconf -e "smtpd_sasl_auth_enable = yes"
      postconf -e "smtpd_sasl_local_domain = "
      postconf -e "smtpd_sasl_path = smtpd"
      postconf -e "smtpd_sasl_local_domain = $myhostname"
      postconf -e "smtpd_sasl_security_options = noanonymous"
      postconf -e "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd"
      postconf -e "smtp_sasl_type = cyrus"
      # tls config
      postconf -e "smtp_use_tls = yes"
      postconf -e "smtpd_use_tls = yes"
      postconf -e "smtp_tls_CAfile = /etc/postfix/ssl/smtpd.pem"
      postconf -e "smtp_tls_session_cache_database = btree:/var/lib/postfix/smtp_tls_session_cache"
      postconf -e "smtp_tls_note_starttls_offer = yes"
      postconf -e "smtpd_tls_key_file = /etc/postfix/ssl/smtpd.pem"
      postconf -e "smtpd_tls_cert_file = /etc/postfix/ssl/smtpd.pem"
      postconf -e "smtpd_tls_CAfile = /etc/postfix/ssl/smtpd.pem"
      postconf -e "smtpd_tls_loglevel = 1"
      postconf -e "smtpd_tls_received_header = yes"
      postconf -e "smtpd_tls_session_cache_timeout = 3600s"
      postconf -e "tls_random_source = dev:/dev/urandom"
      postconf -e "smtpd_tls_session_cache_database = btree:/var/lib/postfix/smtpd_tls_session_cache"
      postconf -e "smtpd_tls_security_level = may"
      postconf -e "smtp_tls_note_starttls_offer = yes"
      # Poodle
      # Disable SSLv2 & SSLv3 
      postconf -e 'smtpd_tls_mandatory_protocols = !SSLv2,!SSLv3'
      postconf -e 'smtp_tls_mandatory_protocols = !SSLv2,!SSLv3'
      postconf -e 'smtpd_tls_protocols = !SSLv2,!SSLv3'
      postconf -e 'smtp_tls_protocols = !SSLv2,!SSLv3'
      # restrictions
      #postconf -e "smtpd_helo_restrictions =  check_helo_access hash:/etc/postfix/helo_access, reject_invalid_hostname"
      postconf -e "smtpd_helo_restrictions = permit_mynetworks,permit_sasl_authenticated,check_client_access mysql:/etc/postfix/sql/mysql_rbl_override.cf,reject_invalid_helo_hostname,permit"
      postconf -e "smtpd_sender_restrictions = permit_mynetworks,permit_sasl_authenticated,check_client_access mysql:/etc/postfix/sql/mysql_rbl_override.cf, check_sender_access hash:/etc/postfix/sender_access, reject_non_fqdn_sender,reject_invalid_hostname, reject_unknown_sender_domain,permit"
      postconf -e "smtpd_data_restrictions =  reject_unauth_pipelining"
      postconf -e "smtpd_client_restrictions = permit_sasl_authenticated, reject_rbl_client zen.spamhaus.org"
      #postconf -e "smtpd_recipient_restrictions =  permit_sasl_authenticated, permit_mynetworks, check_recipient_access mysql:/etc/postfix/sql/mysql-virtual_recipient.cf, reject_unauth_destination, check_policy_service inet:127.0.0.1:2501"
      ## enable spf policy_service
      postconf -e "smtpd_recipient_restrictions =  check_policy_service inet:127.0.0.1:10031, permit_sasl_authenticated, permit_mynetworks, ,check_client_access mysql:/etc/postfix/sql/mysql_rbl_override.cf,check_recipient_access mysql:/etc/postfix/sql/mysql_virtual_recipient_access.cf,reject_non_fqdn_sender,reject_non_fqdn_recipient,reject_unknown_sender_domain,reject_unknown_recipient_domain, reject_unauth_destination, check_policy_service inet:127.0.0.1:2501, check_policy_service unix:private/policyd-spf,check_sender_access mysql:/etc/postfix/sql/mysql_domain_antispoofing.cf,permit"
      #http://www.webstershome.co.uk/2014/04/07/postfix-whitelisting-spf-filtering/
      #postconf -e "smtpd_recipient_restrictions = permit_mynetworks, permit_inet_interfaces, permit_sasl_authenticated, reject_unauth_pipelining, reject_invalid_hostname, reject_non_fqdn_hostname, reject_unknown_recipient_domain, reject_unauth_destination, check_client_access hash:/etc/postfix/rbl_override_whitelist, check_policy_service unix:private/policyd-spf, reject_rbl_client dnsbl.sorbs.net, reject_rbl_client zen.spamhaus.org, reject_rbl_client bl.spamcop.net, permit" 
      postconf -e "smtpd_end_of_data_restrictions = check_policy_service inet:127.0.0.1:10031"
      postconf -e "masquerade_domains = \$mydomain"
      #other configuration files
      
      postconf -e "smtpd_sasl_authenticated_header = yes"
      postconf -e "unverified_recipient_reject_code = 550"
      postconf -e "smtp_host_lookup = native, dns"
      
      
      /usr/bin/newaliases
     
      touch /etc/postfix/sender_access
      touch /etc/postfix/transport
      touch /etc/postfix/virtual
      touch /etc/postfix/helo_access
      touch /etc/postfix/sender_access
      echo "/(.*)/  prepend X-Envelope-From: <\$1>" > /etc/postfix/sender_access
      
      touch /etc/postfix/recipient_access
      touch /etc/postfix/sasl_passwd
      touch /etc/postfix/tls_policy
     
      /usr/sbin/postmap /etc/postfix/transport
      /usr/sbin/postmap /etc/postfix/virtual
      /usr/sbin/postmap /etc/postfix/helo_access
      /usr/sbin/postmap /etc/postfix/sender_access
      /usr/sbin/postmap /etc/postfix/recipient_access
      /usr/sbin/postmap /etc/postfix/sasl_passwd
      /usr/sbin/postmap /etc/postfix/tls_policy
      
      
    printf '/^127\.0\.0\.1$/ 550 You are using my IP address for ID. Am I talking to myself?'"\n" > /etc/postfix/helo_checks
    /usr/sbin/postmap /etc/postfix/helo_checks
      
      
    # Change perms on /etc/postfix/sasl_passwd to 600 
    chmod 0600 /etc/postfix/sasl_passwd

    # Logjam Vulnerability 
    openssl dhparam -out /etc/postfix/ssl/dhparam.pem 2048
    postconf -e "smtpd_tls_dh1024_param_file = /etc/postfix/ssl/dhparam.pem"
    #postconf -e "smtpd_tls_ciphers = low"
    
    postconf -e "smtpd_tls_ciphers = high"
    postconf -e "smtpd_tls_exclude_ciphers = aNULL,MD5,DES"
      
      echo "pwcheck_method: auxprop">/usr/lib64/sasl2/smtpd.conf
      echo "auxprop_plugin: sasldb">>/usr/lib64/sasl2/smtpd.conf
      echo "mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5">>/usr/lib64/sasl2/smtpd.conf
      
      #EMFABOX 
      #change banner
      postconf -e "smtpd_banner = \$myhostname EMFABOX ESMTP MAIL Service"   
      postconf -e "biff = no" 
      postconf -e "message_size_limit = 50000000"
      postconf -e "default_destination_concurrency_limit = 40"    
      postconf -e "unknown_local_recipient_reject_code = 450"
      #
      postconf -e receive_override_options='no_address_mappings'
      #postconf -e smtpd_data_restrictions='reject_unauth_pipelining'
      postconf -e smtpd_reject_unlisted_recipient='yes'   # Default
      postconf -e delay_warning_time='4h'
      postconf -e policy_time_limit='3600'
      postconf -e maximal_queue_lifetime='1d'
      postconf -e bounce_queue_lifetime='1d'
      
      ## SLEEP TIME 20-01-2015
      postconf -e smtpd_error_sleep_time='1s'
      postconf -e smtpd_soft_error_limit='10'
      #postconf -e smtpd_hard_error_limit='20'
      
      postconf -e smtpd_timeout='${stress?10}${stress:300}s'
      postconf -e smtpd_hard_error_limit='${stress?1}${stress:20}'
      postconf -e smtpd_junk_command_limit='${stress?1}${stress:100}'
      
            
# START SQL POSTFIX PART 
    
    if [ ! -d /etc/postfix/sql ] ; then
           mkdir -p /etc/postfix/sql
    fi
    # disable relay_domains
    sed -i '/^relay_domains =/ c\#relay_domains = hash:/etc/postfix/transport' /etc/postfix/main.cf
    #
    postconf -e "virtual_alias_maps = mysql:/etc/postfix/sql/mysql_virtual_alias_maps.cf"
    postconf -e "virtual_mailbox_domains = mysql:/etc/postfix/sql/mysql_virtual_domains_maps.cf"
    postconf -e "virtual_mailbox_maps = mysql:/etc/postfix/sql/mysql_virtual_mailbox_maps.cf"
    postconf -e transport_maps='proxy:mysql:/etc/postfix/sql/mysql_transport_maps_domain.cf'
    #
    # restrict network
    postconf -e mynetworks="127.0.0.0/8, proxy:mysql:/etc/postfix/sql/mysql_mynetworks.cf"
    # client restrictions
    postconf -e smtpd_client_restrictions="permit_mynetworks, permit_sasl_authenticated, check_client_access mysql:/etc/postfix/sql/mysql_rbl_override.cf, mysql:/etc/postfix/sql/mysql_postfixspammer.cf, reject_rbl_client b.barracudacentral.org, reject_rbl_client bl.spamcop.net, reject_rbl_client ix.dnsbl.manitu.net, reject_rbl_client zen.spamhaus.org"
    #mysql_mynetworks_cf
    
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_mynetworks.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "table = allowed_hosts" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "select_field = IP" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "where_field = IP" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_mynetworks.cf
    echo "additional_conditions = AND active='yes'" >> /etc/postfix/sql/mysql_mynetworks.cf
    

    ## mailwatch2rbl
    
    #mysql_postfixspammer_cf  
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_postfixspammer.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_postfixspammer.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_postfixspammer.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_postfixspammer.cf
    echo "table = mwrblblock" >> /etc/postfix/sql/mysql_postfixspammer.cf
    echo "select_field = errormsg" >> /etc/postfix/sql/mysql_postfixspammer.cf
    echo "where_field = clientip" >> /etc/postfix/sql/mysql_postfixspammer.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_postfixspammer.cf
    
    ## mysql_transport_maps_domain.cf
    echo "# Configuration written by EMFABox install script @ ${CreateDate} " > /etc/postfix/sql/mysql_transport_maps_domain.cf
    echo "user = ${MAILSCANNER_DBUSER} " >> /etc/postfix/sql/mysql_transport_maps_domain.cf
    echo "password = ${MAILSCANNER_DBPASS} " >> /etc/postfix/sql/mysql_transport_maps_domain.cf
    echo "hosts = ${DBHOST} " >> /etc/postfix/sql/mysql_transport_maps_domain.cf
    echo "port = ${MYSQL_PORT} " >> /etc/postfix/sql/mysql_transport_maps_domain.cf
    echo "dbname = ${MAILSCANNER_DBNAME} " >> /etc/postfix/sql/mysql_transport_maps_domain.cf
    echo "query = SELECT transport FROM domain WHERE domain='%s' AND active=1" >> /etc/postfix/sql/mysql_transport_maps_domain.cf
    
    ## mysql_transport_maps_user.cf  
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "user = ${MAILSCANNER_DBUSER} " >> /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "password = ${MAILSCANNER_DBPASS} " >> /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "hosts = ${DBHOST} " >> /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "port = ${MYSQL_PORT} " >> /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "dbname = ${MAILSCANNER_DBNAME} " >> /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "query = SELECT mailbox.transport FROM mailbox,domain WHERE mailbox.username='%s' AND mailbox.domain='%d' AND mailbox.domain=domain.domain AND mailbox.transport<>'' AND mailbox.active=1 AND mailbox.enabledeliver=1 AND domain.backupmx=0 AND domain.active=1" >> /etc/postfix/sql/mysql_transport_maps_user.cf
    echo "#expansion_limit = 100  " >> /etc/postfix/sql/mysql_transport_maps_user.cf
    
    
    ## mysql_virtual_alias_domain_catchall_maps.cf  
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    echo "query  = SELECT goto FROM alias,alias_domain WHERE alias_domain.alias_domain = '%d' and alias.address = CONCAT('@', alias_domain.target_domain) AND alias.active = 1 AND alias_domain.active='1'" >> /etc/postfix/sql/mysql_virtual_alias_domain_catchall_maps.cf
    
    ## mysql_virtual_alias_domain_mailbox_maps.cf:
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    echo "query = SELECT maildir FROM mailbox,alias_domain WHERE alias_domain.alias_domain = '%d' and mailbox.username = CONCAT('%u', '@', alias_domain.target_domain) AND mailbox.active = 1 AND alias_domain.active='1'" >> /etc/postfix/sql/mysql_virtual_alias_domain_mailbox_maps.cf
    
    ## mysql_virtual_alias_domain_maps.cf 
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "user = ${MAILSCANNER_DBUSER} " >> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "password = ${MAILSCANNER_DBPASS} " >> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "hosts = ${DBHOST} " >> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "port = ${MYSQL_PORT} " >> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME} " >> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "query = SELECT goto FROM alias,alias_domain WHERE alias_domain.alias_domain = '%d' and alias.address = CONCAT('%u', '@', alias_domain.target_domain) AND alias.active = 1 AND alias_domain.active='1' ">> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    echo "#expansion_limit = 100  " >> /etc/postfix/sql/mysql_virtual_alias_domain_maps.cf
    
    
    ## mysql_virtual_alias_maps.cf
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "user = ${MAILSCANNER_DBUSER} " >> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "password = ${MAILSCANNER_DBPASS} " >> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "hosts = ${DBHOST} " >> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "port = ${MYSQL_PORT} " >> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME} " >> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "query = SELECT goto FROM alias WHERE address='%s' AND active = '1' ">> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    echo "#expansion_limit = 100  " >> /etc/postfix/sql/mysql_virtual_alias_maps.cf
    
    ## mysql_virtual_domains_maps.cf
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "user = ${MAILSCANNER_DBUSER} " >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "password = ${MAILSCANNER_DBPASS} " >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "hosts = ${DBHOST} " >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "port = ${MYSQL_PORT} " >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME} " >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "query = SELECT domain FROM domain WHERE domain='%s' AND active = '1'" >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "#query = SELECT domain FROM domain WHERE domain='%s'" >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "#optional query to use when relaying for backup MX" >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "#query = SELECT domain FROM domain WHERE domain='%s' AND backupmx = '0' AND active = '1'" >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
    echo "#expansion_limit = 100  " >> /etc/postfix/sql/mysql_virtual_domains_maps.cf
      
    # For quota support
    
    #mysql_virtual_mailbox_limit_maps.cf: 
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf
    echo "query = SELECT quota FROM mailbox WHERE username='%s' AND active = '1'" >> /etc/postfix/sql/mysql_virtual_mailbox_limit_maps.cf

    #mysql_virtual_mailbox_maps.cf:
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "query = SELECT maildir FROM mailbox WHERE username='%s' AND active = '1'" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    echo "#expansion_limit = 100" >> /etc/postfix/sql/mysql_virtual_mailbox_maps.cf
    
    ### RBL STUFF
    #mysql_postfix_rbl_override.cf
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "table = sys_rbl_override" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "select_field = id" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    echo "where_field = ip_address" >> /etc/postfix/sql/mysql_postfix_rbl_override.cf
    
    
    ### smtpd_recipient_restrictions
    #mysql_virtual_recipient_access.cf
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_virtual_recipient_access.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_virtual_recipient_access.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_virtual_recipient_access.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_virtual_recipient_access.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_virtual_recipient_access.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_virtual_recipient_access.cf
    echo "query = SELECT access FROM virtual_sender_access WHERE source='%s'" >> /etc/postfix/sql/mysql_virtual_recipient_access.cf
    
    ### antispoofing
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_domain_antispoofing.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_domain_antispoofing.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_domain_antispoofing.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_domain_antispoofing.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_domain_antispoofing.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_domain_antispoofing.cf
    echo "query = SELECT concat('REJECT') FROM domain WHERE domain='%d' AND antispoofing='REJECT'" >> /etc/postfix/sql/mysql_domain_antispoofing.cf
    
    #### rbl_override
    echo "# Configuration written by EMFABox install script @ ${CreateDate}" > /etc/postfix/sql/mysql_rbl_override.cf
    echo "user = ${MAILSCANNER_DBUSER}" >> /etc/postfix/sql/mysql_rbl_override.cf
    echo "password = ${MAILSCANNER_DBPASS}" >> /etc/postfix/sql/mysql_rbl_override.cf
    echo "hosts = ${DBHOST}" >> /etc/postfix/sql/mysql_rbl_override.cf
    echo "port = ${MYSQL_PORT}" >> /etc/postfix/sql/mysql_rbl_override.cf
    echo "dbname = ${MAILSCANNER_DBNAME}" >> /etc/postfix/sql/mysql_rbl_override.cf
    echo "query = SELECT concat('PERMIT') FROM sys_rbl_override WHERE ip_address='%s'" >> /etc/postfix/sql/mysql_rbl_override.cf
    

	## check_helo_access.pcre
	
	#https://bitbucket.org/emfabox/beta/raw/1165d17b5e625a715871e85a3be95333a1a8e9b3/src/postfix/check_helo_access.pcre
	/usr/bin/wget --no-check-certificate -O /etc/postfix/check_helo_access.pcre https://bitbucket.org/emfabox/beta/raw/1165d17b5e625a715871e85a3be95333a1a8e9b3/src/postfix/check_helo_access.pcre
    
    ## SASL DB FIX
    
    # echo `uuidgen -r` | saslpasswd2 -p -c /etc/sasldb2
    echo `tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1` | saslpasswd2 -p -c /etc/sasldb2
   
    #chgrp postfix /etc/sasldb2
     #mtagroup
    chgrp mtagroup /etc/sasldb2
  
  # check postfix   
    postfix -c /etc/postfix check
    
    ### http://tipstricks.itmatrix.eu/?p=855
    ### http://www.claudiokuenzler.com/blog/275/howto-postfix-force-smtp-send-mail-with-tls-encryption-encrypted-starttls#.U9_602PG-5I
    
    # postfix fix
if [ -f "/etc/postfix/master.cf" ]; then
	sed -i "s/pickup    unix/pickup    fifo/g" /etc/postfix/master.cf
	sed -i "s/qmgr      unix/qmgr      fifo/g" /etc/postfix/master.cf
fi

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# POSTFIX-OUT TRANSPORT HACK
# +---------------------------------------------------+
emfa_postfix_out_db () {
#db passwords
source /etc/emfa/variables.conf

# add outgoing route to postfix transport      
QUERY99="INSERT INTO domain (uuid,domain,description,aliases,mailboxes,maxquota,quota,transport,backupmx,created,modified,expired,active) VALUES ('${EMFABOX_TRANSPORT_UUID}','*', 'out', 1, 0, 0, 0, 'smtp:[127.0.0.1]:2525', 0, '0000-0-0 00:00:00', '0000-0-0 00:00:00', '9999-12-31 00:00:00', 1);"
/usr/bin/mysql -e "$QUERY99" -uroot -p${MYSQL_ROOTPASS} -Dmailscanner

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# IXED
# +---------------------------------------------------+
emfa_ixed () {

mkdir -p /usr/src/EMFA/ixed/  

cd /usr/src/EMFA/ixed/
 
  /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/ixed/loaders.linux-x86_64.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/loaders.linux-x86_64.tar.gz 
  
tar -xvzf loaders.linux-x86_64.tar.gz
  
  if [ ! -d /opt/emfa/ixed ] ; then   
  mkdir -p /opt/emfa/ixed/ 
  yes | cp *.lin /opt/emfa/ixed/
  printf "extension=${MY_IXED_DIR}/ixed.5.3.lin" > /etc/php.d/ixed.ini
  fi
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# GEOIP
# +---------------------------------------------------+
emfa_geo_ip () {
pecl install geoip
echo ";extension=geoip.so" > /etc/php.d/geoip.ini
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# ClamAV Unofficial Signatures Updater
# https://github.com/extremeshok/clamav-unofficial-sigs
# +---------------------------------------------------+
emfa_sane_sec () {

mkdir -p /usr/src/EMFA/sanesecurity/
cd /usr/src/EMFA/sanesecurity/

#https://bitbucket.org/emfabox/beta/raw/c8fb376322ab7fc4785af9dff269b8194f4766cf/src/clamav-unofficial-sigs-b53b7ad.tar.gz
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/sanesecurity/clamav-unofficial-sigs.tar.gz https://bitbucket.org/emfabox/beta/raw/c8fb376322ab7fc4785af9dff269b8194f4766cf/src/clamav-unofficial-sigs-b53b7ad.tar.gz
    
      
tar -xvzf clamav-unofficial-sigs.tar.gz
cd clamav-unofficial-sigs*


mv -f clamav-unofficial-sigs.sh /usr/local/bin/clamav-unofficial-sigs.sh.orig
mv -f clamav-unofficial-sigs.conf /etc/
mv -f clamav-unofficial-sigs.8 /usr/share/man/man8/
mv -f clamav-unofficial-sigs-cron /etc/cron.d/
mv -f clamav-unofficial-sigs-logrotate /etc/logrotate.d/

if [ -f /usr/local/bin/clamav-unofficial-sigs.sh ] ; then 

rm -rf /usr/local/bin/clamav-unofficial-sigs.sh

fi

/usr/bin/wget --no-check-certificate -O /usr/local/bin/clamav-unofficial-sigs.sh  https://bitbucket.org/emfabox/beta/raw/49f13953f8ee76cbfc0a23c45480370fa92a2ff9/src/clamav-unofficial-sigs-latest/clamav-unofficial-sigs.sh

chmod 755 /usr/local/bin/clamav-unofficial-sigs.sh
    
sed -i "/45 \* \* \* \* root / c\45 * * * * root /usr/local/bin/clamav-unofficial-sigs.sh -c /etc/clamav-unofficial-sigs.conf >> /var/log/clamav-unofficial-sigs.log 2>&1" /etc/cron.d/clamav-unofficial-sigs-cron
       
    #log_file_path="/var/log/clamav-unofficial-sigs"
    sed -i '/log_file_path=/ c\log_file_path="/var/log/"' /etc/clamav-unofficial-sigs.conf
    sed -i '/#clamd_socket=/ c\clamd_socket="/var/run/clamav/clamd.sock"' /etc/clamav-unofficial-sigs.conf
    ##foxhole
    sed -i '/#foxhole_generic.cdb/ c\foxhole_generic.cdb #MED See Foxhole page for more details' /etc/clamav-unofficial-sigs.conf 
    sed -i '/#foxhole_filename.cdb/ c\foxhole_filename.cdb #MED See Foxhole page for more details' /etc/clamav-unofficial-sigs.conf
    ### SCAMNAILER
    sed -i '/#scamnailer.ndb/ c\scamnailer.ndb #MED Spear phishing and other phishing emails' /etc/clamav-unofficial-sigs.conf
    ### BADMACRO   2015-08-14
    #badmacro.ndb #MED Detect dangerous macros
    sed -i '/#badmacro.ndb/ c\badmacro.ndb #MED Detect dangerous macros' /etc/clamav-unofficial-sigs.conf
    sed -i '/user_configuration_complete="no"/ c\user_configuration_complete="yes"' /etc/clamav-unofficial-sigs.conf
    # fix socket file in mailscanner.conf
    # ls /var/run/clamav/
    # clamd.pid  clamd.sock
    sed -i '/^Clamd Socket/ c\Clamd Socket = \/var\/run\/clamav\/clamd.sock' /etc/MailScanner/MailScanner.conf
    ln -s /etc/clamav-unofficial-sigs.conf /usr/local/etc/clamav-unofficial-sigs.conf  
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# Memory Info
# +---------------------------------------------------+
emfa_check_mem () 
{
numMem="`cat /proc/meminfo | grep MemTotal | awk '{print $2}'`"
if [ $numMem -lt 409600 ] ; then

    echo ""
    echo "Additional ClamAV Rules have been installed."
    echo "If this is a VPS, please make sure you have atleast 4GB of memory"
    echo "or you will notice a huge decrease in performance."
    echo ""
fi 
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# SA Part I
# +---------------------------------------------------+
emfa_sa_part_1 () {
#db passwords
source /etc/emfa/variables.conf

cd /usr/src/EMFA/
 
# Download an initial KAM.cf file updates are handled by SA-Update.
#https://bitbucket.org/emfabox/beta/raw/625ffc511bb164fd5f5103452db8c625275245b4/src/KAM.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/KAM.cf https://bitbucket.org/emfabox/beta/raw/625ffc511bb164fd5f5103452db8c625275245b4/src/KAM.cf
    
# Configure spamassassin bayes and awl DB settings
echo "#Begin MySQL ">>/etc/MailScanner/spam.assassin.prefs.conf
#echo "bayes_store_module              Mail::SpamAssassin::BayesStore::SQL">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_store_module              Mail::SpamAssassin::BayesStore::MySQL">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_sql_dsn                   DBI:mysql:sa_bayes:localhost">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_sql_username              sa_user">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_sql_password              ${SA_BAYES_DBPASS}">>/etc/MailScanner/spam.assassin.prefs.conf
echo "auto_whitelist_factory          Mail::SpamAssassin::SQLBasedAddrList">>/etc/MailScanner/spam.assassin.prefs.conf
echo "user_awl_dsn                    DBI:mysql:sa_bayes:localhost">>/etc/MailScanner/spam.assassin.prefs.conf
echo "user_awl_sql_username           sa_user">>/etc/MailScanner/spam.assassin.prefs.conf
echo "user_awl_sql_password           ${SA_BAYES_DBPASS}">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_sql_override_username     mailwatch">>/etc/MailScanner/spam.assassin.prefs.conf
echo "#End MySQL">>/etc/MailScanner/spam.assassin.prefs.conf
    
sed -i '/bayes_ignore_header/d' /etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_ignore_header X-EMFABOX-EMFABox">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_ignore_header X-EMFABOX-EMFABox-SpamCheck">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_ignore_header X-EMFABOX-EMFABox-SpamScore">>/etc/MailScanner/spam.assassin.prefs.conf
echo "bayes_ignore_header X-EMFABOX-EMFABox-Information">>/etc/MailScanner/spam.assassin.prefs.conf
sed -i '/envelope_sender_header/d' /etc/MailScanner/spam.assassin.prefs.conf
echo "envelope_sender_header X-EMFABOX-EMFABox-From">>/etc/MailScanner/spam.assassin.prefs.conf

echo "#SaneSecurity Signatures">>/etc/MailScanner/spam.assassin.prefs.conf
echo "header EMFA_FOUND_SPAMVIRUS exists:X-EMFABOX-EMFABox-SpamVirus-Report">>/etc/MailScanner/spam.assassin.prefs.conf
echo "describe  EMFA_FOUND_SPAMVIRUS Message contains spam virus pattern">>/etc/MailScanner/spam.assassin.prefs.conf
echo "score  EMFA_FOUND_SPAMVIRUS 10.0">>/etc/MailScanner/spam.assassin.prefs.conf

/bin/sed -i '/^score ANY_BOUNCE_MESSAGE/d' /etc/MailScanner/spam.assassin.prefs.conf
echo "score ANY_BOUNCE_MESSAGE 3.5" >> /etc/MailScanner/spam.assassin.prefs.conf
    
#Set Timeouts
sed -i '/^rbl_timeout/d' /etc/MailScanner/spam.assassin.prefs.conf
sed -i "/^#rbl_timeout / c\rbl_timeout 3" /etc/MailScanner/spam.assassin.prefs.conf
    
# Add example spam to db
# source: http://spamassassin.apache.org/gtube/gtube.txt
cd /usr/src/EMFA/
#https://bitbucket.org/emfabox/beta/raw/d94f8a1db6137aca37abab566f44831ada73725a/src/sem.cf  
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/sem.cf  https://bitbucket.org/emfabox/beta/raw/d94f8a1db6137aca37abab566f44831ada73725a/src/sem.cf
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/gtube.txt https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/gtube.txt

/usr/local/bin/sa-learn --spam /usr/src/EMFA/gtube.txt
    
    # Enable Auto White Listing
    sed -i '/^#loadplugin Mail::SpamAssassin::Plugin::AWL/ c\loadplugin Mail::SpamAssassin::Plugin::AWL' /etc/mail/spamassassin/v310.pre  
    # loadplugin Mail::SpamAssassin::Plugin::TxRep
    #sed -i '/^# loadplugin Mail::SpamAssassin::Plugin::TxRep/ c\loadplugin Mail::SpamAssassin::Plugin::TxRep' /etc/mail/spamassassin/v341.pre
    #loadplugin Mail::SpamAssassin::Plugin::URILocalBL
    sed -i '/^# loadplugin Mail::SpamAssassin::Plugin::URILocalBL/ c\loadplugin Mail::SpamAssassin::Plugin::URILocalBL' /etc/mail/spamassassin/v341.pre
    # loadplugin Mail::SpamAssassin::Plugin::PDFInfo
    sed -i '/^# loadplugin Mail::SpamAssassin::Plugin::PDFInfo/ c\loadplugin Mail::SpamAssassin::Plugin::PDFInfo' /etc/mail/spamassassin/v341.pre
    # loadplugin Mail::SpamAssassin::Plugin::RelayCountry
    sed -i '/^# loadplugin Mail::SpamAssassin::Plugin::RelayCountry/ c\loadplugin Mail::SpamAssassin::Plugin::RelayCountry' /etc/mail/spamassassin/init.pre
    
    # loadplugin Mail::SpamAssassin::Plugin::DKIM
    echo "# loadplugin Mail::SpamAssassin::Plugin::DKIM" >> /etc/mail/spamassassin/init.pre
    echo "#" >> /etc/mail/spamassassin/init.pre
    echo "loadplugin Mail::SpamAssassin::Plugin::DKIM" >> /etc/mail/spamassassin/init.pre
    
    
    # cisco sender base check
    #https://bitbucket.org/emfabox/beta/raw/58d4cf0d06648be1379db2fca6356a9b4008c3f5/src/sa/iprep.cf
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/iprep.cf  https://bitbucket.org/emfabox/beta/raw/58d4cf0d06648be1379db2fca6356a9b4008c3f5/src/sa/iprep.cf
    
    #https://bitbucket.org/emfabox/beta/raw/58d4cf0d06648be1379db2fca6356a9b4008c3f5/src/sa/iprep.pm
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/iprep.pm  https://bitbucket.org/emfabox/beta/raw/58d4cf0d06648be1379db2fca6356a9b4008c3f5/src/sa/iprep.pm
       
    ##2015-06-28
    #https://bitbucket.org/emfabox/beta/raw/25d34ff3ac8d67782be17403787d3aa047d97338/src/sa/crm114.cf
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/crm114.cf.disabled https://bitbucket.org/emfabox/beta/raw/bd031a235a2e43f9d131550f15504bff91daa307/src/sa/crm114.cf
    
    #https://bitbucket.org/emfabox/beta/raw/25d34ff3ac8d67782be17403787d3aa047d97338/src/sa/crm114.pm
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/crm114.pm https://bitbucket.org/emfabox/beta/raw/25d34ff3ac8d67782be17403787d3aa047d97338/src/sa/crm114.pm
    
    #https://bitbucket.org/emfabox/beta/src/bd031a235a2e43f9d131550f15504bff91daa307/src/sa/crm114.tar.gz?at=master
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/crm114.tar.gz  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/sa/crm114.tar.gz
     
    cd  /etc/mail/spamassassin/
    tar xzvf crm114.tar.gz
     
    rm -f /etc/mail/spamassassin/crm114.tar.gz
    
    chown -R postfix:mtagroup /etc/mail/spamassassin/crm114
    
    chmod 755  /etc/mail/spamassassin/crm114/*.crm
    
    cd  /etc/mail/spamassassin/crm114/
    cssutil -b -r spam.css
    cssutil -b -r nonspam.css
   
    #country_filter.cf 2015-08-11
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/country_filter.cf https://bitbucket.org/emfabox/beta/raw/d4a2103537101077cbee6072d12a73ed055c14ad/src/sa/country_filter.cf
    
    #CountryFilter.pm 2015-08-11
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/CountryFilter.pm  https://bitbucket.org/emfabox/beta/raw/2a3e9822f2d722dce8ad427cff881200144a649f/src/sa/CountryFilter.pm
    
    #2015-06-30
    #https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/sagrey.cf
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/sagrey.cf  https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/sagrey.cf
    
    #https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/sagrey.pm
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/sagrey.pm  https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/sagrey.pm
    
    #https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/DecodeShortURLs.cf
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/DecodeShortURLs.cf https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/DecodeShortURLs.cf
    
    #https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/DecodeShortURLs.pm
    /usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/DecodeShortURLs.pm  https://bitbucket.org/emfabox/beta/raw/e05abf0b0468aecec1767b62c22ecd1b92a05bc1/src/sa/DecodeShortURLs.pm
    
    # AWL cleanup tools (just a bit different then esva)
    # http://notes.sagredo.eu/node/86 
    echo '#!/bin/sh'>/usr/sbin/trim-awl
    echo "/usr/bin/mysql -usa_user -p${SA_BAYES_DBPASS} < /etc/trim-awl.sql">>/usr/sbin/trim-awl
    echo 'exit 0 '>>/usr/sbin/trim-awl
    chmod +x /usr/sbin/trim-awl

    echo "USE sa_bayes;">/etc/trim-awl.sql
    echo 'DELETE FROM awl WHERE ts < (NOW() - INTERVAL 28 DAY);'>>/etc/trim-awl.sql
    
    cd /etc/cron.weekly
    echo '#!/bin/sh'>trim-sql-awl-weekly
    echo '#'>>trim-sql-awl-weekly
    echo '#  Weekly maintenance of auto-whitelist for'>>trim-sql-awl-weekly
    echo '#  SpamAssassin using MySQL'>>trim-sql-awl-weekly
    echo '/usr/sbin/trim-awl'>>trim-sql-awl-weekly
    echo 'exit 0'>>trim-sql-awl-weekly
    chmod +x trim-sql-awl-weekly

    # Create .spamassassin directory
    mkdir /var/www/.spamassassin
    chown postfix:postfix /var/www/.spamassassin
    
    mkdir -p /usr/src/EMFA/Sought
   
    # Add Sought Channel to replace Sare and initialize sa-update 
    /usr/local/bin/sa-update
    #https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/Sought/GPG.KEY
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/Sought/GPG.KEY https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/Sought/GPG.KEY 
    /usr/local/bin/sa-update --import /usr/src/EMFA/Sought/GPG.KEY
    
    # Add ZMI German Channel
    mkdir -p /usr/src/EMFA/zmi
    
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/zmi/GPG.KEY  https://bitbucket.org/emfabox/beta/raw/2918f6ea3e882722ed08d65f517aae6db9871d44/src/sa-zmi-at/GPG.KEY
    /usr/local/bin/sa-update --import /usr/src/EMFA/zmi/GPG.KEY
    
    #  sa-update --gpgkey 40F74481 --channel sa.zmi.at
    
    # Customize sa-update in /etc/sysconfig/update_spamassassin
    sed -i '/^SAUPDATE=/ c\SAUPDATE=/usr/local/bin/sa-update' /etc/sysconfig/update_spamassassin
    sed -i '/^SACOMPILE=/ c\SACOMPILE=/usr/local/bin/sa-compile' /etc/sysconfig/update_spamassassin
    # sed -i '/^SAUPDATEARGS=/ c\SAUPDATEARGS=" --gpgkey 6C6191E3 --channel sought.rules.yerp.org --channel updates.spamassassin.org"' /etc/sysconfig/update_spamassassin
    sed -i '/^SAUPDATEARGS=/ c\SAUPDATEARGS=" --gpgkey 6C6191E3 --channel sought.rules.yerp.org --gpgkey 40F74481 --channel sa.zmi.at --channel updates.spamassassin.org"' /etc/sysconfig/update_spamassassin
   
    sed -i "/^# loadplugin Mail::SpamAssassin::Plugin::Rule2XSBody/ c\loadplugin Mail::SpamAssassin::Plugin::Rule2XSBody" /etc/mail/spamassassin/v320.pre
     
    # run the command for logging
    /usr/local/bin/sa-update --gpgkey 6C6191E3 --channel sought.rules.yerp.org --gpgkey 40F74481 --channel sa.zmi.at --channel updates.spamassassin.org 
    
    #11-26-2014 
    #http://www.heinlein-support.de/blog/news/aktuelle-spamassassin-regeln-von-heinlein-support/
    /usr/local/bin/sa-update --nogpg --channel spamassassin.heinlein-support.de
    
    mkdir -p /usr/src/EMFA/RegistrarBoundaries/
    cd /usr/src/EMFA/RegistrarBoundaries/
    wget $mirror/$mirrorpath/RegistrarBoundaries.pm
    rm -f /usr/local/share/perl5/Mail/SpamAssassin/Util/RegistrarBoundaries.pm
    mv RegistrarBoundaries.pm /usr/local/share/perl5/Mail/SpamAssassin/Util/RegistrarBoundaries.pm
 
# 2015-07-04
# CRM114 TRE ADDON   
mkdir -p /usr/src/EMFA/tre/
      
cd /usr/src/EMFA/tre/
#https://bitbucket.org/emfabox/beta/src/6924de0a9ddae110e9c2dca28fe9a664c6b4f206/src/sa/tre-0.7.5.tar.bz2?at=master
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/tre/tre-0.7.5.tar.bz2  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/sa/tre-0.7.5.tar.bz2
      
tar xvf tre-0.7.5.tar.bz2
cd  tre-*
      
./configure -enable-static
make
make install


timewait 2

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# iXhash2
# +---------------------------------------------------+
emfa_iXhash2 () {
#https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/iXhash2/iXhash2.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/iXhash2.cf https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/iXhash2/iXhash2.cf

#https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/iXhash2/iXhash2.pm
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/iXhash2.pm https://bitbucket.org/emfabox/beta/raw/aa3ba964e9fd4095afa6c8b28fd8393ddae6e219/src/iXhash2/iXhash2.pm

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# spamassassin-extremeshok_fromreplyto
# https://github.com/extremeshok/spamassassin-extremeshok_fromreplyto
# +---------------------------------------------------+
emfa_fromreplyto () {
cd /etc/mail/spamassassin/

#https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/01_extremeshok_fromreplyto.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/01_fromreplyto.cf https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/01_extremeshok_fromreplyto.cf

if [ ! -d /etc/mail/spamassassin/plugins ] ; then 
mkdir /etc/mail/spamassassin/plugins
fi

cd /etc/mail/spamassassin/plugins

#https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromAndReplyToIsNotSameDomain.pm
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/plugins/FromAndReplyToIsNotSameDomain.pm  https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromAndReplyToIsNotSameDomain.pm

#https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromIsNotReplyTo.pm
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/plugins/FromIsNotReplyTo.pm https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromIsNotReplyTo.pm

#https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromIsNotReplyToWhitelist.pm
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/plugins/FromIsNotReplyToWhitelist.pm https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromIsNotReplyToWhitelist.pm

#https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromIsTo.pm
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/plugins/FromIsTo.pm  https://bitbucket.org/emfabox/beta/raw/76c15994510394cb4a5c1bddabbb997bfc052b80/src/sa_fromreplyto/plugins/FromIsTo.pm

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# DecodeShortURLs
# +---------------------------------------------------+
emfa_decodeshorturls () {
      mkdir -p /usr/src/EMFA/decodeshorturls
      
      cd /usr/src/EMFA/decodeshorturls
      
      #https://bitbucket.org/emfabox/beta/raw/bfe7318fbf39b7ca573ac46f7eb65f41d51ca480/src/DecodeShortURLs/DecodeShortURLs.cf
      /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/decodeshorturls/DecodeShortURLs.cf https://bitbucket.org/emfabox/beta/raw/bfe7318fbf39b7ca573ac46f7eb65f41d51ca480/src/DecodeShortURLs/DecodeShortURLs.cf
      
      #https://bitbucket.org/emfabox/beta/raw/bfe7318fbf39b7ca573ac46f7eb65f41d51ca480/src/DecodeShortURLs/DecodeShortURLs.pm
      /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/decodeshorturls/DecodeShortURLs.pm https://bitbucket.org/emfabox/beta/raw/bfe7318fbf39b7ca573ac46f7eb65f41d51ca480/src/DecodeShortURLs/DecodeShortURLs.pm
      
      # mark it out if not needed
      yes | cp /usr/src/EMFA/decodeshorturls/DecodeShortURLs.cf /etc/mail/spamassassin/DecodeShortURLs.cf
      
      yes | cp /usr/src/EMFA/decodeshorturls/DecodeShortURLs.pm /etc/mail/spamassassin/DecodeShortURLs.pm
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# SA PART II
# +---------------------------------------------------+
emfa_sa_part_2 () {
#10_traps.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/10_traps.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/10_traps.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/10_traps.cf

#20_mspike.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/20_mspike.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/20_mspike.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/20_mspike.cf

#mime_validate.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/mime_validate.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/mime_validate.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/mime_validate.cf

#msonline.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/msonline.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/msonline.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/msonline.cf

#tls_auth.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/tls_auth.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/tls_auth.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/tls_auth.cf

#urltemplate.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/urltemplate.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/urltemplate.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/urltemplate.cf

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Install Pyzor
# http://downloads.sourceforge.net/project/pyzor/pyzor/0.5.0/pyzor-0.5.0.tar.gz
# +---------------------------------------------------+
emfa_pyzor () {

    cd ${SOURCE_DIR}
    wget $mirror/$mirrorpath/pyzor-$PYZORVERSION.tar.gz
    tar xvzf pyzor-$PYZORVERSION.tar.gz
    cd pyzor-$PYZORVERSION
    python setup.py build
    python setup.py install

    # Fix deprecation warning message
    sed -i '/^#!\/usr\/bin\/python/ c\#!\/usr\/bin\/python -Wignore::DeprecationWarning' /usr/bin/pyzor

    mkdir /var/spool/postfix/.pyzor
    ln -s /var/spool/postfix/.pyzor /var/www/.pyzor
    chown -R postfix:apache /var/spool/postfix/.pyzor
    chmod -R ug+rwx /var/spool/postfix/.pyzor

    # and finally initialize the servers file with an discover.
    su postfix -s /bin/bash -c 'pyzor discover'

    # Add version to EMFA-Config
    echo "PYZORVERSION:$PYZORVERSION" >> /etc/EMFA-Config
    
    if [ -f /var/www/.pyzor/servers ] ; then
    # add pyzor server
    echo "46.101.158.108:24441" > /var/www/.pyzor/servers
    echo "82.94.255.100:24441" >> /var/www/.pyzor/servers
    echo "66.250.40.33:24441" >> /var/www/.pyzor/servers
    fi    
}
# +---------------------------------------------------+



# +---------------------------------------------------+
# Install Razor (http://razor.sourceforge.net/)
# +---------------------------------------------------+
emfa_razor () {
    cd ${SOURCE_DIR}
    wget $mirror/$mirrorpath/razor-agents-2.84.tar.bz2
    tar xvjf razor-agents-2.84.tar.bz2
    cd razor-agents-2.84

    perl Makefile.PL
    make
    make test
    make install

    mkdir /var/spool/postfix/.razor
    ln -s /var/spool/postfix/.razor /var/www/.razor
    chown postfix:apache /var/spool/postfix/.razor
    chmod -R ug+rwx /var/spool/postfix/.razor

    # Issue #157 Razor failing after registration of service
    # Use setgid bit
    chmod ug+s /var/spool/postfix/.razor
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Install DCC http://www.rhyolite.com/dcc/
# (current version = version 1.3.154, December 03, 2013)
# +---------------------------------------------------+
emfa_dcc () {
    cd ${SOURCE_DIR}

    wget $mirror/$mirrorpath/dcc-1.3.154.tar.Z
    tar xvzf dcc-1.3.154.tar.Z
    cd dcc-*

    ./configure --disable-dccm
    make install

    ln -s /var/dcc/libexec/cron-dccd /usr/bin/cron-dccd
    ln -s /var/dcc/libexec/cron-dccd /etc/cron.monthly/cron-dccd
    echo "dcc_home /var/dcc" >> /etc/MailScanner/spam.assassin.prefs.conf
    sed -i '/^dcc_path / c\dcc_path /usr/local/bin/dccproc' /etc/MailScanner/spam.assassin.prefs.conf
    sed -i '/^DCCIFD_ENABLE=/ c\DCCIFD_ENABLE=on' /var/dcc/dcc_conf
    sed -i '/^DBCLEAN_LOGDAYS=/ c\DBCLEAN_LOGDAYS=1' /var/dcc/dcc_conf
    sed -i '/^DCCIFD_LOGDIR=/ c\DCCIFD_LOGDIR="/var/dcc/log"' /var/dcc/dcc_conf
    chown postfix:postfix /var/dcc

    cp /var/dcc/libexec/rcDCC /etc/init.d/adcc
    sed -i "s/#loadplugin Mail::SpamAssassin::Plugin::DCC/loadplugin Mail::SpamAssassin::Plugin::DCC/g" /etc/mail/spamassassin/v310.pre
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# imageCerberus to replace fuzzyocr
# http://sourceforge.net/projects/imagecerberus/
# +---------------------------------------------------+
emfa_imagecerberus () {
    cd ${SOURCE_DIR}
    wget $mirror/$mirrorpath/imageCerberus-v$IMAGECEBERUSVERSION.zip
    unzip imageCerberus-v$IMAGECEBERUSVERSION.zip
    cd imageCerberus-v$IMAGECEBERUSVERSION
    mkdir /etc/spamassassin
    mv spamassassin/imageCerberus /etc/spamassassin/
    rm -f /etc/spamassassin/imageCerberus/imageCerberusEXE
    mv /etc/spamassassin/imageCerberus/x86_64/imageCerberusEXE /etc/spamassassin/imageCerberus/
    rm -rf /etc/spamassassin/imageCerberus/x86_64
    rm -rf /etc/spamassassin/imageCerberus/i386

    mv spamassassin/ImageCerberusPLG.pm /usr/local/share/perl5/Mail/SpamAssassin/Plugin/
    mv spamassassin/ImageCerberusPLG.cf /etc/mail/spamassassin/

    sed -i '/^loadplugin ImageCerberusPLG / c\loadplugin ImageCerberusPLG /usr/local/share/perl5/Mail/SpamAssassin/Plugin/ImageCerberusPLG.pm' /etc/mail/spamassassin/ImageCerberusPLG.cf

    # fix a few library locations
    ln -s /usr/lib64/libcv.so.2.0 /usr/lib64/libcv.so.1
    ln -s /usr/lib64/libhighgui.so.2.0 /usr/lib64/libhighgui.so.1
    ln -s /usr/lib64/libcxcore.so.2.0 /usr/lib64/libcxcore.so.1
    ln -s /usr/lib64/libcvaux.so.2.0 /usr/lib64/libcvaux.so.1

    # Issue 67 default ImageCeberus score
    sed -i "/^score     ImageCerberusPLG0/ c\score     ImageCerberusPLG0     0.0  0.0  0.0  0.0" /etc/mail/spamassassin/ImageCerberusPLG.cf

    # Add the version to EFA-Config
    echo "IMAGECEBERUSVERSION:$IMAGECEBERUSVERSION" >> /etc/EFA-Config
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# AVIRA SAVAPI
# +---------------------------------------------------+
emfa_avira () {
	if [ ! -d  /var/opt/savapi/ ] ; then
	mkdir -p /var/opt/savapi/
	fi
	
	cd /var/opt/savapi
  #https://bitbucket.org/emfabox/beta/raw/7e588a5ecb6bce83b62adf62b24cda36bf72cd8e/src/avira/svapi
  /usr/bin/wget --no-check-certificate -O /etc/init.d/svapi https://bitbucket.org/emfabox/beta/raw/7e588a5ecb6bce83b62adf62b24cda36bf72cd8e/src/avira/svapi
  
  chmod 755 /etc/init.d/svapi
  if [ ! -d  /var/opt/savapi/bin ] ; then
	mkdir -p /var/opt/savapi/bin
	fi

  #https://bitbucket.org/emfabox/beta/raw/9acc9d5c4a946baa659049b88daed321fab29bac/src/avira/AviraInterface.pl
  /usr/bin/wget --no-check-certificate -O /var/opt/savapi/bin/AviraInterface.pl https://bitbucket.org/emfabox/beta/raw/9acc9d5c4a946baa659049b88daed321fab29bac/src/avira/AviraInterface.pl
  chmod 755 /var/opt/savapi/bin/AviraInterface.pl
  
  cd  /usr/share/MailScanner/MailScanner/
  
 yes | cp /usr/share/MailScanner/MailScanner/SweepViruses.pm   /usr/share/MailScanner/MailScanner/SweepViruses.pm.orig
 
 rm -rf /usr/share/MailScanner/MailScanner/SweepViruses.pm
 
#https://bitbucket.org/emfabox/beta/raw/e5c861ca3cbccc797a672684f9e8dc928fbe6e3f/src/avira/SweepViruses.pm
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/MailScanner/SweepViruses.pm https://bitbucket.org/emfabox/beta/raw/e5c861ca3cbccc797a672684f9e8dc928fbe6e3f/src/avira/SweepViruses.pm
  
cd /usr/share/MailScanner

#https://bitbucket.org/emfabox/beta/raw/9acc9d5c4a946baa659049b88daed321fab29bac/src/avira/savapi-autoupdate
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/savapi-autoupdate  https://bitbucket.org/emfabox/beta/raw/9acc9d5c4a946baa659049b88daed321fab29bac/src/avira/savapi-autoupdate
chmod 755 /usr/share/MailScanner/savapi-autoupdate

#https://bitbucket.org/emfabox/beta/raw/9acc9d5c4a946baa659049b88daed321fab29bac/src/avira/savapi-wrapper
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/savapi-wrapper https://bitbucket.org/emfabox/beta/raw/9acc9d5c4a946baa659049b88daed321fab29bac/src/avira/savapi-wrapper
chmod 755 /usr/share/MailScanner/savapi-wrapper

# 
cd /etc/MailScanner/

yes | cp /etc/MailScanner/virus.scanners.conf  /etc/MailScanner/virus.scanners.conf.orig
rm -rf /etc/MailScanner/virus.scanners.conf

#https://bitbucket.org/emfabox/beta/raw/e5c861ca3cbccc797a672684f9e8dc928fbe6e3f/src/avira/virus.scanners.conf
/usr/bin/wget --no-check-certificate -O /etc/MailScanner/virus.scanners.conf https://bitbucket.org/emfabox/beta/raw/e5c861ca3cbccc797a672684f9e8dc928fbe6e3f/src/avira/virus.scanners.conf

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# APACHE
# +---------------------------------------------------+
emfa_apache () {
  
    rm -f /etc/httpd/conf.d/welcome.conf
    cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.original
    
    # Remove unwanted modules
    sed -i '/LoadModule ldap_module modules\/mod_ldap.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule authnz_ldap_module modules\/mod_authnz_ldap.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule dav_module modules\/mod_dav.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule autoindex_module modules\/mod_autoindex.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule info_module modules\/mod_info.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule dav_fs_module modules\/mod_dav_fs.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule userdir_module modules\/mod_userdir.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule proxy_module modules\/mod_proxy.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule proxy_balancer_module modules\/mod_proxy_balancer.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule proxy_ftp_module modules\/mod_proxy_ftp.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule proxy_http_module modules\/mod_proxy_http.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule proxy_ajp_module modules\/mod_proxy_ajp.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule proxy_connect_module modules\/mod_proxy_connect.so/d' /etc/httpd/conf/httpd.conf
    sed -i '/LoadModule version_module modules\/mod_version.so/d' /etc/httpd/conf/httpd.conf

    # Remove config for disabled modules
    sed -i '/IndexOptions /d' /etc/httpd/conf/httpd.conf
    sed -i '/AddIconByEncoding /d' /etc/httpd/conf/httpd.conf
    sed -i '/AddIconByType /d' /etc/httpd/conf/httpd.conf
    sed -i '/AddIcon /d' /etc/httpd/conf/httpd.conf
    sed -i '/DefaultIcon /d' /etc/httpd/conf/httpd.conf
    sed -i '/ReadmeName /d' /etc/httpd/conf/httpd.conf
    sed -i '/HeaderName /d' /etc/httpd/conf/httpd.conf
    sed -i '/IndexIgnore /d' /etc/httpd/conf/httpd.conf
    
    # Set ServerSignature from on to off
    sed -i '/^ServerSignature/ c\ServerSignature Off' /etc/httpd/conf/httpd.conf
    
    # ServerTokens from OS to ProductOnly
    sed -i '/^ServerTokens/ c\ServerTokens ProductOnly' /etc/httpd/conf/httpd.conf

    # Secure PHP (this might break some stuff so need to test carefully)
    # sed -i '/disable_functions =/ c\disable_functions = apache_child_terminate,apache_setenv,define_syslog_variables,escapeshellcmd,eval,fp,fput,ftp_connect,ftp_exec,ftp_get,ftp_login,ftp_nb_fput,ftp_put,ftp_raw,ftp_rawlist,highlight_file,ini_alter,ini_get_all,ini_restore,inject_code,openlog,phpAds_remoteInfo,phpAds_XmlRpc,phpAds_xmlrpcDecode,phpAds_xmlrpcEncode,posix_getpwuid,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid,posix_setuid,posix_uname,proc_close,proc_get_status,proc_nice,proc_open,proc_terminate,syslog,system,xmlrpc_entity_decode,curl_multi_exec' /etc/php.ini

    # Poodle
    # Disable SSLv2 & SSLv3    
    sed -i '/^SSLProtocol/ c\SSLProtocol all -SSLv2 -SSLv3' /etc/httpd/conf.d/ssl.conf 
    
    sed -i '/^#Listen 443/ c\Listen 443' /etc/httpd/conf.d/ssl.conf
    
    
    echo -e "RewriteEngine On" > /etc/httpd/conf.d/redirectssl.conf
    echo -e "RewriteCond %{HTTPS} !=on" >> /etc/httpd/conf.d/redirectssl.conf
    echo -e "RewriteRule ^/?(.*) https://%{SERVER_NAME}/\$1 [R,L]" >> /etc/httpd/conf.d/redirectssl.conf
    
    
printf "<IfModule mod_headers.c>\n" > /etc/httpd/conf.d/headers.conf
printf "    Header unset ETag\n" >> /etc/httpd/conf.d/headers.conf
printf "    Header set X-Frame-Options: SAMEORIGIN\n" >> /etc/httpd/conf.d/headers.conf
printf '    Header set X-XSS-Protection: "1; mode=block"\n' >> /etc/httpd/conf.d/headers.conf
printf "    Header set X-Content-Type-Options: nosniff\n" >> /etc/httpd/conf.d/headers.conf
printf "    Header set X-WebKit-CSP: \"default-src 'self'\"\n" >> /etc/httpd/conf.d/headers.conf
printf '    Header set X-Permitted-Cross-Domain-Policies: "master-only"\n' >> /etc/httpd/conf.d/headers.conf
printf "</IfModule>\n" >> /etc/httpd/conf.d/headers.conf

#ErrorDocument global ...
printf "ErrorDocument 401 /401.php\n"  > /etc/httpd/conf.d/error.conf
printf "ErrorDocument 404 /404.php\n"  >> /etc/httpd/conf.d/error.conf


# Create robots.txt.
cat >> ${WWW_DIR}/robots.txt <<EOF
User-agent: *
Disallow: /
EOF
   
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Mailwatch
# +---------------------------------------------------+
emfa_mailwatch () {
#db passwords
source /etc/emfa/variables.conf

#householding
if [ -f /usr/lib/MailScanner/MailScanner/CustomFunctions/MailWatch.pm ]; then       
  rm -f /usr/lib/MailScanner/MailScanner/CustomFunctions/MailWatch.pm 
fi
    
# new path
if [ -f /usr/share/MailScanner/MailScanner/CustomFunctions/MailWatch.pm ]; then
  rm -f /usr/share/MailScanner/MailScanner/CustomFunctions/MailWatch.pm 
fi

if [ -f /usr/lib/MailScanner/MailScanner/CustomFunctions/SQLBlackWhiteList.pm ]; then
  rm -f /usr/lib/MailScanner/MailScanner/CustomFunctions/SQLBlackWhiteList.pm
fi

if [ -f /usr/share/MailScanner/MailScanner/CustomFunctions/SQLBlackWhiteList.pm ]; then      
  rm -f /usr/share/MailScanner/MailScanner/CustomFunctions/SQLBlackWhiteList.pm
fi

if [ -f /usr/lib/MailScanner/MailScanner/CustomFunctions/SQLSpamSettings.pm ]; then
  rm -f /usr/lib/MailScanner/MailScanner/CustomFunctions/SQLSpamSettings.pm
fi
    
if [ -f /usr/share/MailScanner/MailScanner/CustomFunctions/SQLSpamSettings.pm ]; then
  rm -f /usr/share/MailScanner/MailScanner/CustomFunctions/SQLSpamSettings.pm
fi

if [ -f /var/www/html/emfa/conf.php ]; then 
  mv /var/www/html/emfa/conf.php /var/www/html/emfa/conf.php.install
fi
    
if [ ! -d  /var/www/html/emfa ]; then
  mkdir  /var/www/html/emfa 
fi

mkdir -p /usr/src/EMFA/mailwatch
cd /usr/src/EMFA/mailwatch

# Set php parameters needed
sed -i '/^short_open_tag =/ c\short_open_tag = On' /etc/php.ini

#https://bitbucket.org/emfabox/beta/raw/8c523201232a21bd70f988cec3f5a1482606c19e/src/MailScanner_perl_scripts/MailWatch.pm
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/MailScanner/CustomFunctions/MailWatch.pm https://bitbucket.org/emfabox/beta/raw/8c523201232a21bd70f988cec3f5a1482606c19e/src/MailScanner_perl_scripts/MailWatch.pm

#https://bitbucket.org/emfabox/beta/raw/7426b8e242ee5df8a9538e9f8d01d892e156c20a/src/MailScanner_perl_scripts/SQLBlackWhiteList.pm
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/MailScanner/CustomFunctions/SQLBlackWhiteList.pm https://bitbucket.org/emfabox/beta/raw/8c523201232a21bd70f988cec3f5a1482606c19e/src/MailScanner_perl_scripts/SQLBlackWhiteList.pm
    
 #https://bitbucket.org/emfabox/beta/raw/8c523201232a21bd70f988cec3f5a1482606c19e/src/MailScanner_perl_scripts/SQLSpamSettings.pm
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/MailScanner/CustomFunctions/SQLSpamSettings.pm  https://bitbucket.org/emfabox/beta/raw/8c523201232a21bd70f988cec3f5a1482606c19e/src/MailScanner_perl_scripts/SQLSpamSettings.pm

 if [ ! -d /usr/lib/MailScanner/MailScanner/CustomFunctions/ ]; then
  mkdir -p /usr/lib/MailScanner/MailScanner/CustomFunctions/
fi
    
if [  -d /usr/lib/MailScanner/MailScanner/CustomFunctions/ ]; then
  cd  /usr/lib/MailScanner/MailScanner/CustomFunctions/   
  ln -s /usr/share/MailScanner/MailScanner/CustomFunctions/* .
fi    

# Postfix Relay Info
touch /etc/cron.hourly/mailwatch_update_relay
echo '#!/bin/sh' > /etc/cron.hourly/mailwatch_update_relay
echo "#" >> /etc/cron.hourly/mailwatch_update_relay
echo "/var/www/html/master/sbin/mailwatch_relay.sh  &> /dev/null" >> /etc/cron.hourly/mailwatch_update_relay
chmod +x /etc/cron.hourly/mailwatch_update_relay 


# Place the learn and release scripts
cd /var/www/cgi-bin

chmod 755 *.cgi


chown -R postfix:apache /var/spool/postfix/hold
chown -R postfix:apache /var/spool/postfix/incoming
chmod -R 750 /var/spool/postfix/hold
chmod -R 750 /var/spool/postfix/incoming

if [ ! -d /var/www/temp ] ; then
  mkdir -p /var/www/temp
fi 

chmod 0777 /var/www/temp

if [ -f /var/www/html/lib ] ; then
  mkdir -p /var/www/html/lib
fi 

#
chmod 0640 /etc/postfix/main.cf
chown root:mtagroup /etc/my.cnf
chmod 0640 /etc/my.cnf
chown root:mtagroup /etc/sysconfig/iptables
chmod 0640 /etc/sysconfig/iptables
chown root:mtagroup /etc/httpd/conf/httpd.conf
chmod 0640 /etc/httpd/conf/httpd.conf
chown -R root:mtagroup /etc/MailScanner/reports/
chown postfix:mtagroup /var/spool/MailScanner
chown postfix:mtagroup /var/spool/MailScanner/incoming
chown postfix:mtagroup /var/spool/MailScanner/quarantine
chown postfix.mtagroup /var/spool/MailScanner/spamassassin
chown postfix:mtagroup /var/spool/postfix
chown postfix:mtagroup /var/spool/postfix/incoming
chown -R root:mtagroup /var/www/html
find /var/www/html -type d -exec chmod 0655 {} \;
find /var/www/html -type f -exec chmod 0644 {} \;

find /etc/MailScanner/reports/ -type d -exec chmod 0655 {} \;
find /etc/MailScanner/reports/ -type f -exec chmod 0664 {} \;
chmod g+w /var/spool/MailScanner/*
touch /var/spool/MailScanner/incoming/SpamAssassin.cache.db
chown postfix:mtagroup /var/spool/MailScanner/incoming/SpamAssassin.cache.db
touch /var/spool/MailScanner/incoming/Processing.db
chown postfix:mtagroup /var/spool/MailScanner/incoming/Processing.db

mkdir -p /var/spool/postfix/hold
chown postfix:mtagroup /var/spool/postfix/hold
chmod 0775 /var/spool/postfix/hold

find /var/www/cgi-bin -type f -exec chmod +rx {} \;
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# mailgraph 
# +---------------------------------------------------+
emfa_mailgraph () {
mkdir -p /usr/src/EMFA/mailgraph
cd /usr/src/EMFA/mailgraph
    
#https://bitbucket.org/emfabox/beta/src/1809515421d9a11531b01f63ac0137bf527a2728/src/mailgraph-1.14.tar.gz?at=master
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/mailgraph/mailgraph-1.14.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/mailgraph-1.14.tar.gz
tar xvzf mailgraph-1.14.tar.gz
cd mailgraph-1.14

#mv mailgraph.cgi /var/www/cgi-bin/
mv mailgraph.pl /usr/local/bin/
mv mailgraph-init /etc/init.d/
mv mailgraph.css /var/www/html/
chmod 0755 /etc/init.d/mailgraph-init

#https://bitbucket.org/emfabox/beta/raw/ee628cdafbc9c8b2a4a37d04e33792b85f65511f/src/mailgraph/mailgraph.cgi
    /usr/bin/wget --no-check-certificate -O /var/www/cgi-bin/mailgraph.cgi https://bitbucket.org/emfabox/beta/raw/ee628cdafbc9c8b2a4a37d04e33792b85f65511f/src/mailgraph/mailgraph.cgi
    
    chmod 0755 /var/www/cgi-bin/mailgraph.cgi
    
    chown root:apache  /var/www/html/*.css
    chmod 644 /var/www/html/*.css

    #change css path
    sed -i '/^<link rel="stylesheet" href="mailgraph.css"/ c\<link rel="stylesheet" href="../mailgraph.css" type="text/css" />' /var/www/cgi-bin/mailgraph.cgi
    
    sed -i '/^MAIL_LOG=/ c\MAIL_LOG=\/var\/log\/maillog' /etc/init.d/mailgraph-init
    sed -i "/^my \$rrd =/ c\my \$rrd = \'\/var\/lib\/mailgraph.rrd\'\;" /var/www/cgi-bin/mailgraph.cgi
    sed -i "/^my \$rrd_virus =/ c\my \$rrd_virus = \'\/var\/lib\/mailgraph_virus.rrd\'\;" /var/www/cgi-bin/mailgraph.cgi

    # Mailgraph security modifications
  cd /usr/src/EMFA/mailgraph/   
    #https://bitbucket.org/emfabox/beta/src/c785ed8d99ec2eda2f1edcc124119ac54f0e2c4b/src/PHP-Session-0.27.tar.gz?at=master
    /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/mailgraph/PHP-Session-0.27.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/PHP-Session-0.27.tar.gz
    #https://bitbucket.org/emfabox/beta/src/c785ed8d99ec2eda2f1edcc124119ac54f0e2c4b/src/UNIVERSAL-require-0.15.tar.gz?at=master
 
   /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/mailgraph/UNIVERSAL-require-0.15.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/UNIVERSAL-require-0.15.tar.gz
    #https://bitbucket.org/emfabox/beta/src/c785ed8d99ec2eda2f1edcc124119ac54f0e2c4b/src/CGI-Lite-2.02.tar.gz?at=master
   /usr/bin/wget --no-check-certificate -O /usr/src/EMFA/mailgraph/CGI-Lite-2.02.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/CGI-Lite-2.02.tar.gz

    #https://bitbucket.org/emfabox/beta/raw/0a4c160ac3ca8db8b8467ee7ac64a2f0d90d33c5/src/mailgraph/mailgraph1.cgi
    #https://bitbucket.org/emfabox/beta/raw/d687ae91cc7c35e1f30bb234859af1214ec06a5a/src/mailgraph/mailgraph1.cgi 
    /usr/bin/wget --no-check-certificate -O /var/www/cgi-bin/mailgraph1.cgi https://bitbucket.org/emfabox/beta/raw/d687ae91cc7c35e1f30bb234859af1214ec06a5a/src/mailgraph/mailgraph1.cgi 
    
    chmod 0755 /var/www/cgi-bin/mailgraph1.cgi
    
   tar -xzvf UNIVERSAL-require-0.15.tar.gz
   cd UNIVERSAL-require-0.15
   perl Makefile.PL
   make
   make test
   make install
   cd ..
   
   tar -xzvf PHP-Session-0.27.tar.gz
   cd PHP-Session-0.27
   perl Makefile.PL
   make
   make test
   make install
   cd ..
   
   tar -xzvf CGI-Lite-2.02.tar.gz
   cd CGI-Lite-2.02
   perl Makefile.PL
   make
   make install
    
sed -i "/^my \$VERSION = \"1.14\";/ a\# Begin \nuse PHP::Session;\nuse CGI::Lite;\n\neval {\n  my \$session_name='PHPSESSID';\n  my \$cgi=new CGI::Lite;\n  my \$cookies = \$cgi->parse_cookies;\n  if (\$cookies->{\$session_name}) {\n    my \$session = PHP::Session->new(\$cookies->{\$session_name},{save_path => '/var/lib/php/session/'});\n    if (\$session->get('user_type') ne 'A') {\n      print \"Access Denied\";\n      exit;\n    }\n  } else {\n    print\"Access Denied\";\n    exit;\n  }\n};\nif (\$@) {\n  die(\"Access Denied\");\n}\n# End" /var/www/cgi-bin/mailgraph.cgi


printf "SHELL=/bin/bash\n" > /etc/cron.d/emfa-mailgraph.cron
printf "PATH=/sbin:/bin:/usr/sbin:/usr/bin\n" >> /etc/cron.d/emfa-mailgraph.cron   
printf "# Runs the mailgraph update program\n" > /etc/cron.d/emfa-mailgraph.cron
printf "# This will run every one minute\n" >> /etc/cron.d/emfa-mailgraph.cron
printf "* * * * * root /usr/local/sbin/emfa-mailgraph.sh &> /dev/null\n" >> /etc/cron.d/emfa-mailgraph.cron
printf "\n" >> /etc/cron.d/emfa-mailgraph.cron
clear

timewait 2

printf "SHELL=/bin/bash\n" > /usr/local/sbin/emfa-mailgraph.sh
printf "PATH=/sbin:/bin:/usr/sbin:/usr/bin\n" >> /usr/local/sbin/emfa-mailgraph.sh
printf "perl -w /var/www/cgi-bin/mailgraph1.cgi &> /dev/null\n" >> /usr/local/sbin/emfa-mailgraph.sh
printf "\n" >> /usr/local/sbin/emfa-mailgraph.sh
clear

timewait 2

chmod 0755 /usr/local/sbin/emfa-mailgraph.sh

}
# +---------------------------------------------------+


# +---------------------------------------------------+
# queuegraph
# +---------------------------------------------------+
emfa_queuegraph () {

mkdir -p /usr/src/EMFA/queuegraph
cd /usr/src/EMFA/queuegraph
    
mkdir -p  /tmp/queuegraph-smtp-in
mkdir -p /tmp/queuegraph-smtp-out

if [ ! -d /opt/emfa/postfix-out ] ; then
  mkdir -p /opt/emfa/postfix-out   
fi
    
if [ ! -d /opt/emfa/postfix/ ] ; then
        mkdir -p /opt/emfa/postfix 
fi

#/var/www/cgi-bin/
#https://bitbucket.org/emfabox/beta/raw/a0f5f4bbed51a7f59b0852fc38ec7033c45bb00a/src/queuegraph/queuegraph-smtp-in.cgi
/usr/bin/wget --no-check-certificate -O /var/www/cgi-bin/queuegraph-smtp-in.cgi https://bitbucket.org/emfabox/beta/raw/a0f5f4bbed51a7f59b0852fc38ec7033c45bb00a/src/queuegraph/queuegraph-smtp-in.cgi
    
#https://bitbucket.org/emfabox/beta/raw/a0f5f4bbed51a7f59b0852fc38ec7033c45bb00a/src/queuegraph/queuegraph-smtp-out.cgi
/usr/bin/wget --no-check-certificate -O /var/www/cgi-bin/queuegraph-smtp-out.cgi https://bitbucket.org/emfabox/beta/raw/a0f5f4bbed51a7f59b0852fc38ec7033c45bb00a/src/queuegraph/queuegraph-smtp-out.cgi
    
#/usr/local/bin/
#https://bitbucket.org/emfabox/beta/raw/92a46b5e18c5b1090b90bbb4c727350dca09aa5c/src/queuegraph/queuegraph-smtp-in.sh
/usr/bin/wget --no-check-certificate -O /usr/local/bin/queuegraph-smtp-in.sh  https://bitbucket.org/emfabox/beta/raw/92a46b5e18c5b1090b90bbb4c727350dca09aa5c/src/queuegraph/queuegraph-smtp-in.sh

#https://bitbucket.org/emfabox/beta/raw/92a46b5e18c5b1090b90bbb4c727350dca09aa5c/src/queuegraph/queuegraph-smtp-out.sh
/usr/bin/wget --no-check-certificate -O /usr/local/bin/queuegraph-smtp-out.sh  https://bitbucket.org/emfabox/beta/raw/92a46b5e18c5b1090b90bbb4c727350dca09aa5c/src/queuegraph/queuegraph-smtp-out.sh
    
chmod 0755 /var/www/cgi-bin/queuegraph-smtp-out.cgi
chmod 0755 /var/www/cgi-bin/queuegraph-smtp-in.cgi
     
chmod 0755 /usr/local/bin/queuegraph-smtp-out.sh
chmod 0755 /usr/local/bin/queuegraph-smtp-in.sh


printf "SHELL=/bin/bash\n" > /etc/cron.d/queuegraph.cron
printf "PATH=/sbin:/bin:/usr/sbin:/usr/bin\n" >> /etc/cron.d/queuegraph.cron     
printf "# Runs the queuegraph update program\n"  >> /etc/cron.d/queuegraph.cron
printf "# This will run every one minute\n" >> /etc/cron.d/queuegraph.cron
printf "* * * * * root /usr/local/bin/queuegraph-smtp-in.sh &> /dev/null\n" >> /etc/cron.d/queuegraph.cron
printf "* * * * * root /usr/local/bin/queuegraph-smtp-out.sh &> /dev/null\n" >> /etc/cron.d/queuegraph.cron
printf "#* * * * * perl /var/www/cgi-bin/queuegraph-smtp-out.cgi &> /dev/null\n" >> /etc/cron.d/queuegraph.cron
printf "#* * * * * perl /var/www/cgi-bin/queuegraph-smtp-in.cgi &> /dev/null\n" >> /etc/cron.d/queuegraph.cron
printf "#\n" >> /etc/cron.d/queuegraph.cron
printf "* * * * * root /usr/local/sbin/queuegraph.sh &> /dev/null\n" >> /etc/cron.d/queuegraph.cron
printf "\n" >> /etc/cron.d/queuegraph.cron
clear

timewait 2

printf "SHELL=/bin/bash\n" > /usr/local/sbin/queuegraph.sh
printf "PATH=/sbin:/bin:/usr/sbin:/usr/bin\n" >> /usr/local/sbin/queuegraph.sh
printf "perl -w /var/www/cgi-bin/queuegraph-smtp-out.cgi &> /dev/null\n" >> /usr/local/sbin/queuegraph.sh
printf "perl -w /var/www/cgi-bin/queuegraph-smtp-in.cgi &> /dev/null\n" >> /usr/local/sbin/queuegraph.sh
printf "\n" >> /usr/local/sbin/queuegraph.sh
clear

timewait 2

chmod 0755 /usr/local/sbin/queuegraph.sh
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# SMARTY
# +---------------------------------------------------+
emfa_smarty () {
mkdir -p /usr/src/EMFA/smarty
       cd /usr/src/EMFA/smarty
      
#https://bitbucket.org/emfabox/beta/src/edbc289729380cfacd00d263e8307459aa7d4a69/src/Smarty/Smarty-3.1.21.tar.gz?at=master 
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/smarty/Smarty-3.1.21.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/Smarty/Smarty-3.1.21.tar.gz
      
#https://bitbucket.org/emfabox/beta/src/c120488c186289d59aea27fd83f548788f001c8c/src/Smarty/SmartyPaginate-1.6.tar.gz?at=master
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/smarty/SmartyPaginate-1.6.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/Smarty/SmartyPaginate-1.6.tar.gz
      
#https://bitbucket.org/emfabox/beta/src/c120488c186289d59aea27fd83f548788f001c8c/src/Smarty/SmartyValidate-3.0.3.tar.gz?at=master
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/smarty/SmartyValidate-3.0.3.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/Smarty/SmartyValidate-3.0.3.tar.gz
      
tar xzvf SSmarty-3.1.21.tar.gz
      
if [ ! -d /usr/local/lib/php/Smarty ] ; then
  mkdir -p /usr/local/lib/php/Smarty    
fi

cd Smarty-*
      cd libs/
      yes | cp -rf * /usr/local/lib/php/Smarty
      cd /usr/src/EMFA/smarty
      # install SmartyPaginate
      
      tar xzvf SmartyPaginate-1.6.tar.gz
      cd SmartyPaginate-*
      yes  | cp -rf * /usr/local/lib/php/Smarty
      
      # install SmartyValidate
      cd /usr/src/EMFA/smarty
      tar xzvf SmartyValidate-3.0.3.tar.gz
      cd SmartyValidate-*
      yes  | cp -rf * /usr/local/lib/php/Smarty
      
      # delete NEWS
      yes  | rm /usr/local/lib/php/Smarty/NEWS
      
      # delete README
      yes  | rm /usr/local/lib/php/Smarty/README

      # fix Smarty cache template and create dirs 
      
      if [ ! -d /var/www/emfa_backup/ ] ; then
      mkdir -p /var/www/emfa_backup/
      fi
      
      cd  /var/www/
      chmod 775 emfa_backup
      
      # move smarty cache to /var/cache/ #
      
      if [ ! -d /var/cache/smarty/cache ] ; then
      mkdir -p /var/cache/smarty/cache
      fi
      
      if [ ! -d /var/cache/smarty/templates_c ] ; then
      mkdir -p /var/cache/smarty/templates_c
      fi
      
      cd /var/cache/   
      chmod 775 -R /var/cache/smarty/cache
      chmod 775 -R /var/cache/smarty/templates_c
      
      chown apache:apache -R /var/cache/smarty
      chown apache:apache /var/www/emfa_backup/
      
      cd /var/www/html 
      chown root:apache -R *
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# suhosin
# +---------------------------------------------------+
emfa_suhosin () {
mkdir -p /usr/src/EMFA/suhosin
cd /usr/src/EMFA/suhosin
/usr/bin/wget --no-check-certificate -O /usr/src/EMFA/suhosin/suhosin-0.9.37.1.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/Suhosin/suhosin-0.9.37.1.tar.gz
tar -xzvf suhosin-0.9.37.1.tar.gz
cd suhosin* 
      phpize
      ./configure
      make
      make install

timewait 2

if [ -d /etc/php.d ] ; then 
printf "; Enable suhosin extension module\n" > /etc/php.d/suhosin.ini
printf "extension=suhosin.so\n" >> /etc/php.d/suhosin.ini
printf "suhosin.memory_limit=2048M\n" >> /etc/php.d/suhosin.ini
else    
     echo extension=suhosin.so >> /etc/php.ini            
fi
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# MAILWATCH2RBL
# +---------------------------------------------------+
emfa_mailwatch2rbl () {
mkdir -p /usr/local/mailwatch2rbl/logwatch
cd /usr/local/mailwatch2rbl/logwatch

    #https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/logwatch/mailwatch2rbl.conf
		/usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl.conf https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/logwatch/mailwatch2rbl.conf

		#https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/logwatch/mailwatch2rbl
		/usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/logwatch/mailwatch2rbl

		cd /etc/logwatch/conf/services
		/bin/ln -s /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl.conf .
		/bin/ln -s /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl .
    
    
    cd /usr/share/logwatch/scripts/services
    /bin/ln -s /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl.conf .
		/bin/ln -s /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl .
    

		cd /usr/local/mailwatch2rbl

		#https://bitbucket.org/emfabox/beta/raw/361a43e13412054defa1e486f1a4b590d220033d/src/mailwatch2rbl/config.php
		/usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/config.php https://bitbucket.org/emfabox/beta/raw/361a43e13412054defa1e486f1a4b590d220033d/src/mailwatch2rbl/config.php

		#https://bitbucket.org/emfabox/beta/raw/0648d77639102c9a564f1025b2b9134fe6eb35b2/src/mailwatch2rbl/ez_sql.php
		#/usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/ez_sql.php https://bitbucket.org/emfabox/beta/raw/0648d77639102c9a564f1025b2b9134fe6eb35b2/src/mailwatch2rbl/ez_sql.php

    #https://bitbucket.org/emfabox/beta/raw/3b92c7a65ca063a1973709737e88a379409deb57/src/mailwatch2rbl/ez_sql.php
    /usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/ez_sql.php https://bitbucket.org/emfabox/beta/raw/3b92c7a65ca063a1973709737e88a379409deb57/src/mailwatch2rbl/ez_sql.php
		
    #https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/mailwatch2rbl
		/usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/mailwatch2rbl https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/mailwatch2rbl

		#https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/mw2rbltool
		/usr/bin/wget --no-check-certificate -O /usr/local/mailwatch2rbl/mw2rbltool https://bitbucket.org/emfabox/beta/raw/e03ced48b9480d85d88e133bfa8e669d2390b7fb/src/mailwatch2rbl/mw2rbltool

		cd /usr/local/mailwatch2rbl

    chmod 755 /usr/local/mailwatch2rbl/mailwatch2rbl

    # install logwatch 
    cd /etc/logwatch/conf/services/
    ln -s /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl.conf .
    cd /etc/logwatch/scripts/services/
    ln -s /usr/local/mailwatch2rbl/logwatch/mailwatch2rbl .
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# LSYNCD
# http://www.rackspace.com/knowledge_center/article/install-and-configure-lsyncd
# +---------------------------------------------------+
emfa_install_lsyncd () {
# create transfer sync directory 

mkdir -p /opt/emfa/replication/master
mkdir -p /opt/emfa/replication/slave
mkdir -p /usr/src/EMFA/lsyncd

yum -y install lua lua-devel pkgconfig gcc asciidoc

cd /usr/src/EMFA/lsyncd

#https://bitbucket.org/emfabox/beta/src/4df83f07bd26b771474668453ae4f9a7426a077b/src/lsyncd-2.1.5.tar.gz?at=master
 /usr/bin/wget --no-check-certificate -O  /usr/src/EMFA/lsyncd/lsyncd-2.1.5.tar.gz  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/lsyncd-2.1.5.tar.gz

tar xzvf lsyncd-2.1.5.tar.gz
    cd lsyncd-2.1.5
    ./configure && make && make install

#    Configure Log Rotation
cat << EOF > /etc/logrotate.d/lsyncd
/var/log/lsyncd/*log {
        missingok
        notifempty
        sharedscripts
        postrotate
        if [ -f /var/lock/lsyncd ]; then
            /sbin/service lsyncd restart > /dev/null 2>/dev/null || true
        fi
        endscript
    }
#    
EOF
    
    if [ -f /root/.ssh/id_rsa ]; then
    rm -f /root/.ssh/id_rsa
    fi
    
    if [ -f /root/.ssh/id_rsa.pub ]; then
    rm -f /root/.ssh/id_rsa.pub
    fi
    
    ssh-keygen -t rsa -N '' -f  /root/.ssh/id_rsa  
  # yes | cp  /root/.ssh/id_rsa.pub   /root/.ssh/master.pub
   if [ ! -d /etc/emfa/.ssh ]; then 
   mkdir -p /etc/emfa/.ssh
   fi
   
    cd /root/.ssh/
    rsync -ar  id_rsa.*  /etc/emfa/.ssh
    yes | cp  /etc/emfa/.ssh/id_rsa.pub   /etc/emfa/.ssh/master.pub
       
  #https://bitbucket.org/emfabox/beta/raw/bfd4d81b54b689636240c63b2e9f114925105bc2/src/lsyncd/lsyncd.conf
  /usr/bin/wget --no-check-certificate -O /etc/emfa/lsyncd.conf  https://bitbucket.org/emfabox/beta/raw/bfd4d81b54b689636240c63b2e9f114925105bc2/src/lsyncd/lsyncd.conf
       
  #https://bitbucket.org/emfabox/beta/raw/5642984d7a287643493fad1eda0f4d7c771836ef/src/lsyncd/lsyncd.sh
  /usr/bin/wget --no-check-certificate -O /etc/init.d/lsyncd  #https://bitbucket.org/emfabox/beta/raw/5642984d7a287643493fad1eda0f4d7c771836ef/src/lsyncd/lsyncd.sh
  
  chmod 755 /etc/init.d/lsyncd 

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# FAIL2BAN
# +---------------------------------------------------+
emfa_install_fail2ban () {
Log into syslog instead of log file --> /etc/fail2ban/fail2ban.conf
sed -i "/^logtarget =/ c\logtarget = SYSLOG" /etc/fail2ban/fail2ban.conf

#Disable all default filters -->  /etc/fail2ban/jail.conf

perl -pi -e 's#^(enabled).*=.*#${1} = false#' /etc/fail2ban/jail.conf
CURRIP=`ifconfig eth0 | grep "inet addr" | awk '{ print $2 }' | awk 'BEGIN { FS=":" } { print $2 }'`
     
#https://raw.github.com/fail2ban/fail2ban/master/config/filter.d/postfix-sasl.conf
#/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/postfix-sasl.conf  https://raw.github.com/fail2ban/fail2ban/master/config/filter.d/postfix-sasl.conf


cd  /etc/fail2ban
cat << EOF > /etc/fail2ban/filter.d/postfix-emfa.conf
[Definition]
failregex = \[<HOST>\]: SASL (PLAIN|LOGIN) authentication failed
            reject: RCPT from (.*)\[<HOST>\]: 550 5.1.1
            reject: RCPT from (.*)\[<HOST>\]: 450 4.7.1
            reject: RCPT from (.*)\[<HOST>\]: 554 5.7.1
ignoreregex =
EOF
 
#fi
mv /etc/fail2ban/jail.conf  /etc/fail2ban/jail.conf.install

/usr/bin/wget --no-check-certificate -O /etc/fail2ban/action.d/emfabox.conf  https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/action.d/emfabox.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/action.d/spam.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/action.d/spam.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/jail.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/jail.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/jail.local https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/jail.local

dos2unix /etc/fail2ban/jail.conf
dos2unix /etc/fail2ban/jail.local

/etc/fail2ban/filter.d
#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/postfix.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/postfix.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/postfix.conf
dos2unix /etc/fail2ban/filter.d/postfix.conf

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/postfix_exceeded.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/postfix_exceeded.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/postfix_exceeded.conf
dos2unix /etc/fail2ban/filter.d/postfix_exceeded.conf

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam.conf
dos2unix /etc/fail2ban/filter.d/spam.conf

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_bounce.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_bounce.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_bounce.conf
dos2unix /etc/fail2ban/filter.d/spam_bounce.conf

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_bulk.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_bulk.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_bulk.conf
dos2unix /etc/fail2ban/filter.d/spam_bulk.conf 

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_light.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_light.conf  https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_light.conf
dos2unix /etc/fail2ban/filter.d/spam_light.conf

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_malware.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_malware.conf  https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_malware.conf
dos2unix /etc/fail2ban/filter.d/spam_malware.conf

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_rdns.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_rdns.conf #https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_rdns.conf 

#https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_zero.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_zero.conf https://bitbucket.org/emfabox/beta/raw/ba0034c94e69d44e85163709cd61ec6da5bc12f7/src/fail2ban/filter.d/spam_zero.conf

#https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/fail2ban/filter.d/spam_rdns.conf
/usr/bin/wget --no-check-certificate -O /etc/fail2ban/filter.d/spam_rdns.conf  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/fail2ban/filter.d/spam_rdns.conf

}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Add Admin User
# +---------------------------------------------------+
emfa_add_admin () {
/usr/sbin/useradd admin 
echo password | passwd --stdin admin
# run admin script with admin account
sed -i '1i\\/usr\/local\/sbin\/admin' /home/admin/.bashrc
#admin   ALL=(ALL) 	ALL --> /etc/sudoers
line=$(cat '/etc/sudoers' | grep -n 'Allow root to run any commands anywhere' | grep -o '^[0-9]*')
line=$((line + 2))
sed -i ${line}"i\##\nadmin   ALL=(ALL)       ALL\n" /etc/sudoers

}
# +---------------------------------------------------+


# +---------------------------------------------------+
# SUDO
# +---------------------------------------------------+
# emfa_install_sudo (){
#
#}
# +---------------------------------------------------+

# +---------------------------------------------------+
# Generate HTTPS & Postfix certificate
# +---------------------------------------------------+
emfa_gen_certs () {
cd /etc/pki/tls/certs
      openssl req -new -newkey rsa:4096 -days 1825 -nodes -x509 -subj "/C=US/ST=*/L=*/O=EMFABOX/CN=*" -keyout server.key  -out server.crt
      sed -i "/^SSLCertificateFile / c\SSLCertificateFile /etc/pki/tls/certs/server.crt" /etc/httpd/conf.d/ssl.conf
      sed -i "/^SSLCertificateKeyFile / c\SSLCertificateKeyFile /etc/pki/tls/certs/server.key" /etc/httpd/conf.d/ssl.conf
    
      test -d /etc/postfix/ssl || mkdir /etc/postfix/ssl
      test -d /etc/postfix-out/ssl || mkdir /etc/postfix-out/ssl
      
      test -f /etc/pki/tls/ssl/emfabox.cert || openssl req -new -newkey rsa:4096 -days 1000 -nodes -x509 -subj "/O=EMFABOX/CN=*" -keyout /etc/pki/tls/certs/emfabox.key  -out /etc/pki/tls/certs/emfabox.crt
    
      test -f /etc/postfix/ssl/emfabox.cert || openssl req -new -newkey rsa:2048 -days 1000 -nodes -x509 -subj "/O=EMFABOX/CN=**" -keyout /etc/postfix/ssl/emfabox.key  -out /etc/postfix/ssl/emfabox.crt
      test -f /etc/postfix/ssl/dh_512.pem || openssl gendh -out /etc/postfix/ssl/dh_512.pem -2 512
      test -f /etc/postfix/ssl/dh_1024.pem || openssl gendh -out /etc/postfix/ssl/dh_1024.pem -2 1024
      
      test -f /etc/postfix-out/ssl/emfabox.cert || openssl req -new -newkey rsa:2048 -days 1000 -nodes -x509 -subj "/O=EMFABOX/CN=**" -keyout /etc/postfix-out/ssl/emfabox.key  -out /etc/postfix-out/ssl/emfabox.crt
      test -f /etc/postfix-out/ssl/dh_512.pem || openssl gendh -out /etc/postfix-out/ssl/dh_512.pem -2 512
      test -f /etc/postfix-out/ssl/dh_1024.pem || openssl gendh -out /etc/postfix-out/ssl/dh_1024.pem -2 1024


cd /etc/postfix/ssl
      openssl req -new -x509 -nodes -out smtpd.pem -keyout smtpd.pem -days 3650 -subj "/O=EMFABOX/CN=**"
      \cp /etc/postfix/ssl/smtpd.pem /etc/postfix/ssl/ca-bundle.pem
      \cp /etc/postfix/ssl/smtpd.pem /etc/postfix/ssl/smtpd.key
    
cd /etc/postfix-out/ssl
      openssl req -new -x509 -nodes -out smtpd.pem -keyout smtpd.pem -days 3650 -subj "/O=EMFABOX/CN=**"
      \cp /etc/postfix-out/ssl/smtpd.pem /etc/postfix-out/ssl/ca-bundle.pem
      \cp /etc/postfix-out/ssl/smtpd.pem /etc/postfix-out/ssl/smtpd.key
      
    # Logjam Vulnerability    
    openssl dhparam -out /etc/postfix-out/ssl/dhparam.pem 2048
    postconf -c /etc/postfix-out/ -e "smtpd_tls_dh1024_param_file = /etc/postfix-out/ssl/dhparam.pem"
    postconf -c /etc/postfix-out/ -e "smtpd_tls_ciphers = low"      
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# kernel settings
# +---------------------------------------------------+
emfa_kernel_settings () {
sed -i '/net.bridge.bridge-nf-call-/d' /etc/sysctl.conf
echo -e "# IPv6 \nnet.ipv6.conf.all.disable_ipv6 = 1 \nnet.ipv6.conf.default.disable_ipv6 = 1 \nnet.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
sysctl -q -p  
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# OPENVAS
# +---------------------------------------------------+
emfa_secsettings () {

# net.ipv4.tcp_timestamps = 0
# The remote host implements TCP timestamps and therefore allows to compute the uptime.
sed -i '/net.ipv4.tcp_timestamps/d' /etc/sysctl.conf

echo "#EMFABOX disable timestamp" >> /etc/sysctl.conf
echo "net.ipv4.tcp_timestamps = 0" >> /etc/sysctl.conf

# Remove ETag  
#Header unset
#FileETag none

echo "#EMFABOX disable ETag" >> /etc/httpd/conf/httpd.conf
echo "Header unset" >> /etc/httpd/conf/httpd.conf
echo "FileETag none" >> /etc/httpd/conf/httpd.conf
echo "" >> /etc/httpd/conf/httpd.conf

## REST API
echo "" >> /etc/httpd/conf/httpd.conf
echo '# REST API'>> /etc/httpd/conf/httpd.conf    
echo '<Directory "/var/www/html/emfa/v1">'>> /etc/httpd/conf/httpd.conf
echo '	Options FollowSymLinks'>> /etc/httpd/conf/httpd.conf
echo '     	AllowOverride All'>> /etc/httpd/conf/httpd.conf 
echo '</Directory>'>> /etc/httpd/conf/httpd.conf
echo "" >> /etc/httpd/conf/httpd.conf

#############################
#    RewriteEngine on
#    RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
#    RewriteRule .* - [F]

echo "#EMFABOX RewriteEngine" >> /etc/httpd/conf/httpd.conf
echo "RewriteEngine on"  >> /etc/httpd/conf/httpd.conf
echo "RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)"  >> /etc/httpd/conf/httpd.conf
echo "RewriteRule .* - [F]"  >> /etc/httpd/conf/httpd.conf
echo "" >> /etc/httpd/conf/httpd.conf
}
# +---------------------------------------------------+



# +---------------------------------------------------+
# IPTABLES
# +---------------------------------------------------+
emfa_firewall () {
# BLOCK SMTP HOSTS & Firewall Ruleset # 2015-03-06
#set the firewall
printf "# Firewall configuration written by EMFABox install script\n" > /etc/sysconfig/iptables
printf "# Manual customization of this file is not recommended.\n">> /etc/sysconfig/iptables
printf "*filter\n" >> /etc/sysconfig/iptables
printf ":INPUT ACCEPT [0:0]\n" >> /etc/sysconfig/iptables
printf ":FORWARD ACCEPT [0:0]\n" >> /etc/sysconfig/iptables
printf ":OUTPUT ACCEPT [0:0]\n" >> /etc/sysconfig/iptables
printf ':SMTP-BLOCK-INPUT - [0:0]'"\n" >> /etc/sysconfig/iptables
printf -- '-A INPUT -p tcp -m multiport --dports 22,80,443,25,465 -j SMTP-BLOCK-INPUT'"\n" >> /etc/sysconfig/iptables
printf ':FireWall-INPUT - [0:0]'"\n" >> /etc/sysconfig/iptables
printf -- '-A INPUT -j FireWall-INPUT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FORWARD -j FireWall-INPUT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -i lo -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -p icmp --icmp-type any -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state NEW -m tcp -p tcp --dport 25 -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state NEW -m tcp -p tcp --dport 587 -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -m state --state NEW -m tcp -p tcp --dport 4200 -j ACCEPT'"\n" >> /etc/sysconfig/iptables
printf -- '-A FireWall-INPUT -j REJECT --reject-with icmp-host-prohibited'"\n" >> /etc/sysconfig/iptables
printf "COMMIT\n" >> /etc/sysconfig/iptables
}
# +---------------------------------------------------+


# +---------------------------------------------------+
# SERVICES
# +---------------------------------------------------+
emfa_services () {
# These services we really don't need.
    chkconfig ip6tables off
    chkconfig cpuspeed off
    chkconfig lvm2-monitor off
    chkconfig mdmonitor off
    chkconfig netfs off
    chkconfig smartd off
    chkconfig abrtd off
    chkconfig portreserve off
    # Postfix is launched by MailScanner
    chkconfig postfix off 
    # auditd is something for an future release..
    chkconfig auditd off
    #
    service clamd restart
    service httpd start
    chkconfig --level 345 httpd on
    chkconfig --level 2345 shellinaboxd on
    #service sqlgrey restart
    chkconfig --level 345 sqlgrey on
    service postfix stop
    chkconfig postfix off
    #service clamd restart
    chkconfig --level 345 clamd on
    #service postfix-out restart
    chkconfig --level 345 postfix-out on
    #service crond start
    chkconfig --level 345 crond on
    #service MailScanner restart
    chkconfig --level 345 MailScanner on
    #service spamassassin start
    #chkconfig --level 345 spamassassin on
    #service opendkim restart
    chkconfig --level 345 opendkim on
    #yum remove bind-chroot -y
    #yum install bind -y	
    chkconfig --level 345 named on
    #add denyhosts but do not start it first
    #chkconfig --add denyhosts
    #service opendkim restart
    chkconfig --level 345 mysqld on
    #fail2ban
    chkconfig --level 345 fail2ban on
    #mailgraph
    chkconfig --add mailgraph-init
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# REQUIREMENTS
# +---------------------------------------------------+
 emfa_requirements (){
 # Write version file
    echo "$MYVERSION" > /etc/EMFA-Version
    
    # more directories
    
    # update
    if [ ! -d ${UPDATE_DIR} ] ; then
      mkdir -p ${UPDATE_DIR}
    fi
    
    # backup
    if [ ! -d ${BACKUP_DIR} ] ; then
      mkdir -p ${BACKUP_DIR}
    fi
    
    # KAM
    if [ ! -d ${BACKUP_DIR}/KAM ] ; then
      mkdir -p ${BACKUP_DIR}/KAM
    fi

    # lib directory   
    if [ ! -d ${MY_LIB_DIR} ] ; then
      mkdir -p ${MY_LIB_DIR}
    fi

    
    echo "#EMFABox" >> /etc/rc.local
    echo "/usr/local/sbin/emfa-issue.sh" >> /etc/rc.local
    echo "/usr/sbin/ntpdate -s time.nist.gov" >> /etc/rc.local   
    
   
    # Trusted Networks Config
    touch /etc/sysconfig/WEBTOKEN_trusted_networks
    
    ### issue text
    #sed -i '/\+/d' /etc/issue
    CURRIP=`ifconfig eth0 | grep "inet addr" | awk '{ print $2 }' | awk 'BEGIN { FS=":" } { print $2 }'`
    CurrentRelease=`cat /etc/redhat-release`
    GATEWAYIP=$(/sbin/ip route | awk '/default/ { print $3 }')
    echo -e "\033[0;32mEMFABOX\033[0;37m - \e[0;31meMailSecurity\033[0;37m powered by $CurrentRelease" >/etc/issue
    echo "Kernel \r on an \m"  >>/etc/issue
    #echo "" >>/etc/issue
    echo -e "\033[0;32mHostname  : \e[0;31m `hostname`\033[0;37m" >> /etc/issue
    echo -e "\033[0;32mIP-Address: \e[0;31m $CURRIP\033[0;37m" >> /etc/issue
    echo -e "\033[0;32mGateway   : \e[0;31m $GATEWAYIP\033[0;37m" >> /etc/issue
    echo ".................................................................">>/etc/issue
    echo -e "\033[0;37m" >> /etc/issue
    echo -e "To manage the Appliance go to: \033[0;32mhttp://$CURRIP\033[0;37m">>/etc/issue
    echo -e "or on console run \033[0;32m'admin'\033[0;37m from commandline.">>/etc/issue
    echo "">>/etc/issue
    echo ".................................................................">>/etc/issue
    echo "">>/etc/issue
    
    # SCRIPTS


  
  touch /var/log/emfa/EMFA-SA-Update.log
  
  echo "" > /var/log/emfa/EMFA-SA-Update.log

  # Grab scripts/programs

    #ssh login banner
    sed -i '/^#LoginGraceTime/ c\LoginGraceTime 2m' /etc/ssh/sshd_config
    sed -i '/^#MaxAuthTries/ c\MaxAuthTries 6' /etc/ssh/sshd_config
    sed -i '/^#Banner/ c\Banner \/etc\/issue.net' /etc/ssh/sshd_config
    cd /etc
    cp /etc/issue.net /etc/issue.net.old
    cat << EOF > /etc/issue.net
    NOTICE TO USERS
    
    THIS IS A PRIVATE COMPUTER SYSTEM. It is for authorized use only.
    Users (authorized or unauthorized) have no explicit or implicit
    expectation of privacy.
    
    Any or all uses of this system and all files on this system may
    be intercepted, monitored, recorded, copied, audited, inspected,
    and disclosed to authorized site and law enforcement personnel,
    as well as authorized officials of other agencies, both domestic
    and foreign.  By using this system, the user consents to such
    interception, monitoring, recording, copying, auditing, inspection,
    and disclosure at the discretion of authorized site personnel.
    
    Unauthorized or improper use of this system may result in
    administrative disciplinary action and civil and criminal penalties.
    By continuing to use this system you indicate your awareness of and
    consent to these terms and conditions of use.   LOG OFF IMMEDIATELY
    if you do not agree to the conditions stated in this warning.
    
EOF
    #-------------------------------------------
 
    # Compress logs from logrotate
    sed -i "s/#compress/compress/g" /etc/logrotate.conf
    
    # Set EMFA-Init to run at first root login:
    #sed -i '1i\\/usr\/local\/sbin\/EMFA-Init' /root/.bashrc
 
 }
# +---------------------------------------------------+

# +---------------------------------------------------+
# RBLDNSD
# +---------------------------------------------------+
emfa_rbldnsd () {
cd /etc/init.d/
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/init.d/rbldnsd
/usr/bin/wget --no-check-certificate -O /etc/init.d/rbldnsd https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/init.d/rbldnsd
chmod 755  /etc/init.d/rbldnsd

chkconfig --add rbldnsd
chkconfig rbldnsd on

timewait 3

cd /usr/local/sbin/
#https://bitbucket.org/emfabox/update/raw/7d9d1b1c0bed7692c4e344710e50a692224c5760/1009/scripts/usr/local/sbin/rbldnsd.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/rbldnsd.sh https://bitbucket.org/emfabox/update/raw/7d9d1b1c0bed7692c4e344710e50a692224c5760/1009/scripts/usr/local/sbin/rbldnsd.sh
chmod 755 rbldnsd.sh

./rbldnsd.sh create

#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/sysconfig/rbldnsd
cd /etc/sysconfig
/usr/bin/wget --no-check-certificate -O /etc/sysconfig/rbldnsd  https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/sysconfig/rbldnsd

cd /etc/named/
#https://bitbucket.org/emfabox/update/raw/3708627ff32c2a583d83a4790b578fd5971be844/1009/etc/named/ip.pools
/usr/bin/wget --no-check-certificate -O /etc/named/ip.pools https://bitbucket.org/emfabox/update/raw/3708627ff32c2a583d83a4790b578fd5971be844/1009/etc/named/ip.pools

service rbldnsd restart
}
# +---------------------------------------------------+

# +---------------------------------------------------+
# 
# +---------------------------------------------------+
# emfa_usr_local_sbin () {
#}
# +---------------------------------------------------+


# +---------------------------------------------------+
# PATCHES
# +---------------------------------------------------+
emfa_patch () {

touch /opt/emfa/id/patches

mkdir -p /opt/emfa/patches

#https://bitbucket.org/emfabox/beta/src/4fec9675887b66d2333107b8102160d035cd897a/src/perlpatches/IO-Socket-INET6-2.72.tar.gz?at=master
/usr/bin/wget --no-check-certificate -O /opt/emfa/patches/IO-Socket-INET6-2.72.tar.gz https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/perlpatches/IO-Socket-INET6-2.72.tar.gz

cd /opt/emfa/patches

tar xzvf IO-Socket-INET6-2.72.tar.gz

cd  IO-Socket-INET6-*
perl Makefile.PL
make
#make -i
make test
make install

#Net-DNS-Resolver-Programmable-v0.003.patch

#https://bitbucket.org/emfabox/beta/raw/190962a013242409e92156de804160aa53642ee1/src/perlpatches/Net-DNS-Resolver-Programmable-v0.003.patch
/usr/bin/wget --no-check-certificate -O /opt/emfa/patches/Net-DNS-Resolver-Programmable-v0.003.patch  https://bitbucket.org/emfabox/beta/raw/ff1d296db7bf23e605b0af814d20d530a92659cf/src/perlpatches/Net-DNS-Resolver-Programmable-v0.003.patch
patch /usr/local/share/perl5/Net/DNS/Resolver/Programmable.pm /opt/emfa/patches/Net-DNS-Resolver-Programmable-v0.003.patch

# Mail-SpamAssassin-3.4.0.patch
#https://bitbucket.org/emfabox/beta/raw/190962a013242409e92156de804160aa53642ee1/src/perlpatches/Mail-SpamAssassin-3.4.0.patch
#/usr/bin/wget --no-check-certificate -O /opt/emfa/patches/Mail-SpamAssassin-3.4.0.patch https://bitbucket.org/emfabox/beta/raw/190962a013242409e92156de804160aa53642ee1/src/perlpatches/Mail-SpamAssassin-3.4.0.patch

#patch /usr/local/share/perl5/Mail/SpamAssassin/DnsResolver.pm /opt/emfa/patches/Mail-SpamAssassin-3.4.0.patch

#Mail-ClamAV-0.22.patch
#https://bitbucket.org/emfabox/beta/raw/190962a013242409e92156de804160aa53642ee1/src/perlpatches/Mail-ClamAV-0.22.patch
/usr/bin/wget --no-check-certificate -O /opt/emfa/patches/Mail-ClamAV-0.22.patch  https://bitbucket.org/emfabox/beta/raw/190962a013242409e92156de804160aa53642ee1/src/perlpatches/Mail-ClamAV-0.22.patch


#/usr/bin/wget --no-check-certificate -O /opt/emfa/patches/RegistrarBoundaries.pm https://bitbucket.org/emfabox/beta/raw/721581c6bd37fd6315d69b50c57be584d44db17f/src/RegistrarBoundaries.pm
#rm -f /usr/local/share/perl5/Mail/SpamAssassin/Util/RegistrarBoundaries.pm
#mv /opt/emfa/patches/RegistrarBoundaries.pm /usr/local/share/perl5/Mail/SpamAssassin/Util/RegistrarBoundaries.pm

#https://bitbucket.org/emfabox/beta/raw/0a97a7ed2012a4bdb020c46d7c660f02943ebe62/src/perlpatches/URILocalBL.pm
mv  /usr/local/share/perl5/Mail/SpamAssassin/Plugin/URILocalBL.pm /usr/local/share/perl5/Mail/SpamAssassin/Plugin/URILocalBL.pm.install
/usr/bin/wget --no-check-certificate -O /usr/local/share/perl5/Mail/SpamAssassin/Plugin/URILocalBL.pm  https://bitbucket.org/emfabox/beta/raw/0a97a7ed2012a4bdb020c46d7c660f02943ebe62/src/perlpatches/URILocalBL.pm

}
# +---------------------------------------------------+

# +---------------------------------------------------+
#Set values for mailscanner sql config
# +---------------------------------------------------+
emfa_msdbconf () {
mkdir -p /opt/emfa/msdbconf
source /etc/emfa/variables.conf
#Set values for mailscanner sql config
echo "DELETE FROM ms_config;">/opt/emfa/msdbconf/emfa.sql
echo "DELETE FROM ms_rulesets;">>/opt/emfa/msdbconf/emfa.sql
echo "TRUNCATE TABLE ms_config;">>/opt/emfa/msdbconf/emfa.sql
echo "TRUNCATE TABLE ms_rulesets;">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '1', 'confserialnumber', 'confserialnumber');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'postfix', 'mta', 'mta');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'postfix', 'runasuser', 'runasuser');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'mtagroup', 'runasgroup', 'runasgroup');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'clamd', 'virusscanners', 'virusscanners');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'virusscan.customize', 'virusscan', 'virusscan');">>/opt/emfa/msdbconf/emfa.sql
##/usr/share/MailScanner/MailScanner/CustomFunctions
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/usr/share/MailScanner/MailScanner/CustomFunctions', 'customfunctionsdir', 'customfunctionsdir');">>/opt/emfa/msdbconf/emfa.sql
##
#echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filenameallow.customize', 'allowfilenames', 'allowfilenames');">>/opt/emfa/msdbconf/emfa.sql
#echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filenamedeny.customize', 'denyfilenames', 'denyfilenames');">>/opt/emfa/msdbconf/emfa.sql
#echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filetypeallow.customize', 'allowfiletypes', 'allowfiletypes');">>/opt/emfa/msdbconf/emfa.sql
#echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filetypedeny.customize', 'denyfiletypes', 'denyfiletypes');">>/opt/emfa/msdbconf/emfa.sql
##
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filenameallow.customize', 'allowfilenames', 'allowfilenames');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filenamedeny.customize', 'denyfilenames', 'denyfilenames');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filetypeallow.customize', 'allowfiletypes', 'allowfiletypes');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filetypedeny.customize', 'denyfiletypes', 'denyfiletypes');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filenameallow.customize', 'aallowfilenames', 'aallowfilenames');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filenamedeny.customize', 'adenyfilenames', 'adenyfilenames');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filetypeallow.customize', 'aallowfiletypes', 'aallowfiletypes');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'filetypedeny.customize', 'adenyfiletypes', 'adenyfiletypes');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/etc/MailScanner/filename.rules', 'filenamerules', 'filenamerules');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/etc/MailScanner/filetype.rules', 'filetyperules', 'filetyperules');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/etc/MailScanner/filename.rules', 'afilenamerules', 'afilenamerules');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/etc/MailScanner/filetype.rules', 'afiletyperules', 'afiletyperules');">>/opt/emfa/msdbconf/emfa.sql
##
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/var/spool/MailScanner/incoming', 'incomingworkdir', 'incomingworkdir');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/var/spool/MailScanner/quarantine', 'quarantinedir', 'quarantinedir');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/var/spool/MailScanner/incoming', 'incomingworkdir', 'incomingworkdir');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/var/spool/postfix/hold', 'incomingqueuedir', 'inqueuedir');">>/opt/emfa/msdbconf/emfa.sql
#  outqueuedir = outgoingqueuedir
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/var/spool/postfix/incoming', 'outgoingqueuedir', 'outqueuedir');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'yes', 'findphishingfraud', 'findphishing');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'yes', 'alsofindnumericphishing', 'phishingnumbers');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/etc/MailScanner/phishing.bad.sites.conf', 'phishingbadsitesfile', 'phishingblacklist');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '/etc/MailScanner/phishing.safe.sites.conf', 'phishingsafesitesfile', 'phishingwhitelist');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '0', 'maximumarchivedepth', 'maxzipdepth');">>/opt/emfa/msdbconf/emfa.sql	
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'yes', 'AllowPasswordProtectedArchives', 'allowpasszips');">>/opt/emfa/msdbconf/emfa.sql	
sleep 1
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^Non Spam Actions = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:19}', 'nonspamactions', 'hamactions');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^Spam Actions = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:15}', 'spamactions', 'spamactions');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^High Scoring Spam Actions = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:28}', 'highscoringspamactions', 'highscorespamactions');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^Max Children = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:15}', 'maxchildren', 'children');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^\%report\-dir\% = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:15}', '%report-dir%', '%report-dir%');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^MCP Checks = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:13}', 'mcpchecks', 'mcpchecks');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^MCP Actions = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:14}', 'mcpactions', 'mcpactions');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^High Scoring MCP Actions = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:27}', 'highscoringmcpactions', 'highscoremcpactions');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^MCP Required SpamAssassin Score = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:34}', 'mcprequiredspamassassinscore', 'mcpreqspamassassinscore');">>/opt/emfa/msdbconf/emfa.sql
tmp=`cat /etc/MailScanner/MailScanner.conf | egrep '^MCP High SpamAssassin Score = '`
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '${tmp:30}', 'mcphighspamassassinscore', 'mcphighspamassassinscore');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', '%org-name%-$WATERMARK_SECRET', 'watermarksecret', 'mshmac');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'spammodifysubject.customize', 'spammodifysubject', 'spammodifysubject');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('spammodifysubject', '9999', 'To: Default no');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('textsigs', '9999', 'To: default /etc/MailScanner/reports/en/inline.sig.out.txt');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('htmlsigs', '9999', 'To: default /etc/MailScanner/reports/en/inline.sig.out.html');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('cleansigs', '9999', 'To: default yes');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('spammodifysubject', '9999', 'To: default yes');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('maxzipdepth', '9999', 'To: default 2');">>/opt/emfa/msdbconf/emfa.sql
echo "INSERT INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('virusscan', '9999', 'FromOrTo: default yes');">>/opt/emfa/msdbconf/emfa.sql

timewait 2

/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQL_ROOTPASS}" mailscanner </opt/emfa/msdbconf/emfa.sql
}

emfa_fixups_tasks () {
mkdir -p /opt/emfa/fixups
if [ ! -d /var/www/.spamassassin ] ; then
mkdir /var/www/.spamassassin
fi

chown postfix:postfix /var/www/.spamassassin

# Variables
QUARANTINEDIR="/var/spool/MailScanner/quarantine"
DIRMODE=0770	# drwxrwx---
FILEMODE=0660	# -rw-rw----
GROUP=apache	# Should be the same group as the web server

# Directories
find /var/spool/MailScanner/quarantine -type d \( \! -group $GROUP -o -type d \! -perm $DIRMODE \) -print | while read DIR;
do
 chmod $DIRMODE "$DIR"
 chown :$GROUP "$DIR" 
done

# Files
find /var/spool/MailScanner/quarantine -type f \( \! -group $GROUP -o \! -perm $FILEMODE \) -print | while read FILE;
do
 chmod $FILEMODE "$FILE"
 chown :$GROUP "$FILE"
done


#PHP INI
sed -i "/^upload_max_filesize =/ c\upload_max_filesize = 256M" /etc/php.ini
# php timezone
DATETZ=`head -n 1 /etc/sysconfig/clock| sed 's/ZONE=//g'`
sed -i "s#;date.timezone =#date.timezone =${DATETZ}#" /etc/php.ini
sed -i 's/max_execution_time = 30/max_execution_time = 60/g' /etc/php.ini

# setup postfix chroot
#sh /usr/share/doc/postfix-2.6.6/examples/chroot-setup/LINUX2
sh /usr/share/doc/postfix-3.0.4/examples/chroot-setup/LINUX2

visudo -c -f /etc/sudoers.d/EMFA-Services > /tmp/visudo


}

# +---------------------------------------------------+
# CLEANUP
# +---------------------------------------------------+
emfa_cleanup () {

# final update
yum -y update
# get rpms
rpm -qa --queryformat '%{NAME} \n' > /opt/emfa/logs/rpms_installed.log
 
#disable SELINUX
sed -i '/SELINUX=enforcing/ c\SELINUX=disabled' /etc/selinux/config

# Remove boot splash so we can see whats going on while booting and set console reso to 800x600
sed -i 's/\<rhgb quiet\>/ vga=771/g' /boot/grub/grub.conf

# switch to opt directory
cd /opt/
# Secure SSH
sed -i '/^#PermitRootLogin/ c\PermitRootLogin yes' /etc/ssh/sshd_config
# clear dns entries
echo "" > /etc/resolv.conf
# Stop running services 
service mysqld stop
# clear source files
rm -rf /usr/src/EMFA/*
# clean allowed hosts sql
rm -rf /usr/src/allowed_hosts.sql
# Clean SSH keys (generate at first boot)
/bin/rm -f /etc/ssh/ssh_host_*
# clean yum cache
yum clean all
   
    # clear logfiles
    rm -f /var/log/clamav/freshclam.log
    rm -f /var/log/messages
    touch /var/log/messages
    chmod 600 /var/log/messages
    rm -f /var/log/clamav-unofficial-sigs.log
    rm -f /var/log/cron
    touch /var/log/cron
    chmod 600 /var/log/cron
    rm -f /var/log/dmesg.old
    rm -f /var/log/dracut.log
    rm -f /var/log/httpd/*
    rm -f /var/log/maillog
    touch /var/log/maillog
    chmod 600 /var/log/maillog
    rm -f /var/log/mysqld.log
    touch /var/log/mysqld.log
    chown mysql:mysql /var/log/mysqld.log
    chmod 640 /var/log/mysqld.log
    rm -f /var/log/yum.log    
    touch /var/log/yum.log
    chmod 600 /var/log/yum.log

    # Clean root
    rm -f /root/anaconda-ks.cfg
    rm -f /root/install.log
    rm -f /root/install.log.syslog

    # Clean tmp
    rm -rf /tmp/*
    
    # create staging directory ..  12-03-2014  #
    
    if [ ! -d /opt/emfa/staging ] ; then
      mkdir -p /opt/emfa/staging
    fi
    
    # create 
    # config temp dir
    if [ ! -d /opt/emfa/tmp ] ; then
      mkdir -p /opt/emfa/tmp/
    chown -R apache:apache /opt/emfa/tmp/
    fi
    
    #Remove duplicated clamav db's # jordan
    
    if [ -f /var/lib/clamav/main.cvd ] ; then
    rm -rf /var/lib/clamav/main.cld
    fi
    
    if [ -f /var/lib/clamav/daily.cvd ] ; then
    rm -rf /var/lib/clamav/daily.cld
    fi
    
    #
    # EMFABOX_FQHOSTNAME="${MYHOSTNAME}"
    # EMFABOX_HOSTNAME="${MY_SUB_DOMAIN}"
    # EMFABOX_DOMAIN="${MY_DOMAIN}"
    
    # enable compression in apache
    sed -i '92i SetOutputFilter DEFLATE' /etc/httpd/conf/httpd.conf
    
    # MailWatch requires access to /var/spool/postfix/hold & incoming dir's
    #chown -R postfix:apache /var/spool/postfix/hold
    #chown -R postfix:apache /var/spool/postfix/incoming
    #chmod -R 750 /var/spool/postfix/hold
    #chmod -R 750 /var/spool/postfix/incoming
    
    #GEOIP
    chmod 755 /var/www/html/master/lib/temp
    chown -R apache:apache  /var/www/html/master/lib/temp/

    # recreate hosts file
    myIP=`ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'`
    myHost=`uname -n`
    myDomain=`dnsdomainname`
    mySHName=$(hostname | cut -d. -f1)
    
    
    echo "127.0.0.1 localhost.localdomain localhost">/etc/hosts
    echo "${myIP} ${myHost} ${mySHName}">>/etc/hosts
    
### 
echo "nameserver 127.0.0.1" > /etc/resolv.conf
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf

#Disable dhclient-script from updating resolv.conf

cat <<END > /etc/dhclient-enter-hooks
#!/bin/sh
make_resolv_conf(){
        :
}
END

chmod a+x /etc/dhclient-enter-hooks

## to be sure --> 
echo 'PEERDNS="no"'  >> /etc/sysconfig/network-scripts/ifcfg-eth0

/usr/local/sbin/set_permissions.sh
    
      # Clean networking in preparation for creating VM Images
      #rm -f /etc/udev/rules.d/70-persistent-net.rules
      #echo -e "DEVICE=eth0" > /etc/sysconfig/network-scripts/ifcfg-eth0
      #echo -e "BOOTPROTO=dhcp" >> /etc/sysconfig/network-scripts/ifcfg-eth0

       # zero disks for better compression (VM images) 
       # dd if=/dev/zero of=/filler bs=1000
       # rm -f /filler
       # dd if=/dev/zero of=/tmp/filler bs=1000
       # rm -f /tmp/filler
       # dd if=/dev/zero of=/boot/filler bs=1000
       # rm -f /boot/filler
       # dd if=/dev/zero of=/var/filler bs=1000
       # rm -f /var/filler
   
}
# +---------------------------------------------------+

#-----------------------------------------------------------------------------#
# Restart 
#-----------------------------------------------------------------------------#
emfa_restart ()
{
echo -e "\e[1;42m EMFABOX ... All Done\e[0m"
timewait 15
#rm -f /etc/rc.d/rc3.d/S86ks-post-reboot
clear
echo -e "\e[1;34m Rebooting in three seconds... \e[0m"
timewait 3
# swap to console 1
#chvt 1
shutdown -r now
exit 0
}
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Main function
#-----------------------------------------------------------------------------#
function main () 
{
    emfa_prebuild
#	emfa_efarepo
    emfa_epelrepo
    emfa_postfix_rpm
    emfa_upgradeOS
    emfa_rpms
    emfa_cpan
    emfa_build_config
    emfa_mtagroup
    emfa_masterfiles
#    emfa_mailscanner
#    emfa_postfix_out
#    emfa_spf
#    emfa_mysql
#    emfa_mysql_ssl
#    emfa_opendkim
#    emfa_opendmarc
#    emfa_sqlgrey
#    emfa_cluebringer
#    emfa_postfix
#   emfa_post_out_db
}


#-----------------------------------------------------------------------------#
# Run the main script
#-----------------------------------------------------------------------------#
main
#-----------------------------------------------------------------------------#
