<?php

//
// GFENERAL CONFIGURATION
//

// mim score to be classified as definite spam
$spamscore = 8;

// anything below this is classed as definite ham
$hamscore = 5;

// minimum number of spams (and no hams) for client ip to be blocked
$minspamcount = 2;

// number of hourse a client should be blocked for
$blocktime = 120;

// error template. The following variables can be used
// [ip]       - the ip address of the connecting client
// [count]    - the number of messages sent
// [spam]     - the number of definite spams sent
// [expires]  - date and time when the block will expire e.g 2007-09-22 21:53:28
// [expires2] - date and time when the block will expire in rfc2822 format e.g Sat, 22 Sep 2007 21:53:28 +0100
// [expires3] - date and time when the block will expire in rfc2822 but timezone by name e.g Sat, 22 Sep 2007 21:53:28 (BST)
// [hamscore] - the threshold for a definite ham
// [spamscore] - the threshold for a definite spam
// [minspamcount] - the required number of definite spams in order to be blocked 
//$errtemplate = "You have been blocked for sending [spam] spam emails and no non-spam emails to us within the last 23 hours. You will be unblocked shortly after [expires3]";
$errtemplate = "REJECT You have been blocked for sending [spam] spam emails";


//
// RBL CONFIGURATION
//

// temp file for writing new rbl
$rbltempfile = "/tmp/mailwatch2rbl.rbl";

// final location of rbl file
// leave blank to disable
//$rblfile = "/usr/local/dnsbl/rbldnsd/autoblock";
$rblfile = "";

// SOA record to put in the rbldnsd file.
$rblsoa = '$SOA 300 localhost. postmaster.localhost. 0 300 150 86400 300';

// NS record to put in the rbldnsd file.
$rblns = '$NS 300 localhost.';

// rbl file mode.
// 'simple' is where the complete error text (obtained from the template) is written into the rbl.
// This will however cause the rbl file to become quite large and consume more memory.
// 
// The new default is 'advanced'. This makes the rbl file much smaller but you have a more limited amount of information
// it can contain as it relies on its own template.
$rblmode = "advanced";

// rbls1 and rbls2 are two strings which can be defined and then referenced as $1 and $2 within the rbltemplate.
// This is functionality built into rbldnsd. rbltemplate is the only string supporting variables and then only the 3
// [expires] [expires2] and [expires3] variables are supported. '$' can also be used as an equivilent to [ip]
$rbls1 = '$1 You have been blocked for sending more than 4';
$rbls2 = '$2 spam emails and no non-spam emails to us within the last 23 hours. You will be unblocked shortly after';
$rbltemplate = '$1 $2 [expires3]';

//
// CUSTOM FILE
//

// temp file for writing custom file
$customtempfile = "/tmp/mailwatch2rbl.tmp";

// final location of custom file
// leave blank to disable
$customfile = "/etc/postfix/mailwatch2rbl.cidr";

// custom file template. The following variables can be used
// [ip]       - the ip address of the connecting client
// [errormsg] - the error message
//$customtemplate = "[ip]	550 5.5.0 [errormsg]"; // sendmail example
$customtemplate = "[ip]	REJECT";

// command to run if the new custom one is different to the previous one
// leave blank to disable
$customcmd = "";

?>
