#!/bin/bash
#
# 10-09-2015 
export slave=$1
export psk=$2
export wan=$3
# send authorized_keys over ssh_pass to remote partner
export rootpass=$4


if [ "$1" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-grant host clusterpassword wan(true/false) rootpassword"
        echo ""
        exit 0
fi

if [ "$2" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-grant host clusterpassword wan(true/false) rootpassword"
        echo ""
        exit 0
fi

MYSQLROOTPWD=`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`

# Grant slave permissions on database
echo "GRANT REPLICATION SLAVE, REPLICATION CLIENT, SUPER, RELOAD on *.* TO 'rep_user'@'$slave' IDENTIFIED BY '$psk';">/tmp/sql.tmp
echo "GRANT REPLICATION SLAVE, REPLICATION CLIENT, SUPER, RELOAD on *.* TO 'rep_user'@'localhost' IDENTIFIED BY '$psk';">>/tmp/sql.tmp
echo "GRANT SELECT ON cluster.* TO rep_user@'$slave';">>/tmp/sql.tmp

mysql -u root --password=$MYSQLROOTPWD < /tmp/sql.tmp
rm -rf /tmp/sql.tmp

#Create local user for ssh connection
/bin/egrep  -i "^rep_user:" /etc/passwd
if [ $? -eq 0 ]; then
   echo $psk | passwd --stdin rep_user
else
   /usr/sbin/useradd -g users -s /bin/bash rep_user
   echo $psk | passwd --stdin rep_user
fi
#Generate ssh keys for password-less ssh login with remote host
if [ ! -d "/root/.ssh/" ]; then
        mkdir /root/.ssh/
fi
if [ -f /root/.ssh/id_rsa ]; then
   rm -rf /root/.ssh/id_rsa*
   ssh-keygen -t rsa -f /root/.ssh/id_rsa -N ""
else
   ssh-keygen -t rsa -f /root/.ssh/id_rsa -N ""
fi

# Store Private and Public Key in cluster dir to allow php use them
if [ ! -d "/usr/local/sbin/cluster/.ssh/" ]; then
        mkdir /usr/local/sbin/cluster/
	mkdir /usr/local/sbin/cluster/.ssh/
fi
\cp /root/.ssh/id_rsa.pub /usr/local/sbin/cluster/.ssh/id_rsa.pub
\cp /root/.ssh/id_rsa /usr/local/sbin/cluster/.ssh/id_rsa
chmod +r /usr/local/sbin/cluster/.ssh/id_rsa.pub
chmod +r /usr/local/sbin/cluster/.ssh/id_rsa

#Disable known host fingerprint check
echo "StrictHostKeyChecking no" > /root/.ssh/config
echo "UserKnownHostsFile /dev/null" >> /root/.ssh/config

#Permit ssh login from slave node
echo "sshd: $slave: ALLOW" >> /etc/hosts.allow

#send send authorized_keys

cd /usr/local/sbin/cluster/.ssh/

cat id_rsa.pub | sshpass -p ${rootpass} ssh root@${slave} 'cat >> .ssh/authorized_keys'

#send public key too ...
cat id_rsa.pub | sshpass -p ${rootpass} ssh root@${slave} 'cat > .ssh/cluster.pub'

#create IPTABLES RULE
iptables -I FireWall-INPUT -p tcp -s ${slave} --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
service iptables save


#Setup lsyncd service
\cp /etc/lsyncd.emfa /etc/lsyncd.conf

#if nodes are on wan segments keep tranport file not synched
#if [ "$wan" = "true" ]; then
#	sed -i '/^sync{default\.rsync, source="\/etc\/postfix"/d' /etc/lsyncd.conf
#	echo 'sync{default.rsync, source="/etc/postfix", target="root@h0stname:/etc/postfix", rsyncOpts={"-ltgops", "--update", "--temp-dir=/tmp"}, subdirs=false, exclude={ "prng_exch", "mailwatch2rbl.cidr", "transport", "transport.db" }, init=false}' >> /etc/lsyncd.conf
#fi

sed -i "s/h0stname/$slave/" /etc/lsyncd.conf
dos2unix /etc/lsyncd.conf

#Setup first quarantine replica
\cp /usr/local/sbin/emfa-cluster-quarantine-sync.emfa /usr/local/sbin/emfa-cluster-quarantine-sync
sed -i "s/h0stname/$slave/" /usr/local/sbin/emfa-cluster-quarantine-sync
chmod 777 /usr/local/sbin/emfa-cluster-quarantine-sync

exit 0
