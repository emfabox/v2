#!/bin/bash
#Force HTTPS
export status=$1

if [ "$status" = "https" ]; then
	sed -i '327s/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
	echo "Options -Indexes" > /var/www/html/.htaccess
	echo "RewriteEngine On" >> /var/www/html/.htaccess
	echo "RewriteCond %{SERVER_PORT} 80" >> /var/www/html/.htaccess
	echo "RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}" >> /var/www/html/.htaccess
else
	sed -i '327s/AllowOverride All/AllowOverride None/' /etc/httpd/conf/httpd.conf
	echo "Options -Indexes" > /var/www/html/.htaccess
fi
exit 0