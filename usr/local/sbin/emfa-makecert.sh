#!/bin/bash
#Make Https Certificate
export sslcountry=$1
export sslprov=$2
export sslcity=$3
export sslorganization=$4
export sslunit=$5
export sslcommonname=$6
export sslemail=$7
export sslbits=$8
export action=$9

if [ "$action" == "selfsigned" ] ; then

   #Generate HTTPS certificate
   cd /etc/pki/tls/certs
   openssl genrsa -out emfa.key $sslbits
   openssl req -new -x509 -nodes -out emfa.crt -key emfa.key -days 1825 -subj /C=$sslcountry/ST=$sslprov/L=$sslcity/O=$sslorganization/OU=$sslunit/CN=$sslcommonname/emailAddress=$sslemail
   sed -i "/^SSLCertificateFile / c\SSLCertificateFile /etc/pki/tls/certs/emfa.crt" /etc/httpd/conf.d/ssl.conf
   sed -i "/^SSLCertificateKeyFile / c\SSLCertificateKeyFile /etc/pki/tls/certs/emfa.key" /etc/httpd/conf.d/ssl.conf
   rm -rf /etc/pki/tls/certs/root.crt
   rm -rf /etc/pki/tls/certs/intermediate.crt
   sed -i '/^SSLCACertificateFile /d' /etc/httpd/conf.d/ssl.conf
   sed -i '/^SSLCertificateChainFile /d' /etc/httpd/conf.d/ssl.conf
fi

if [ "$action" == "certreq" ] ; then
   #Generate certificate request
   cd /etc/pki/tls/certs
   openssl genrsa -out emfa_req.key $sslbits
   openssl req -new -key emfa_req.key -out emfa_req.csr -subj /C=$sslcountry/ST=$sslprov/L=$sslcity/O=$sslorganization/OU=$sslunit/CN=$sslcommonname/emailAddress=$sslemail

fi

if [ "$action" == "reset" ] ; then
   #Delete certificate request
   cd /etc/pki/tls/certs
   rm -rf emfa_req.key
   rm -rf emfa_req.csr
fi

exit 0
