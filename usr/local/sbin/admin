#!/bin/bash
#set -x
shopt -s extglob
export LANG=en_US.UTF-8
# 
#get database passwords
source /etc/emfa/variables.conf
debug="0"


# 
# Load functions
# 
. /usr/local/lib/emfa/func_getipsettings
. /usr/local/lib/emfa/func_sethostname
. /usr/local/lib/emfa/func_apachesettings
. /usr/local/lib/emfa/func_mailsettings
. /usr/local/lib/emfa/func_systemrestore
. /usr/local/lib/emfa/func_recovermysql
. /usr/local/lib/emfa/func_spamsettings
. /usr/local/lib/emfa/func_trustednetworks
. /usr/local/lib/emfa/func_virussettings
. /usr/local/lib/emfa/func_askcleandeliver
. /usr/local/lib/emfa/func_asksigrules
. /usr/local/lib/emfa/func_asknonspam
. /usr/local/lib/emfa/func_askspam
. /usr/local/lib/emfa/func_ipsettings
. /usr/local/lib/emfa/func_greylisting
. /usr/local/lib/emfa/func_autoupdates
. /usr/local/lib/emfa/func_tunables
. /usr/local/lib/emfa/func_setipsettings
. /usr/local/lib/emfa/func_resetadminpwd
. /usr/local/lib/emfa/func_restorecert
. /usr/local/lib/emfa/func_settimezone
. /usr/local/lib/emfa/func_systemsettings
. /usr/local/lib/emfa/func_getsysstettings
# performance tests
. /usr/local/lib/emfa/func_hdd-test

# 

# 
# Display menus
# 
show_menu() {
  menu=1
  while [ $menu == "1" ]
    do
      func_echo-header 
      echo -e "Please choose an option:"
      echo -e " "
      echo -e "0) Logout                              6) Restore HTTPS Certificate"
      echo -e "1) Shell                               7) Mysql Recovery" 
      echo -e "2) Change Console password             8) Apache Settings" 
      echo -e "3) Halt system                         9) System Restore"
      echo -e "4) IP Settings                        10) Reboot system" 
      echo -e "5) Reset Web admin password"
      echo -e " "     
      echo -e "Performance Tools:"
      echo -e " "
      echo -e "a) HTOP                                b) IFTOP (Network Monitor)"
      echo -e "c) DSTAT (60sec)                       d) MySQL Permormance Checks"
      echo -e "f) Check HDD Performance               g) Ping google! (Ctrl-c to stop)"
      echo -e "h) Check DNS                           "
      echo -e ""
      echo -e -n "$green[EMFA]$clean : "
      local choice
      read choice
      case $choice in
                    0) clear; SSHPID=`ps aux | egrep "sshd: [a-zA-Z]+@" | awk {' print $2 '}`; kill $SSHPID ;;
                    1) exit 0 ;;
                    2) echo "Change Console Password for current user:";
                       passwd; 
                       echo "Press [enter] key to continue. . .";
                       read enterKey;;
                    3) func_halt ;;
                    4) func_ip-settings ;;
                    5) func_resetadminpwd ;;
                    6) func_restorecert ;;                    
                   7) func_recover-mysql ;;
                   8) func_apache-settings ;;
                   9) func_system-restore ;;
                   10) func_reboot ;;
#                    
                    a) htop;;
                    b) iftop;;
                    c) dstat 1 60;;
                    d) echo "*********** MySQL Permormance Checks *************";
                       /usr/local/sbin/check-mysql-perf.pl >/tmp/mysql_status.txt;
                       more /tmp/mysql_status.txt;
                       echo "Press [enter] key to exit...";
                       read enterKey;;
                    f) func_hdd-test ;;
                    g) echo "Ping www.google.com ..";
                       ping www.google.com;
                       echo "Press [enter] key to continue. . .";
                       read enterKey;;
                    h) echo "Query DNS for domain:";
                       echo `hostname`;
                       dig `hostname` mx; 
                       echo "Press [enter] key to continue. . .";
                       read enterKey;;
                    *) echo -e "Error \"$choice\" is not an option..." && sleep 2
      esac
    done
}
# 

# 
# Reboot function
# 
func_reboot() {
  menu=0
  rebootmenu=1
  while [ $rebootmenu == "1" ]
    do
      func_echo-header 
      echo -e "Are you sure you want to reboot this host?"
      echo -e ""
      echo -e "Y)  Yes I am sure"
      echo -e "N)  No no no take me back!"
      echo -e ""
      echo -e -n "$green[EMFA]$clean : "
      local choice
      read choice
      case $choice in
                    Y) reboot && exit 0 ;;
                    N) menu=1 && return  ;;
                    n) menu=1 && return  ;;
                    *) echo -e "Error \"$choice\" is not an option..." && sleep 2
      esac
    done
}
# 

# 
# Halt function
# 
func_halt() {
  menu=0
  haltmenu=1
  while [ $haltmenu == "1" ]
    do
      func_echo-header 
      echo -e "Are you sure you want to halt this host?"
      echo -e ""
      echo -e "Y)  Yes I am sure"
      echo -e "N)  No no no take me back!"
      echo -e ""
      echo -e -n "$green[EMFA]$clean : "
      local choice
      read choice
      case $choice in
                    Y) shutdown -h now && exit 0 ;;
                    N) menu=1 && return  ;;
                    n) menu=1 && return  ;;
                    *) echo -e "Error \"$choice\" is not an option..." && sleep 2
      esac
    done
}
# 

# 
# Function to test IP addresses
# 
function checkip(){
  local ip=$1
  local stat=1

  if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
  fi
  return $stat
}
# 

# 
# Trap CTRL+C, CTRL+Z and quit singles
# 
if [ $debug == "0" ]; then
    trap '' SIGINT SIGQUIT SIGTSTP
fi
# 

# 
# Pause
# 
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}
# 

# 
# Menu header
# 
func_echo-header(){
  clear
  echo -e "$BGblue"
  echo -e "--------------------------------------------------------------"
  echo -e "---      EMFABox Configuration and Maintenance Menu        ---"
  echo -e "---                http://www.emfabox.com                  ---"
  echo -e "--------------------------------------------------------------"
  echo -e "$clean"
}
# 

# 
# Main logic
# 
clear
red='\E[00;31m'
green='\E[00;32m'
yellow='\E[00;33m'
blue='\E[00;34m'
BGblue='\e[1;44m'
magenta='\E[00;35'
cyan='\E[00;36m'
clean='\e[00m'
BGclean='\e[0m'

if [ `whoami` == root ]
    then
        menu="1"
        while [ $menu == "1" ]
        do
            show_menu
        done
    else
        echo -e "$red [EMFA] ERROR: Please become root.$clean"
        exit 0
    fi
#  