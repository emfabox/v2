#!/bin/bash
export action=$1

if [ "$action" = "stopin" ]; then
	/sbin/iptables -I INPUT -p tcp --destination-port 25 -j DROP
	exit 0
fi
if [ "$action" = "check" ]; then
        /sbin/iptables -L
        exit 0
fi
/sbin/iptables -D INPUT -p tcp --destination-port 25 -j DROP