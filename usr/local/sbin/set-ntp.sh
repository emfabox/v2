#!/bin/bash
export ntp=$1
if [ ${#ntp} != 0 ]; then
	ntpdate $ntp
	echo "#!/bin/bash" > /etc/cron.hourly/ntpdate
	echo "ntpdate $ntp > /dev/null 2>&1" >> /etc/cron.hourly/ntpdate
	chmod 0755 /etc/cron.hourly/ntpdate
else
	rm -rf /etc/cron.hourly/ntpdate
fi 
