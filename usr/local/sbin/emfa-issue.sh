#!/bin/bash
#

# Reset
COLOR_OFF='\e[0m'       # Text Reset

# Regular Colors
BLACK='\e[0;30m'        # Black
RED='\e[0;31m'          # Red
GREEN='\e[0;32m'        # Green
YELLOW='\e[0;33m'       # Yellow
BLUE='\e[0;34m'         # Blue
PURPLE='\e[0;35m'       # Purple
CYAN='\e[0;36m'         # Cyan
WHITE='\e[0;37m'        # White

#DATA#
HOSTNAME=`hostname`
KERNEL_INFO=`/bin/uname -r`
OS=`uname -s`
ARCH=`uname -m`

CURRENTRELEASE=`cat /etc/centos-release`
INTERFACES=`/sbin/ifconfig |awk -F '[/  |: ]' '{print $1}' |sed -e '/^$/d'`
CURRIP=`ifconfig eth0 | grep "inet addr" | awk '{ print $2 }' | awk 'BEGIN { FS=":" } { print $2 }'`

GATEWAYIP=$(/sbin/ip route | awk '/default/ { print $3 }')

# ISSUE 
echo -e "*****************************************************************">/etc/issue
echo -e "${GREEN}EMFABox${COLOR_OFF} - ${RED}eMailSecurity${COLOR_OFF} powered by ${GREEN}$CURRENTRELEASE${COLOR_OFF}" >>/etc/issue
echo -e "Kernel\t\t $KERNEL_INFO on an $ARCH"  >>/etc/issue
echo -e "*****************************************************************">>/etc/issue
echo -e "${GREEN}Hostname\t${COLOR_OFF} $HOSTNAME" >> /etc/issue
for INTERFACE in $INTERFACES
    do
        IP_LAN=`/sbin/ip -f inet -o addr show ${INTERFACE} | cut -d\  -f 7 | cut -d/ -f 1`
        echo -e "${GREEN}IP LAN (${INTERFACE})\t ${COLOR_OFF}$IP_LAN" >> /etc/issue
    done
echo -e "${GREEN}GATEWAY\t\t ${COLOR_OFF}$GATEWAYIP" >> /etc/issue
echo -e ".................................................................">>/etc/issue
echo -e "${RESET}" >> /etc/issue
echo -e "To manage the Appliance go to: ${GREEN}http://$CURRIP${COLOR_OFF}">>/etc/issue
echo -e "or on console run ${YELLOW}'admin'${COLOR_OFF} from commandline.">>/etc/issue
echo -e "">>/etc/issue
echo -e ".................................................................">>/etc/issue
echo -e "">>/etc/issue