#!/bin/bash
#


export ip=$1
export mask=$2
export gw=$3
export oldip=$4
export dns=$5
export olddns=$6
export dns2=$7
export olddns2=$8
export hn=$9
export oldhn=${10}

macaddr=`/sbin/ifconfig | grep 'eth0' | tr -s ' ' | cut -d ' ' -f5`

#rm -rf /etc/sysconfig/network-scripts/ifcfg-eth0
echo "BOOTPROTO=static">/etc/sysconfig/network-scripts/ifcfg-eth0
echo "TYPE=Ethernet">>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "DEVICE=eth0">>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "ONBOOT=yes">>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "IPADDR=$ip">>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "NETMASK=$mask">>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "GATEWAY=$gw">>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "HWADDR=$macaddr">>/etc/sysconfig/network-scripts/ifcfg-eth0

sed -i "s/$oldip/$ip/" /etc/hosts
echo "nameserver $dns">/etc/resolv.conf
if [ ${#dns2} > 2 ]; then
echo "nameserver $dns2">>/etc/resolv.conf
fi

# Change Hostname
if [ "$hn" != "$oldhn" ]; then
	# shn returns short hostname without domain
	shn=`echo $hn|gawk -F . '{ print $1 }'`
	echo "$hn">/etc/HOSTNAME
	echo "NETWORKING=yes">/etc/sysconfig/network
	echo "HOSTNAME=$hn">>/etc/sysconfig/network
	echo "127.0.0.1 localhost.localdomain localhost">/etc/hosts
	echo "$ip $hn $shn">>/etc/hosts
	hostname $hn
	sed -i "/^Hostname =/ c\Hostname = $hn" /etc/MailScanner/MailScanner.conf

	# Update MySQL Config
	echo "UPDATE \`mailscanner\`.\`ms_config\` SET \`hostname\`='$hn' WHERE \`hostname\`='$oldhn';">/tmp/tmp.sql
	mysql -hlocalhost -umailwatch -pM4ilw4tch -Dmailscanner </tmp/tmp.sql
	rm -rf /tmp/tmp.sql

	# Update the inbound inline sig files...
	echo "Setting Hostname $hn on signatures..."
	sed -i "s/$oldhn/$hn/" /etc/MailScanner/reports/en/inline.sig.in.txt
	sed -i "s/$oldhn/$hn/" /etc/MailScanner/reports/en/inline.sig.in.html
	sed -i "s/$oldhn/$hn/" /etc/MailScanner/reports/it/inline.sig.in.txt
	sed -i "s/$oldhn/$hn/" /etc/MailScanner/reports/it/inline.sig.in.html

	# Update Apache Config
	sed -i "/^ServerName / c\ServerName $hn" /etc/httpd/conf/httpd.conf
	
	# If enabled backscatter check, update it with new hostname
	grep "^whitelist_bounce_relays" /etc/MailScanner/spam.assassin.prefs.conf
	if [ "$?" -eq "0" ]; then
		sed -i "/^whitelist_bounce_relays/d" /etc/MailScanner/spam.assassin.prefs.conf
		sed -i "/ Your Edits Go Here/awhitelist_bounce_relays $hn" /etc/MailScanner/spam.assassin.prefs.conf
	fi	
	
	# Return reboot trigger
	exit 100
fi

service network restart
