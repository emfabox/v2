#!/bin/bash
#
#This script will add a SASL user to your host

if [ "$1" = "" ]; then
	echo "emfa-sasladd usage:"
	echo "emfa-sasladd username password"
	echo ""
	exit 0
fi

if [ "$2" = "" ]; then
	echo "emfa-sasladd usage:"
	echo "emfa-sasladd username password"
	echo ""
	exit 0
fi

if [ "$1" = "--help" ]; then
	echo "emfa-sasladd usage:"
	echo "emfa-sasladd username password"
	echo ""
	exit 0
fi

echo $2|saslpasswd2 -p -c -u `postconf -h myhostname` $1

