#!/usr/bin/perl -w

use strict;
use warnings;
use URI;
use LWP;
use File::Spec;
use Time::localtime;
use List::MoreUtils qw(uniq);
use HTTP::Cookies;
use HTTP::Request;
use File::Path qw{mkpath};
use IO::Uncompress::Unzip qw(unzip $UnzipError);


sub timestamp {
  my $t = localtime;
  return sprintf( "%04d-%02d-%02d %02d:%02d:%02d",
                  $t->year + 1900, $t->mon + 1, $t->mday,
                  $t->hour, $t->min, $t->sec );
}



my @new_list = ();
my @new_domain = ();

my $HOME = '/opt/emfa/tmp';
#my $HOME = $ENV{HOME} . '/tmp';
mkpath $HOME   unless -d $HOME;

my $filename = '/etc/MailScanner/phishing.safe.sites.conf';
my $zipfile = $HOME.'/top-1m.csv.zip';

if (-f $filename) {
    unlink $filename
        or die "Cannot delete $filename";
}

if (-f $zipfile) {
    unlink $zipfile
        or die "Cannot delete $zipfile";
}

# create a cookie jar on disk
my $cookies = HTTP::Cookies->new(
        file     => $HOME.'/cookies.txt',
        autosave => 1,
        );
        
# create an user-agent and assign the cookie jar to it
my $http = LWP::UserAgent->new();
$http->cookie_jar($cookies);

 my $download_url       = 'http://s3.amazonaws.com/alexa-static/top-1m.csv.zip';
 my $file_req           = HTTP::Request->new('GET', $download_url);
 my $get_file           = $http->request( $file_req );
     
  if($get_file->is_success){
        print "--> downloaded $download_url, saving it to file\n";
 
        # save the file content to disk
        open my $fh, '>', $HOME.'/top-1m.csv.zip'
                                         or die "ERROR: $!\n";
        print $fh $get_file->decoded_content;
        close $fh;
 
        print "\nsaved file:\n";
        print "-------------\n";
        print "filename:  ".$get_file->filename."\n";
        print "size:      ".(-s $HOME.'/top-1m.csv.zip')."\n";
    }
    else {
        die "ERROR: download of $download_url failed: " . $get_file->status_line . "\n";
    }
 #
 
 print "-------------\n";
 
my $u = IO::Uncompress::Unzip->new($zipfile)
    or die "Cannot open $zipfile: $UnzipError";

my $status;

for ($status = 1; ($status > 0) #doc wrong; eof unreliable
     ; $status = $u->nextStream())
{
    my $header = $u->getHeaderInfo();
    my $name = $header->{Name};
      
            my $buff;
            my $newname = $HOME.'/'.$name;
            my $fh = IO::File->new($newname, "w");
            while (($status = $u->read($buff)) > 0) {
                $fh->write($buff);
            }
            $fh->close();
            my $stored_time = $header->{'Time'};
            utime ($stored_time, $stored_time, $newname) or warn "couldn't touch $newname: $!";
     
    last if $status < 0;
}


open (MYFILE, $HOME.'/top-1m.csv');
my @my_source = <MYFILE>;
close (MYFILE);

foreach (@my_source) {
 chomp;
  my @entry  = split /,/, $_;
  my $url = '*.'.$entry[1] ;
  push @new_list, $url;
  }


@new_domain = splice @new_list, 0, 500;

my $arraySize = @new_domain;
$arraySize = scalar (@new_domain);
$arraySize = $#new_domain + 1;
         
if (-f $filename) {
    unlink $filename
        or die "Cannot delete $filename";
}


my $OUTFILE;

my $mydate = timestamp();

open $OUTFILE, '>>', $filename
    or die "Cannot open $filename";
    
    
print { $OUTFILE } "################################\n";    
print { $OUTFILE } "# phishing.safe.sites.conf\n";
print { $OUTFILE } "# \n";
print { $OUTFILE } "# Update $mydate UTC\n";
print { $OUTFILE } "# \n";
print { $OUTFILE } "# Entries: $arraySize\n";
print { $OUTFILE } "#\n\n";


chomp(@new_domain);
    
foreach  (@new_domain) {


print { $OUTFILE } "$_\n"; 

}

print { $OUTFILE } "#EOF\n"; 

close $OUTFILE
