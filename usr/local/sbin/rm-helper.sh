#!/bin/bash
export myfile=$1


if [ "$1" = "" ]; then
        echo "usage:"
        echo "rm-helper file (rm -f)"
        echo ""
        exit 0
fi


/bin/rm -f $myfile 