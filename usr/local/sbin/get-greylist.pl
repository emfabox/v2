#!/usr/bin/perl
# helper to remove :
# check_policy_service unix:private/policyd-spf
# check_policy_service inet:127.0.0.1:2501
# from smtpd_recipient_restrictions
# 
# to keep entries dynamic 
# 

use strict;
use warnings;
use File::Spec;
use Data::Dumper qw(Dumper);


open (MYFILE, '/tmp/EMFA/postfix/main.bak');
my @pf_conf = <MYFILE>;
close (MYFILE);
#s{^\s+|\s+$}{}g foreach @pf_conf;

my @part = grep /smtpd_recipient_restrictions =/, @pf_conf; 
my ($line) = @part;

#print $line;
my @parts = split /=/, $line;


my $c_part = $parts[1];
#print $c_part;

#$c_part =~ s/^\s+|\s+$//g;
#print $c_part;

my @entry  = split /,/,$c_part;
@entry = grep {!/check_policy_service inet:127.0.0.1:2501/} @entry;
@entry = grep {!/policyd-spf/} @entry;

my ($my_result) = join(', ',@entry); 

#print Dumper \$my_result;

print $my_result;

 