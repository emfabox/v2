#!/bin/bash

if [ "$(pidof httpd)" ]; then
   exit 0
else
  /etc/init.d/MailScanner stop >/dev/null 2>&1
  /sbin/fuser -k -n tcp 80 >/dev/null 2>&1
  /etc/init.d/httpd start >/dev/null 2>&1
  /etc/init.d/MailScanner start >/dev/null 2>&1
fi


