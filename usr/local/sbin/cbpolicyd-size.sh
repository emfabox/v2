#!/bin/bash
export size=$1

MAILWATCHSQLPWD=`grep MAILWATCHSQLPWD /etc/DB-Config | sed 's/.*://'`

# Remove old settings
sed -i '/min_servers=/d' /etc/cbpolicyd/cluebringer.conf
sed -i '/min_spare_servers=/d' /etc/cbpolicyd/cluebringer.conf
sed -i '/max_spare_servers=/d' /etc/cbpolicyd/cluebringer.conf
sed -i '/max_servers=/d' /etc/cbpolicyd/cluebringer.conf
sed -i '/max_requests=/d' /etc/cbpolicyd/cluebringer.conf

case "$size" in
"small")
	/bin/sed -i '/^# Large mailserver/amax_requests=1000' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amax_servers=10' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amax_spare_servers=4' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amin_spare_servers=2' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amin_servers=2' /etc/cbpolicyd/cluebringer.conf
    ;;
"medium")
	/bin/sed -i '/^# Large mailserver/amax_requests=1000' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amax_servers=25' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amax_spare_servers=12' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amin_spare_servers=4' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amin_servers=4' /etc/cbpolicyd/cluebringer.conf
    ;;
"large")
    /bin/sed -i '/^# Large mailserver/amax_requests=1000' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amax_servers=64' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amax_spare_servers=16' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amin_spare_servers=8' /etc/cbpolicyd/cluebringer.conf
	/bin/sed -i '/^# Large mailserver/amin_servers=8' /etc/cbpolicyd/cluebringer.conf
    ;;
esac

# Restart Service if active
# killall -q -0 procname && echo Running 
# killall -q -0 procname || echo Not Running 

killall -q -0 cbpolicyd || exit 0

/etc/init.d/cbpolicyd restart

#Check if Cluster is active (status=4) and exit if not


echo "USE cluster; SELECT value FROM settings WHERE variable='status' LIMIT 1;" | mysql -u root --password=$MAILWATCHSQLPWD 2>&1 | grep "4"
if [ "$?" -ne "0" ]; then
exit 0;
fi
sleep 4
hn=`hostname`
slave_ip=`echo "USE cluster; SELECT value FROM settings WHERE variable='slave_ip' AND hostname='$hn' LIMIT 1;" | mysql -u root --password=$MAILWATCHSQLPWD 2>&1 | grep -v "value"`
ssh root@$slave_ip /sbin/service cbpolicyd restart