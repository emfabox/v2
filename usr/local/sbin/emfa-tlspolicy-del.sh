#!/bin/bash
#
#emfa-tlspolicy-del

export domain=$1
#Configure the tlspolicy map
domainescaped=`sed 's/[]\.|$(){}[?+*^]/\\\&/g' <<< $domain`
sed -i "/^$domainescaped/d" /etc/postfix/tls_policy
postmap /etc/postfix/tls_policy
postfix reload

