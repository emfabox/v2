#!/bin/bash
#Make TLS Certificate
export sslcountry=$1
export sslprov=$2
export sslcity=$3
export sslorganization=$4
export sslunit=$5
export sslcommonname=$6
export sslemail=$7
export sslbits=$8
export action=$9

if [ "$action" == "selfsigned" ] ; then
   #Regenerate TLS keys
   cd /etc/postfix/ssl
   openssl req -new -x509 -nodes -out smtpd.pem -keyout smtpd.pem -days 3650 -subj /C=$sslcountry/ST=$sslprov/L=$sslcity/O=$sslorganization/OU=$sslunit/CN=$sslcommonname/emailAddress=$sslemail
   #Edit Postfix config file
   \cp /etc/postfix/ssl/smtpd.pem /etc/postfix/ssl/ca-bundle.pem
   \cp /etc/postfix/ssl/smtpd.pem /etc/postfix/ssl/smtpd.key
   /usr/bin/sudo /usr/sbin/postconf -e "smtpd_tls_key_file = /etc/postfix/ssl/smtpd.key"
   /usr/bin/sudo /usr/sbin/postconf -e "smtpd_tls_cert_file = /etc/postfix/ssl/smtpd.pem"
   /usr/bin/sudo /usr/sbin/postconf -e "smtpd_tls_CAfile = /etc/postfix/ssl/ca-bundle.pem"
   /usr/bin/sudo /usr/sbin/postconf -e "smtp_tls_key_file = /etc/postfix/ssl/smtpd.key"
   /usr/bin/sudo /usr/sbin/postconf -e "smtp_tls_cert_file = /etc/postfix/ssl/smtpd.pem"
   /usr/bin/sudo /usr/sbin/postconf -e "smtp_tls_CAfile = /etc/postfix/ssl/ca-bundle.pem"
fi

if [ "$action" == "certreq" ] ; then
   #Generate certificate request
   cd /etc/postfix/ssl
   openssl genrsa -out smtpdreq.key $sslbits
   openssl req -new -key smtpdreq.key -out smtpdreq.csr -subj /C=$sslcountry/ST=$sslprov/L=$sslcity/O=$sslorganization/OU=$sslunit/CN=$sslcommonname/emailAddress=$sslemail
   chmod 0640 smtpdreq.key
fi

if [ "$action" == "reset" ] ; then
   #Delete certificate request
   cd /etc/postfix/ssl
   rm -rf smtpdreq.key
   rm -rf smtpdreq.csr
fi

exit 0
