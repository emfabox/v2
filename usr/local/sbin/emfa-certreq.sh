#!/bin/sh
#This script will create an SSL certificate request which should
#be sent to your signing authority. Keep the private key safe and
#don't disclose it to anyone.
#
#Based on code from http://lena.franken.de/linux/create_certificate.html

SERVER=$HOSTNAME
PRIVATE_KEY=$SERVER.private.key
CERTIFICATE_FILE=$SERVER.crt
SIGNING_REQUEST=$SERVER.signing.request
VALID_DAYS=1095

echo Delete old private key
rm $PRIVATE_KEY
echo Create new private/public-keys without passphrase for server
openssl genrsa -out $PRIVATE_KEY 1024

echo Create file for signing request
rm $SIGNING_REQUEST
openssl req -new -days $VALID_DAYS -key $PRIVATE_KEY -out $SIGNING_REQUEST

echo Send the content of the file to the certification authority:
echo ""
cat $SIGNING_REQUEST
echo ""
echo Filename for the signing request is: $SIGNING_REQUEST