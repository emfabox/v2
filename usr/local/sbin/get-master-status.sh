#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

MYSQLROOTPWD=`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`


SMS=/tmp/show_master_status.txt
mysql  -u root --password=$MYSQLROOTPWD -ANe "SHOW MASTER STATUS" > ${SMS}
CURRENT_LOG=`cat ${SMS} | awk '{print $1}'`
CURRENT_POS=`cat ${SMS} | awk '{print $2}'`
echo ${CURRENT_LOG} ${CURRENT_POS}