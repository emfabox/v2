#!/bin/bash
export from=$1
export to=$2

if [ "$1" = "" ]; then
        echo "usage:"
        echo "mv-helper source destination"
        echo ""
        exit 0
fi


if [ "$2" = "" ]; then
        echo "usage:"
        echo "mv-helper source destination"
        echo ""
        exit 0
fi

/bin/mv -f $from $to 