#!/bin/bash
#
# 10-09-2015 

export slave=$1
export rootpass=$2

#
if [ "$1" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-ssh-auth host password"
        echo ""
        exit 0
fi

if [ "$2" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-ssh-auth host password"
        echo ""
        exit 0
fi

cd /usr/local/sbin/cluster/.ssh/

#create authorized_keys ...
cat id_rsa.pub | sshpass -p ${rootpass} ssh root@${slave} 'cat >> .ssh/authorized_keys'

#send public key too ...
cat id_rsa.pub | sshpass -p ${rootpass} ssh root@${slave} 'cat > .ssh/cluster.pub'

#remove host from known_hosts 
#ssh-keygen -R hostname
#sed -i '/$pub_key/d' /root/.ssh/authorized_keys 