#!/bin/bash

MAILWATCHSQLPWD=`grep MAILWATCHSQLPWD /etc/DB-Config | sed 's/.*://'`

echo "USE mailscanner; UPDATE sys_config SET value='YES' WHERE variable_name='cbpolicyd';" | mysql -u root --password=$MAILWATCHSQLPWD 2>&1

#Check if Greylisting is active or not
echo "USE mailscanner; SELECT value FROM sys_config WHERE variable_name='greylisting';" | mysql -u root --password=$MAILWATCHSQLPWD 2>&1 | grep "YES"
if [ "$?" -ne "0" ]; then
	# Greylisting OFF
  /usr/sbin/postconf -e "smtpd_recipient_restrictions = check_policy_service inet:127.0.0.1:10031, permit_sasl_authenticated, permit_mynetworks, ,check_client_access mysql:/etc/postfix/sql/mysql_rbl_override.cf,check_recipient_access mysql:/etc/postfix/sql/mysql_virtual_recipient_access.cf,reject_non_fqdn_sender,reject_non_fqdn_recipient,reject_unknown_sender_domain,reject_unknown_recipient_domain, reject_unauth_destination, check_policy_service unix:private/policyd-spf,check_sender_access mysql:/etc/postfix/sql/mysql_domain_antispoofing.cf,permit"
  /usr/sbin/postconf -e "smtpd_end_of_data_restrictions = check_policy_service inet:127.0.0.1:10031"
else
	# Greylisting ON
  /usr/sbin/postconf -e "smtpd_recipient_restrictions = check_policy_service inet:127.0.0.1:10031, permit_sasl_authenticated, permit_mynetworks, ,check_client_access mysql:/etc/postfix/sql/mysql_rbl_override.cf,check_recipient_access mysql:/etc/postfix/sql/mysql_virtual_recipient_access.cf,reject_non_fqdn_sender,reject_non_fqdn_recipient,reject_unknown_sender_domain,reject_unknown_recipient_domain, reject_unauth_destination, check_policy_service inet:127.0.0.1:2501, check_policy_service unix:private/policyd-spf,check_sender_access mysql:/etc/postfix/sql/mysql_domain_antispoofing.cf,permit"
  /usr/sbin/postconf -e "smtpd_end_of_data_restrictions = check_policy_service inet:127.0.0.1:10031"
fi

chown root:apache /etc/postfix/main.cf
chmod 664 /etc/postfix/main.cf

/sbin/chkconfig --level 345 cbpolicyd on
/sbin/service cbpolicyd start 
/usr/sbin/postfix reload

#Check if Cluster is active (status=4) and exit if not
echo "USE cluster; SELECT value FROM settings WHERE variable='status' LIMIT 1;" | mysql -u root --password=$MAILWATCHSQLPWD 2>&1 | grep "4"
if [ "$?" -ne "0" ]; then
exit 0;
fi
sleep 4
hn=`hostname`
slave_ip=`echo "USE cluster; SELECT value FROM settings WHERE variable='slave_ip' AND hostname='$hn' LIMIT 1;" | mysql -u root --password=$MAILWATCHSQLPWD 2>&1 | grep -v "value"`
ssh root@$slave_ip /sbin/chkconfig --level 345 cbpolicyd on
ssh root@$slave_ip /sbin/service cbpolicyd start
ssh root@$slave_ip /usr/sbin/postfix reload