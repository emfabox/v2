#!/bin/bash
# This script checks for any MailScanner quarantine spool permission problem

if [ "$(find /var/spool/MailScanner/quarantine/ -type d -perm 0700 -user root)" ]; then
dir=`find /var/spool/MailScanner/quarantine/ -type d -perm 0700 -user root`
for line in $dir; do
	chown -R postfix:apache $line
	chmod -R 770 $dir
done
/etc/init.d/MailScanner restart
fi
