#!/bin/bash
#Import TLS certificates
export action=$1

if [ "$action" = "wildcard" ]; then
	cd /etc/postfix/ssl
        \mv -f /tmp/tmp_emfa.crt /etc/postfix/ssl/smtpd.pem
        \cp /etc/postfix/ssl/smtpd.pem /etc/postfix/ssl/ca-bundle.pem
	\mv -f /tmp/tmp_emfa.key /etc/postfix/ssl/smtpd.key
        chmod 0644 smtpd.key
else
	cd /etc/postfix/ssl
        \mv -f /tmp/smtpd.pem /etc/postfix/ssl/smtpd.pem
        \mv -f /tmp/ca-bundle.pem /etc/postfix/ssl/ca-bundle.pem
        \mv -f /etc/postfix/ssl/smtpdreq.key /etc/postfix/ssl/smtpd.key
        rm -rf /etc/postfix/ssl/smtpdreq.csr
        chmod 0644 smtpd.key
fi
exit 0
