#!/bin/bash
# Monitor Postfix Queue

queuelength=`/usr/sbin/postqueue -p -c /etc/postfix-out | tail -n1 | awk '{print $5}'`
queuecount=`echo $queuelength | grep "[0-9]"`

if [ "$queuecount" == "" ]; then
        echo 0;
else
        echo ${queuelength};
fi
exit 35