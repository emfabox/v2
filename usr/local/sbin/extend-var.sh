#!/bin/bash
# Extend Var LVM partition
# 
disk=/dev/sdb
lastpartno=$(parted /dev/sdb print | awk '{print $1}' | sed '$d' | tail -1)
partno=$((lastpartno + 1))
if [[ -z $disk ]]; then
 echo "Usage: $0 disk device name $1 partition number: e.g $0 /dev/sdb $1 2"
 exit
fi

if [[ -z $partno ]]; then
 echo "Usage: $0 disk device name $1 partition number: e.g $0 /dev/sdb $1 2"
 exit
fi
 
if [[ -e ${disk}${partno} ]]; then
 echo "==> ${disk}${partno} already exist!"
 exit
fi
 
echo "==> Read first and last cylinder"
startcyl=$(parted /dev/sdb unit cyl print | egrep '\s1\s{6}([0-9]*)cyl' | awk '{ print $4 }' | rev | cut -c 4- | rev)
lastncyl=$(parted $disk unit cyl print  | sed -n 's/.*: \([0-9]*\)cyl/\1/p')
startncyl=$((startcyl + 1))
if [[ $startncyl != [0-9]* ]]; then
 echo "disk $disk has invalid start cylinders number: $startncyl"
 exit
fi

if [[ $lastncyl != [0-9]* ]]; then
 echo "disk $disk has invalid end cylinders number: $lastncyl"
 exit
fi
 
echo "==> creating primary parition  $partno from cyl $startncyl to cyl $lastncyl"
parted -a optimal $disk mkpart primary ${startncyl}cyl ${lastncyl}cyl
echo "==> set partition $partno to type: lvm "
parted $disk set $partno lvm on
#partprobe > /dev/null 2>&1
partx -a /dev/sdb > /dev/null 2>&1
echo "==> create PV ${disk}${partno} "
pvcreate ${disk}${partno}
echo "==> Add new physical volume to volume group"
vgextend vg_var ${disk}${partno}
echo "==> Extend the logical volume (this uses all free space)"
lvextend -l +100%FREE /dev/vg_var/lv_var
echo "==> Grow the file system to include the new space"
resize2fs /dev/vg_var/lv_var
